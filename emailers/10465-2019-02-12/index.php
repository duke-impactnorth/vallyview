<html>
<head>
<title>One Weekend Only Sales Event!</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10465.psd) -->
</p>
<table width="701" height="2771" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td   colspan="2" align="center">Please confirm your interest below, if you wish to be included in our One Weekend Only Sales Event! If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td  colspan="2">&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_01.png" width="701" height="442" alt="Valleyview Bowmanville"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_02.png" width="701" height="481" alt="Please confirm your interest below, if you wish to be included in our One Weekend Only Sales Event!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/emailers/10465-2019-02-12/thanks.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_03.png" width="701" height="78" alt="Click to confirm your interest"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_04.png" width="701" height="50" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_05.jpg" width="701" height="498" alt="A Collection of Detached Homes in Bowmanville"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_06.png" width="701" height="758" alt="Starting from $609,000* Don’t miss this opportunity to purchase at Durham’s MOST successful community"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_07.png" width="701" height="48" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_08.png" width="701" height="43" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_09.png" width="701" height="148" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_10.png" width="701" height="201" alt="All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. E.&amp; O.E."></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_11.png" width="351" height="24" alt="unsubscribe from this community"></a></td>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_12.png" width="350" height="24" alt="unsubscribe from WP Development"></a></td>
	</tr>
	<tr>
	  <td height="25"  >&nbsp;</td>
	  <td height="25"  >&nbsp;</td>
  </tr>
	<tr>
	  <td colspan="2" align="center"  ><p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
        <p>Phase 2 Coming Soon</p>
        <p>Please confirm your interest below, if you wish to be included in our One Weekend Only Sales Event!</p>
        <p><a href="https://thevalleyview.ca/emailers/10465-2019-02-12/thanks.php">Click to confirm your interest</a></p>
        <p>A Collection of Detached Homes in Bowmanville</p>
        <p>Starting from $609,000*</p>
        <p>Don’t miss this opportunity to purchase at Durham’s MOST successful community</p>
        <p>&nbsp;</p>
        <p><a href="https://goo.gl/maps/8djtz9HfZe72"><br>
          </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
          Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
        <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. E.&amp; O.E.</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>