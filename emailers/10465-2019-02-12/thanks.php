<html>
<head>
<title>One Weekend Only Sales Event!</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10465.psd) -->
</p>
<table width="701" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">

	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_01.png" width="701" height="442" alt="Valleyview Bowmanville"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="images/index_02z.png" width="701" height="398" alt="Please confirm your interest below, if you wish to be included in our One Weekend Only Sales Event!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="images/index_03a.png" width="701" height="78" alt="Click to confirm your interest"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="images/index_06a.png" width="701" height="69" alt="Starting from $609,000* Don’t miss this opportunity to purchase at Durham’s MOST successful community"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_07.png" width="701" height="48" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_08.png" width="701" height="43" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_09.png" width="701" height="148" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10465-2019-02-12/images/index_10.png" width="701" height="201" alt="All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. E.&amp; O.E."></a></td>
	</tr>
	<tr>
	  <td  >&nbsp;</td>
	  <td  >&nbsp;</td>
  </tr>
	
</table>
<!-- End Save for Web Slices -->
</body>
</html>