<html>
<head>
<title>WEAL-VAL-W-EM-10466-OneWeekendOnly-FINAL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (WEAL-VAL-W-EM-10466-OneWeekendOnly-FINAL.psd) -->
</p>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td   colspan="2" align="center">Don't miss this opportunity to purchase at Durham's MOST successful community. If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td  colspan="2">&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_01.png" width="700" height="429" alt="Phase 2 Opening March 30th At 11am Sharp"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_02.png" width="700" height="549" alt="Don't miss this opportunity to purchase at Durham's MOST successful community"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/Valleyview-animation.gif" width="700" height="579" alt="Up to a Million Dollars in Giveaways Cash Draws* 1 in 10 chance to win $25,000 off the purchase price"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_04.png" width="700" height="232" alt="Bonus Upgrades* Standard Features*"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_05.jpg" width="700" height="498" alt="A Collection of Traditional &amp; Transitional Detached Homes in Bowmanville "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_06.png" width="700" height="595" alt="Starting from $609,000* "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_07.png" width="700" height="52" alt="tel:905-419-6888"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_08.png" width="700" height="52" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_09.png" width="700" height="284" alt="*This Campaign is open only to those who purchased from March 30th, 2019 to the end of second release for *Valleyview or further notice from WP Development Inc. and who are 19 years of age or older as of the date of entry.The Campaign is only open to legal residents of Canada, and is void where prohibited by law. Entries will be accepted at 104-2021 Green Rd, Bowmanville.The Purchaser(s) is/are automatically entered after a home is purchased, and only become valid when all conditions (mortgage and lawyer review of contract) are fulfilled. Prize will be given on/after successful closing of the house purchased.Every Purchaser(s) will receive the following: bonus package (approximate value $25,000), luxury features (approximate value $25,000). One in every 10 successful buyers will enter a draw to win a ($25,000) deduction off purchase price on closing of house purchased. The sum of the total giveaway is approximately $1,000,000.00. Actual/appraised value may differ at time of prize award. The odds of winning depend on the number of eligible entries received. The specifics of the p"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_10.png" width="700" height="271" alt="All illustrations are artist's concept. All dimensions are approximate. **Prices, specifications, terms and conditions are subject to change without notice. Further details available at the Valleyview Sales Office. E.&amp;O.E."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_11.png" width="350" height="74" alt=""></a></td>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/10466-2019-03-07/images/index_12.png" width="350" height="74" alt=""></a></td>
	</tr>
	<tr>
	  <td  colspan="2" align="center"><p>&nbsp;</p>
	    <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
        <p>Phase 1 Sold Out </p>
        <p>Phase 2 Coming Soon</p>
        <p>Phase 2 Opening March 30th At 11am Sharp</p>
        <p>Don't miss this opportunity to purchase at Durham's MOST successful community</p>
        <p>Up to a Million Dollars in Giveaways</p>
        <p>Cash Draws*<br>
          1 in 10 chance to win $25,000 off the purchase price</p>
        <p>Bonus Upgrades*</p>
        <p>Standard Features* </p>
        <p>A Collection of Traditional &amp; Transitional Detached Homes in Bowmanville        </p>
        <p><a href="https://goo.gl/maps/8djtz9HfZe72"><br>
          </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
        Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
        <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
        <p>*This Campaign is open only to those who purchased from March 30th, 2019 to the end of second release for *Valleyview or further notice from WP Development Inc. and who are 19 years of age or older as of the date of entry.The Campaign is only open to legal residents of Canada, and is void where prohibited by law. Entries will be accepted at 104-2021 Green Rd, Bowmanville.The Purchaser(s) is/are automatically entered after a home is purchased, and only become valid when all conditions (mortgage and lawyer review of contract) are fulfilled. Prize will be given on/after successful closing of the house purchased.Every Purchaser(s) will receive the following: bonus package (approximate value $25,000), luxury features (approximate value $25,000). One in every 10 successful buyers will enter a draw to win a ($25,000) deduction off purchase price on closing of house purchased. The sum of the total giveaway is approximately $1,000,000.00. Actual/appraised value may differ at time of prize award. The odds of winning depend on the number of eligible entries received. The specifics of the prize shall be solely determined by WP Development Inc. </p>
        <p>All illustrations are artist's concept. All dimensions are approximate. **Prices, specifications, terms and conditions are subject to change without notice. Further details available at the Valleyview Sales Office. E.&amp;O.E.</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>