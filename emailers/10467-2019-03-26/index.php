<html>
<head>
<title>10467</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10467.psd) -->
</p>
<table width="700" height="4160" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td align="center"  >Don’t miss this opportunity to purchase at Durham’s  MOST successful community. 
      If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td  >&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_01.png" width="700" height="374" alt="Reminder - Only 4 Days Away! Phase 1 Sold Out"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_02.png" width="700" height="741" alt="New Release Of Lots Valleyview Bowmanville Phase 2  Opening March 30th At 11am Sharp"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/10465-2019-02-12/thanks.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_03.png" width="700" height="98" alt="Yes, I will attend - Click here"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_04a.png" width="700" height="293" alt="This Weekend Only  Closing Adjustment  Waived With Purchase. (Only Certain Costs Are Applicable*)"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_05.jpg" width="700" height="282" alt="A Collection of Traditional  &amp; Transitional Detached Homes in Bowmanville"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_06.png" width="700" height="167" alt="Click to View Floorplans"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_07.png" width="700" height="484" alt="Starting From $609,990* Don’t miss this opportunity to purchase at Durham’s  MOST successful community "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_08.png" width="700" height="1141" alt="Things To Bring $5,000 Bank Draft For The First Deposit* Personal Cheques* Valid Government Issued Photo Identification*  (Driver's License,passport Or  Permanent Resident Card) Mortgage Pre- Approval Letter  If Available. "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_09.png" width="700" height="46" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_10.png" width="700" height="57" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_11a.png" width="700" height="126" alt=""></a></td>
	</tr>
	<tr>
	  <td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_11b.png" width="700" height="27" alt=""></a></td>
  </tr>
	<tr>
	  <td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10467-2019-03-26/images/index_11c.png" width="700" height="326" alt=""></td>
  </tr>
	<tr>
	  <td align="center"  ><p>&nbsp;</p>
	    <p>Reminder - Only 4 Days Away! </p>
        <p>Phase 1 Sold Out</p>
        <p>New Release Of Lots</p>
	    <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a>        </p>
	    <p>Phase 2 </p>
        <p>Opening March 30th At 11am Sharp</p>
        <p><a href="https://thevalleyview.ca/emailers/10465-2019-02-12/thanks.php">Yes, I will attend -  Click here</a></p>
        <p>This Weekend Only </p>
        <p>Closing Adjustment  Waived With Purchase.<br>
          (Only Certain Costs Are Applicable*)</p>
        <p><a href="https://thevalleyview.ca/floorplans.html">Click to View Floorplans</a></p>
        <p>A Collection of Traditional  &amp; Transitional Detached Homes in Bowmanville</p>
        <p>Starting From $609,990*</p>
        <p>Don’t miss this opportunity to purchase at Durham’s  MOST successful community </p>
        <p>Things To Bring</p>
        <p>$5,000 Bank Draft For The First Deposit*<br>
          Personal Cheques*<br>
          Valid Government Issued Photo Identification* <br>
          (Driver's License,passport Or  Permanent Resident Card)</p>
        <p>Mortgage Pre- Approval Letter  If Available.        </p>
        <p><a href="https://goo.gl/maps/8djtz9HfZe72"><br>
          </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
          Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
        <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
        <p>*This Campaign is open only to those who purchased from March 30th, 2019 to the end of second release for *Valleyview or further notice from WP Development Inc. and who are 19 years of age or older as of the date of entry.The Campaign is only open to legal residents of Canada, and is void where prohibited by law. Entries will be accepted at 104-2021 Green Rd, Bowmanville.The Purchaser(s) is/are automatically entered after a home is purchased, and only become valid when all conditions (mortgage and lawyer review of contract) are fulfilled. Prize will be given on/after successful closing of the house purchased.Every Purchaser(s) will receive the following: bonus package (approximate value $25,000), luxury features (approximate value $25,000). One in every 10 successful buyers will enter a draw to win a ($25,000) deduction off purchase price on closing of house purchased. The sum of the total giveaway is approximately $1,000,000.00. Actual/appraised value may differ at time of prize award. The odds of winning depend on the number of eligible entries received. The specifics of the prize shall be solely determined by WP Development Inc.</p>
        <p>Items included: <br>
          Closing Adjustment, Security Deposit, Building or Foundation Survey, Driveway Fee, Electronic Registration System Fee, Utilities installation and charges, Transaction Levy Surcharge Fee, Taxes Fuel water rates assessment rates and local improvement rates, Ontario New Home Warranties Plan Act Enrolment Fee. </p>
        <p>All illustrations are artist’s concept. All dimensions are approximate. **Prices, specifications, terms and conditions are subject to change without notice. Further details available at the Valleyview Sales Office. E.&amp;O.E.</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>