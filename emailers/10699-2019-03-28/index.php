<html>
<head>
<title>WEAL-VAL-W-EM-10699-2DaysAway_F-OTLN</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (WEAL-VAL-W-EM-10699-2DaysAway_F-OTLN.psd) -->
</p>
<table width="701" height="3237" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td align="center"  >Don’t miss this opportunity to purchase at Durham’s  MOST successful community. 
      If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td  >&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_01.png" width="701" height="375" alt="Valleyview Bowmanville Opening march 30th at 11 am Sharp!"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_02.png" width="701" height="407" alt="This Weekend Only Closing Adjustment Waived With Purchase. (Only Certain Costs Are Applicable*)"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_03.png" width="701" height="331" alt="Starting from $609,990* Things To Bring $5,000 bank draft for the first deposit*"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_04.jpg" width="701" height="280" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_05.png" width="701" height="172" alt="Click to Browse Online"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_06.png" width="701" height="922" alt="Personal cheques* Valid Government Issued Photo Identification* (Driver's license,Passport or Permanent Resident Card) Mortgage Pre-Approval Letter if available."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_07.png" width="701" height="360" alt="A Collection of Traditional &amp; Transitional Detached Homes in Bowmanville"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_08.png" width="701" height="48" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_09.png" width="701" height="50" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_10.png" width="701" height="130" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_11.png" width="701" height="25" alt="Unsubscribe from this community  |  Unsubscribe from WP Development"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/10699-2019-03-28/images/index_12.png" width="701" height="137" alt="Waived closing adjustments: Building or foundation survey, driveway fee, electronic registration system fee, release of vendor’s lien, registration of discharges, provision installation connection, energization and payments, transaction levy surcharge, increases in levies/charges etc. All illustrations are artist’s concept. All dimensions are approximate. **Prices, specifications, terms and conditions are subject to change without notice. Further details available at the Valleyview Sales Office. E.&amp;O.E."></td>
	</tr>
	<tr>
	  <td align="center"  ><p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>Opening march 30th at 11 am Sharp!</p>
      <p>This Weekend Only </p>
      <p>Closing Adjustment Waived With Purchase.<br>
        (Only Certain Costs Are Applicable*)</p>
      <p><a href="https://thevalleyview.ca/floorplans.html">Click to Browse Online</a></p>
      <p>Starting from $609,990*</p>
      <p>Things To Bring</p>
      <p>$5,000 bank draft for the first deposit*<br>
        Personal cheques*<br>
        Valid Government Issued Photo Identification* (Driver's license,Passport or Permanent Resident Card)<br>
        Mortgage Pre-Approval Letter if available.</p>
      <p>A Collection of Traditional &amp; Transitional Detached Homes in Bowmanville</p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72"><br>
        </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
        Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>Waived closing adjustments: <br>
        Building or foundation survey, driveway fee, electronic registration system fee, release of vendor’s lien, registration of discharges, provision installation connection, energization and payments, transaction levy surcharge, increases in levies/charges etc. </p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. **Prices, specifications, terms and conditions are subject to change without notice. Further details available at the Valleyview Sales Office. E.&amp;O.E.</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>