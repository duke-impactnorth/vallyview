<html>
<head>
<title>10702-1DaysAway_5-01</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10702-1DaysAway_5-01.psd) -->
</p>
<table width="700" height="5064" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_01.gif" width="700" height="400" alt="Reminder Opening March 30th at 11am SHARP"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_02.gif" width="700" height="388" alt="A Collection of Traditional &amp; Transitional Detached Homes in Bowmanville"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/10702-2019-03-29/thanks.php"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_03.gif" width="700" height="306" alt="Click To Confirm Your Attendance"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_04.gif" width="700" height="364" alt="Traditional | Transitional Starting from $609,990*"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_05.jpg" width="700" height="270" alt="Up to a Million Dollars in Giveaways Cash Draws* 1 in 10 chance to win $25,000 off the purchase price"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_06.gif" width="700" height="81" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_07.jpg" width="700" height="542" alt="Closing Adjustment Waived With Purchase* Only certain costs are applicable"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_08.gif" width="700" height="290" alt="Starting from $609,990*"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/animation.gif" width="700" height="664" alt="Up to a Million Dollars in Giveaways Cash Draws* 1 in 10 chance to win $25,000 off the purchase price Closing Adjustment Waived With Purchase* Only certain costs are applicable Luxury Entertainment System $9,000 market value* Bonus Upgrades* Standard Features*  "></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_10.gif" width="700" height="573" alt="Things To Bring $5,000 bank draft for the first deposit* ($5,000 bank draft payable to Valleyview Bowmanville Inc.) Personal cheques*"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_11.gif" width="700" height="397" alt="Valid Government Issued Photo Identification* (Driver's license,Passport or Permanent Resident Card) Mortgage Pre-Approval Letter if available."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_12.gif" width="700" height="170" alt="SALES OFFICE 2021 Green Rd. Bowmanville, On L1C 3K7"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_13.gif" width="700" height="44" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_14.gif" width="700" height="66" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_15.gif" width="700" height="95" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_16.gif" width="700" height="392" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="Unsubscribe from this community  |  Unsubscribe from WP Development"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10702-2019-03-29/images/index_17.gif" width="700" height="23" alt="Unsubscribe from this community  |  Unsubscribe from WP Development"></a></td>
	</tr>
	<tr>
	  <td align="center"  ><p>&nbsp;</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>        Reminder</p>
      <p>Opening March 30th at 11am SHARP</p>
      <p><a href="https://thevalleyview.ca/emailers/10702-2019-03-29/thanks.php">Click To Confirm Your Attendance</a></p>
      <p>A Collection of Traditional &amp; Transitional Detached Homes in Bowmanville</p>
      <p>Traditional | Transitional Starting from $609,990*</p>
      <p><a href="https://thevalleyview.ca/floorplans.html">Click To Browse Online</a></p>
      <p>Up to a Million Dollars in Giveaways <br>
        Cash Draws* <br>
        1 in 10 chance to win $25,000 off the purchase price <br>
        Closing Adjustment Waived With Purchase* <br>
        Only certain costs are applicable <br>
        Luxury Entertainment System $9,000 market value* <br>
        Bonus Upgrades* <br>
        Standard Features* </p>
      <p>Things To Bring <br>
        $5,000 bank draft for the first deposit* <br>
        ($5,000 bank draft payable to Valleyview Bowmanville Inc.)<br>
        Personal cheques*<br>
        Valid Government Issued Photo Identification* <br>
        (Driver's license,Passport or Permanent Resident Card) <br>
        Mortgage Pre-Approval Letter if available.</p>
      <p>&nbsp;</p>
      <p>SALES OFFICE <br>
        2021 Green Rd. <br>
        Bowmanville, <br>
        On L1C 3K7</p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72"><br>
        </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
        Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>Waived closing adjustments: <br>
        Building or foundation survey, driveway fee, electronic registration system fee, release of vendor’s lien, registration of discharges, provision installation connection, energization and payments, transaction levy surcharge, increases in levies/charges etc. </p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. **Prices, specifications, terms and conditions are subject to change without notice. Further details available at the Valleyview Sales Office. E.&amp;O.E.</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>