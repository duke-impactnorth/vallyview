<html>
<head>
<title>Thank You</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10702-1DaysAway_5-02.psd) -->
</p>
<table width="700" height="1847" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td><a href="https://thevalleyview.ca/"><img src="images/thanks_01.png" width="700" height="260" alt=""></a></td>
	</tr>
	<tr>
		<td>
			<img src="images/thanks_02.png" width="700" height="400" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/thanks_03.png" width="700" height="261" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/thanks_04.png" width="700" height="368" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/thanks_05.png" width="700" height="204" alt=""></td>
	</tr>
	<tr>
		<td><a href="tel:905-419-6888"><img src="images/thanks_06.png" width="700" height="43" alt=""></a></td>
	</tr>
	<tr>
		<td><a href="mailto:info@thevalleyview.ca"><img src="images/thanks_07.png" width="700" height="49" alt=""></a></td>
	</tr>
	<tr>
		<td><a href="http://wpdevelopment.ca/"><img src="images/thanks_08.png" width="700" height="112" alt=""></a></td>
	</tr>
	<tr>
		<td>
			<img src="images/thanks_09.png" width="700" height="150" alt=""></td>
	</tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>