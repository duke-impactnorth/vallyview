<html>
<head>
<title>10713</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10713.psd) -->
</p>
<table width="700" height="2858" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td align="center"  >Don’t miss this opportunity to purchase at Durham’s  MOST successful community. 
      If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td  >&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_01.jpg" width="700" height="381" alt="Due to incredible success, our Grand Release Event continues."></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_02.png" width="700" height="201" alt="The lots are selling fast."></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_03.jpg" width="700" height="369" alt="Contact our sales representatives for more details."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_04.png" width="700" height="244" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://goo.gl/maps/8djtz9HfZe72"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_05.png" width="700" height="185" alt="SALES OFFICE 2021 Green Rd. Bowmanville, On L1C 3K7"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_06.png" width="700" height="58" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_07.png" width="700" height="45" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_08.png" width="700" height="343" alt="Office Hours: Monday to Thursday 1-7 pm Friday closed  Saturday &amp; Sunday 11-5 pm  "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_09.png" width="700" height="114" alt="click to Browse Online"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_10.png" width="700" height="328" alt="A Collection of Traditional &amp; Transitional Detached Homes Starting from $609,990*"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_11.jpg" width="700" height="268" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_12.png" width="700" height="108" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_13.png" width="700" height="195" alt="All illustrations are artist’s concept. All dimensions are approximate. **Prices, specifications, terms and conditions are subject to change without notice. Further details available at the Valleyview Sales Office. E.&amp; O.E."></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10713-2019-04-04/images/index_14.png" width="700" height="19" alt="Unsubscribe from this community  |  Unsubscribe from WP Development"></a></td>
	</tr>
	<tr>
	  <td align="center"  ><p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>Due to incredible success, our Grand Release Event continues.</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>The lots are selling fast.</p>
      <p>Contact our sales representatives for more details.</p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72">SALES OFFICE<br>
        2021 Green Rd.<br>
        Bowmanville, On L1C 3K7</a></p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72"><br>
      </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
      <p>Office Hours:<br>
        Monday to Thursday 1-7 pm<br>
        Friday closed <br>
        Saturday &amp; Sunday 11-5 pm </p>
      <p><a href="https://thevalleyview.ca/floorplans.html">Click to Browse Online</a></p>
      <p>A Collection of Traditional &amp; Transitional Detached Homes Starting from $609,990*</p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. **Prices, specifications, terms and conditions are subject to change without notice. Further details available at the Valleyview Sales Office. E.&amp; O.E.</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>