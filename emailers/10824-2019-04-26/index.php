<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10824-RBC-Event_F-OTLN.psd) -->
</p>
<table width="701" height="1875" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td align="center"  >Join us for a free Information Session as we discuss: What type of Mortgage is Best for Me?  
      If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td  >&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_01.png" width="701" height="217" alt="Join us for a free Information Session as we discuss:"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_02.png" width="701" height="360" alt="What type of Mortgage is Best for Me ? Government Stress Test and How it will Affect me Primary Residence or Investment Property "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_03.png" width="701" height="409" alt="Current Real Estate Market Resale vs. New Construction  How much Down payment do I require?  How much can I Afford? Steps to being Pre-Approved Trends in Mortgage Rates "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_04.png" width="701" height="121" alt="Thursday, May 23, 2019 at 7:00 pm Location: Valleyview Bowmanville Sales Office 104-2021 Green Rd, Bowmanville L1C 3K7"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://goo.gl/maps/iSRYtmhC2m19aSXFA"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_05.png" width="701" height="194" alt="104-2021 Green Rd, Bowmanville L1C 3K7"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="http://thevalleyview.ca/emailers/10824-2019-04-26/thanks.php"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/animation-yes.gif" width="700" height="150" alt="Yes! I will attend - click here"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_07.png" width="701" height="49" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_08.png" width="701" height="106" alt="Presented in partnership with: Dave Stone  RBC Mortgage Specialist "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="tel:905-259-1655"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_09.png" width="701" height="15" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="mailto:dave.stone@rbc.com"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_10.png" width="701" height="23" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_11.png" width="701" height="21" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_12.png" width="701" height="189" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_13.png" width="701" height="19" alt=""></a></td>
	</tr>
	<tr>
	  <td align="center"  ><p>&nbsp;</p>
	    <p>&nbsp;</p>
        <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
        <p>Join us for a free Information Session as we discuss:</p>
        <p>What type of Mortgage is Best for Me ?<br>
          Government Stress Test and How it will Affect me<br>
          Primary Residence or Investment Property<br>
          Current Real Estate Market<br>
          Resale vs. New Construction <br>
          How much Down payment do I require? <br>
          How much can I Afford?<br>
          Steps to being Pre-Approved <br>
          Trends in Mortgage Rates </p>
        <p>Thursday, May 23, 2019<br>
          at 7:00 pm</p>
        <p>Location:<br>
          Valleyview Bowmanville Sales Office<br>
          <a href="https://goo.gl/maps/iSRYtmhC2m19aSXFA">104-2021 Green Rd, Bowmanville L1C 3K7</a></p>
        <p>Coffee and refreshments will be served.</p>
        <p><a href="http://thevalleyview.ca/emailers/10824-2019-04-26/thanks.php">Yes! I will attend - click here</a></p>
        <p>Presented in partnership with:</p>
        <p>Dave Stone <br>
          RBC Mortgage Specialist <br>
          <a href="tel:905-259-1655">905-259-1655</a><br>
          <a href="mailto:dave.stone@rbc.com">dave.stone@rbc.com</a></p>
        <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
        <p>&nbsp;</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>