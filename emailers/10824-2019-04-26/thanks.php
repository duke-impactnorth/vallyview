<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10824-RBC-Event_F-OTLN.psd) -->
</p>
<table width="701" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_01.png" width="701" height="217" alt="Join us for a free Information Session as we discuss:"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="images/index_02a.png" width="701" height="589" alt="What type of Mortgage is Best for Me ? Government Stress Test and How it will Affect me Primary Residence or Investment Property "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_07.png" width="701" height="49" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_08.png" width="701" height="106" alt="Presented in partnership with: Dave Stone  RBC Mortgage Specialist "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="tel:905-259-1655"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_09.png" width="701" height="15" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="mailto:dave.stone@rbc.com"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_10.png" width="701" height="23" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_11.png" width="701" height="21" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="http://thevalleyview.ca/emailers/10824-2019-04-26/images/index_12.png" width="701" height="189" alt=""></a></td>
	</tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>