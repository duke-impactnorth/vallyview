<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (10992.psd) -->
</p>
<table width="700" height="4263" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td align="center" >Hurry in and purchase before June 23rd to have a chance to win $25K off the purchase price. If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td   >&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  ><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_01.png" width="700" height="245" alt="Valleyview Bowmanville Friends &amp;  Family BBQ Please Join The Excitement"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_02.png" width="700" height="490" alt="Hurry in and purchase before June 23rd to have a chance to win $25,000 off the purchase price.
"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_03.png" width="700" height="719" alt="Our first 30 purchasers have been entered in a draw for a chance to win a brand new Ford F150* . Please join the excitement to learn who the winner is."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_04.jpg" width="700" height="358" alt="Winners Will Be Chosen!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_05.png" width="700" height="414" alt="WP Developments invites you, your friends and family to join us Sunday, June 23rd at 1:00 pm to enjoy a delicious BBQ, beverages and desserts, along with the opportunity to view the spectacular collection of luxurious detached homes that awaits you here in Bowmanville."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><a href="http://thevalleyview.ca/emailers/10947-2019-06-03/thanks.php"><img src="http://thevalleyview.ca/emailers/10947-2019-06-03/images/animation-bubble-confirm.gif" width="700" height="330" alt="Click To Confirm Your Attendance"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_07.png" width="700" height="28" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_08.jpg" width="700" height="522" alt="A Superb Collection of Detached Homes blending contemporary architecture with the quiet character of small town living."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_09.png" width="700" height="493" alt="- Enjoy the best of both town and country - Surrounded by 14 acres of green space - Close to the 401 and the future GO station- Just 30 minutes east of Toronto"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_10.png" width="700" height="111" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_11.png" width="700" height="153" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><a href="https://goo.gl/maps/8djtz9HfZe72"><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_12.png" width="700" height="133" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_13.png" width="700" height="40" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_14.png" width="700" height="38" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_15.png" width="700" height="105" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  ><a href="*|UNSUB|*"><img style="display:block;line-height:0;font-size:0;"   src="http://thevalleyview.ca/emailers/10992-2019-06-11/images/index_16.png" width="700" height="32" alt=""></a></td>
	</tr>
	<tr>
	  <td align="center"   ><p>&nbsp;</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>Friends &amp;  Family BBQ</p>
      <p>Please Join The Excitement</p>
      <p>Winners Will Be Chosen!</p>
      <p>Hurry in and purchase before June 23rd to have a chance to win $25K off the purchase price.</p>
      <p>Plus</p>
      <p>Our first 30 purchasers have been entered in a draw for a chance to win a brand new Ford F150* . Please join the excitement to learn who the winner is.</p>
      <p>One of our sales reps will be in touch with the eligible buyers.</p>
      <p>WP Developments invites you, your friends and family to join us Sunday, June 23rd at 1:00 pm to enjoy a delicious BBQ, beverages and desserts, along with the opportunity to view the spectacular collection of luxurious detached homes that awaits you here in Bowmanville.</p>
      <p><a href="http://thevalleyview.ca/emailers/10947-2019-06-03/thanks.php">Click To Confirm Your Attendance</a></p>
      <p>A Superb Collection of Detached Homes blending contemporary architecture with the quiet character of small town living.</p>
      <p>•	Enjoy the best of both town and country <br>
        •	Surrounded by 14 acres of green space <br>
        •	Close to the 401 and the future GO station<br>
        •	Just 30 minutes east of Toronto</p>
      <p><a href="https://thevalleyview.ca/floorplans.html">Click To View Floor Plans</a></p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72">SALES OFFICE<br>
        2021 Green Rd.<br>
        Bowmanville, On L1C 3K7</a></p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72"><br>
        </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
        Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice.
        *Winners for both contests will be drawn at random on the day of the event (June 23). Winner of the vehicle draw will receive a 3-year paid lease.  Must be 18 years of age or older to qualify. Other restrictions may apply. E.&amp; O.E.</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>