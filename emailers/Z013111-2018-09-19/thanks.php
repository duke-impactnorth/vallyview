<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (Z013111.psd - Slices: 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11) -->
</p>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td style="font-size: 0px;"   colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013111-2018-09-19/images/index_01.png" width="700" height="264" alt="Save the Date for our VIP Preview Opening"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"   colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013111-2018-09-19/images/index_02.png" width="700" height="505" alt="Saturday, September 29th From 11:00 am to 6:00 pm"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"   colspan="2"><img style="display:block;line-height:0;font-size:0;" src="images/index_04a.png" width="700" height="379" alt="Because you registered early, you are invited to attend our VIP Preview Opening before we open to the public. This will be your opportunity to choose your dream home, with VIP Pricing – just for you!The demand will be high and the turn out will be huge so don’t miss this wonderful opportunity."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"   colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013111-2018-09-19/images/index_09.png" width="700" height="203" alt="All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. *Winner of the vehicle draw will receive a 3 year paid lease. Must be 18 years of age or older to qualify. Other restrictions may apply. E.&amp; O.E."></a></td>
	</tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>