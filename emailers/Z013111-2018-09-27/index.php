<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (Z013111.psd) -->
</p>
<table width="700" height="3466" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td   colspan="2" align="center">You are invited to attend our VIP Preview Opening.  
If you are having problems viewing this email properly, please scroll down for the text version.<a href="https://thevalleyview.ca/emailers/Z013063-2018-07-24/" target="_blank"></a></td>
  </tr>
	<tr>
	  <td  colspan="2">&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_01.png" width="700" height="249" alt="Reminder Save the Date for our VIP Preview Opening"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_02.png" width="700" height="536" alt="Saturday, September 29th From 11:00 am to 6:00 pm"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/emailers/Z013111-2018-09-27/thanks.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_03.png" width="700" height="344" alt="Click To Confirm Your Attendance "></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_04.png" width="700" height="399" alt="Because you registered early, you are invited to attend our VIP Preview Opening before we open to the public. This will be your opportunity to choose your dream home, with VIP pricing – just for you!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_05.png" width="700" height="344" alt="Win a brand new Ford F150*!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_06.png" width="700" height="645" alt="Things To Bring  Goverment issued ID  Personal cheques"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/emailers/Z013111-2018-09-27/valleyview-floorplans.pdf" target="_blank"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_07.png" width="700" height="136" alt="Click To View Floor Plans"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_08.jpg" width="700" height="522" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_09.png" width="700" height="108" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://goo.gl/maps/8djtz9HfZe72"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_10.png" width="700" height="68" alt="Sales Office 2021 Green Rd. Bowmanville, On L1C 3K7"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_11.png" width="700" height="26" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_12.png" width="700" height="66" alt="All illustrations are artist's concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. *Winner of the vehicle draw will receive a 3 year paid lease. Must be 18 years of age or older to qualify. Other restrictions may apply. E.&amp; O.E."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_13.png" width="350" height="23" alt=""></a></td>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/index.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_14.png" width="350" height="23" alt=""></a></td>
	</tr>
	<tr>
	  <td colspan="2" align="center"  ><p>&nbsp;</p>
      <p>Reminder</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>Save the Date for our VIP Preview Opening</p>
      <p>Saturday, September 29th<br>
        From 11:00 am to 6:00 pm</p>
      <p><a href="https://thevalleyview.ca/emailers/Z013111-2018-09-27/thanks.php">Click To Confirm Your Attendance </a></p>
      <p>Because you registered early, you are invited to attend our VIP Preview Opening before we open to the public. This will be your opportunity to choose your dream home, with VIP pricing – just for you!</p>
      <p>The demand will be high and the turn out will be huge so don't miss this wonderful opportunity.</p>
      <p>Win a brand new Ford F150*!</p>
      <p>Things To Bring<br>
        &#8226; Goverment issued ID<br>
        &#8226; Personal cheques</p>
      <p>Starting from the 500's</p>
      <p>A Collection of Detached Homeson 33', 37' &amp; 40' Lots</p>
      <p><a href="https://thevalleyview.ca/emailers/Z013111-2018-09-27/valleyview-floorplans.pdf" target="_blank">Click To View Floor Plans</a></p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72">SALES OFFICE<br>
        2021 Green Rd.<br>
        Bowmanville, On L1C 3K7<br>
        Email: info@thevalleyview.ca</a></p>
      <p>All illustrations are artist's concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. *Winner of the vehicle draw will receive a 3 year paid lease. Must be 18 years of age or older to qualify. Other restrictions may apply. E.&amp; O.E.</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>