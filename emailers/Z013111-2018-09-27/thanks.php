<html>
<head>
<title>Contest Rules and Regulations</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
	body { font-family :Helvetica, Arial, "sans-serif"; color: #231F20; font-size: 13px; line-height: 18px}

</style>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (Z013111.psd) -->
</p>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_01.png" width="700" height="249" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_02.png" width="700" height="536" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="images/index_03a.png" width="700" height="344" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><table width="87%" border="0" align="center" cellpadding="0" cellspacing="0">
		  <tbody>
		    <tr>
		      <td><h2>Contest Rules and Regulations:</h2>
		        <p>Ford F150:</p>
		        <p>The first 30 firmed deal will become eligible to enter a draw for a chance to win a brand new Ford F150 (3 year lease) on successful closing of house purchased. Upon receiving the 30th firmed deal, the person eligible for the draw will be contacted by phone or email. The draw will be hosted at Valleyview Sales Office. The participants need to attend the Sales Office in person on the day of draw to participate. </p>
		        <p>Residents of Canada who have a valid unencumbered driver's license in good standing and have reached the age of majority in their province or territory of residence at the time of entry may enter this draw.</p>
		        <p>Limit of one submission per house purchase.</p>
		        <p>There is one (1) Grand Prize available to be won from all valid entries received, consisting of: Three Year Lease on a Ford F150 XL base model with manufacturer's suggested retail price of $26,000 CAD or less.</p>
		        <p>The Grand Prize includes destination/freight charges. Colour, accessories, and other specifics of the Grand Prize will be at WP Development's sole discretion. Manufacturer's warranty applies to the Grand Prize. Upon notification, winner will personally take delivery of the Grand Prize from an assigned Ford Canada dealership within Canada within two (2) months of being declared the winner. Winner must present adequate personal identification to claim the Grand Prize vehicle. The Grand Prize vehicle will not be released unless winner first shows proof of having a valid driver's licence in the province in which he/she resides and satisfactory insurance, as determined by Ford Canada/WP Development. Winner is solely responsible for all taxes and expenses which are not included in the Grand Prize description above, including but not limited to: registration and license fees, insurance, additional accessories and all other costs incurred in claiming, registering or using the vehicle. Winner is responsible for all costs associated with any upgrade or option packages he/she may request and which WP Development, in its sole discretion, is willing to provide or has provided respecting any vehicle. WP Development, at its discretion, may substitute a 2020/2021 model year vehicle of the same or like model and style in the event of lack of vehicle availability due to limited production or any other reason.</p>
		        <p>Any costs or expenses incurred by the winner in claiming or using the Grand Prize will be the responsibility of the winner. Grand Prize must be accepted as described in these Rules and cannot be transferred to another person, substituted for another prize, or exchanged in whole or in part for cash, subject to the provisions set forth elsewhere in these Rules.</p>
		        <p>Winner of the Grand Prize needs to sign a Declaration and Release form and send back to WP Development.</p>
		        <p>By entering this Draw, the winner authorizes WP Development and its representatives to use in any related publicity, if required, his/her name, photograph, image and any statements he/she may take regarding the Grand Prize, place of residence and/or voice for advertising purposes in any form of media including the Internet, without any form of compensation, or further notice, except where prohibited by law.</p>
		        <p>In the event that, for reasons beyond their control and not related to the winners, WP Development is unable to award the Grand Prize as described in these Rules, they will agree to substitute a Grand Prize of similar nature and equal or greater value.</p>
		        <p>This Terms and Conditions is subject to change. WP Development reserve the right to amend or modify this Terms and Conditions at any time.</p>
	          <p>Privacy Statement: WP Development is committed to protect your privacy. Personal information collected will be used to administer and evaluate the draw, including fulfilling draw prizes and contacting and publicizing the name and municipality of residence of prize winner. We may also use your person information to connect with you to share other news, coming events of WP Development. If you wish to be removed from our contact list; Please email <a href="mailto:info@wpdevelopment.ca">info@wpdevelopment.ca</a>. </p></td>
	        </tr>
	      </tbody>
      </table></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013111-2018-09-27/images/index_09.png" width="700" height="108" alt=""></a></td>
	</tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>