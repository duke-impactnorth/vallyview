<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
	body { font-family :Helvetica, Arial, "sans-serif"; color: #231F20; font-size: 13px; line-height: 18px}

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114987483-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114987483-1');
</script>


</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (WEAL-VAL-W-EM-Z013111-SaveTheDate_5C[1]-2.pdf) -->
</p>
<table width="648" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_01.png" width="700" height="236" alt="Valleyview Bowmanville"></a></td>
  </tr>
	<tr>
		<td><a href="https://thevalleyview.ca/"><span style="font-size: 0px;"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_02.png" width="700" height="539" alt="You’re Invited to our Grand Opening Saturday, October 27th At 11 am Sharp!"></span></a></td>
	</tr>
	<tr>
		<td>
			<img src="images/thanks_02.png" width="648" height="363" alt=""></td>
	</tr>
	<tr>
		<td><p>No purchase necessary to enter. Limit of one person per submission when attending Valleyview Bowmanville Sales Office during the Grand Opening weekend.</p>
		  <p>Only tickets filled from Oct 27th to Oct 28th before 6pm will qualify for the draw. The draw will take place on Oct 28th at approx 6:05pm in Valleyview Bowmanville Sales Office by on site sales representatives.</p>
		  <p>Winners will be notified by email with email provided on the ticket. Prizes will be awarded to the individual named on the ticket. It is the responsibility of the primary tickethoder to allocate the prize if necessary.</p>
		  <p>Participants must be at least 18 years or older.</p>
		  <p>Vacation prizes will be awarded as a voucher to the primary ticketholder for the value of the trip as advertised. the prize winner may contact the prize supplier to make travel arrangements should they wish to take the trip as described. Voucher has no cash value.</p>
		  <p>Prize winner is asked to claim prizes in a timely manner following notification. Any prizes which are not claimed shall be secured for a period of (1) month from the date of the draw. Unclaimed prizes will be null and void.</p>
		  <p>This Terms and Conditions is subject to change. We reserve the right to amend or modify this Terms and Conditions at any time.</p>
		  <p>Privacy Statement: WP Development is committed to protect your privacy. Personal information collected will be used to administer and evaluate the draw, including fulfilling draw prizes and contacting and publicizing the name and municipality of residence of prize winner. We may also use your personal information to connect with you to share other news, coming events of WP Development. If you wish to be removed from our contact list; Please email</p>
		  <p><a href="mailto:info@wpdevelopment.ca">info@wpdevelopment.ca</a>.</p>
		  <p> <span style="color:#F09B1F; font-size:25px; font-weight: 500">Ford F150:</span><br>
		    The first 30 firmed deal will become eligible to enter a draw for a chance to win a brand new Ford F150 (3 year lease) on successful closing of house purchased. Upon receiving the 30th firmed deal, the person eligible for the draw will be contacted by phone or email. The draw will be hosted at Valleyview Sales Office. The participants need to attend the Sales Office in person on the day of draw to participate. <br>
            <br>
          Residents of Canada who have a valid unencumbered driver's license in good standing and have reached the age of majority in their province or territory of residence at the time of entry may enter this draw.</p>
		  <p>Limit of one submission per house purchase.</p>
		  <p>There is one (1) Grand Prize available to be won from all valid entries received, consisting of: Three Year Lease on a Ford F150 XL base model with manufacturer's suggested retail price of $26,000 CAD or less.</p>
		  <p>The Grand Prize includes destination/freight charges. Colour, accessories, and other specifics of the Grand Prize will be at WP Development's sole discretion. Manufacturer's warranty applies to the Grand Prize. Upon notification, winner will personally take delivery of the Grand Prize from an assigned Ford Canada dealership within Canada within two (2) months of being declared the winner. Winner must present adequate personal identification to claim the Grand Prize vehicle. The Grand Prize vehicle will not be released unless winner first shows proof of having a valid driver's licence in the province in which he/she resides and satisfactory insurance, as determined by Ford Canada/WP Development. Winner is solely responsible for all taxes and expenses which are not included in the Grand Prize description above, including but not limited to: registration and license fees, insurance, additional accessories and all other costs incurred in claiming, registering or using the vehicle. Winner is responsible for all costs associated with any upgrade or option packages he/she may request and which WP Development, in its sole discretion, is willing to provide or has provided respecting any vehicle. WP Development, at its discretion, may substitute a 2020/2021 model year vehicle of the same or like model and style in the event of lack of vehicle availability due to limited production or any other reason.</p>
		  <p>Any costs or expenses incurred by the winner in claiming or using the Grand Prize will be the responsibility of the winner. Grand Prize must be accepted as described in these Rules and cannot be transferred to another person, substituted for another prize, or exchanged in whole or in part for cash, subject to the provisions set forth elsewhere in these Rules.</p>
		  <p>Winner of the Grand Prize needs to sign a Declaration and Release form and send back to WP Development.</p>
		  <p>By entering this Draw, the winner authorizes WP Development and its representatives to use in any related publicity, if required, his/her name, photograph, image and any statements he/she may take regarding the Grand Prize, place of residence and/or voice for advertising purposes in any form of media including the Internet, without any form of compensation, or further notice, except where prohibited by law.</p>
		  <p>In the event that, for reasons beyond their control and not related to the winners, WP Development is unable to award the Grand Prize as described in these Rules, they will agree to substitute a Grand Prize of similar nature and equal or greater value.<br>
		    This Terms and Conditions is subject to change. WP Development reserve the right to amend or modify this Terms and Conditions at any time.</p>
		  <p>Privacy Statement: WP Development is committed to protect your privacy. Personal information collected will be used to administer and evaluate the draw, including fulfilling draw prizes and contacting and publicizing the name and municipality of residence of prize winner. We may also use your person information to connect with you to share other news, coming events of WP Development. If you wish to be removed from our contact list; Please email <a href="mailto:info@wpdevelopment.ca">info@wpdevelopment.ca</a>. </p></td>
	</tr>
	<tr>
		<td><a href="http://wpdevelopment.ca/"><img src="images/thanks_04.png" width="648" height="89" alt=""></a></td>
	</tr>
</table>
<!-- End Save for Web Slices -->

<!-- Drip -->
<script type="text/javascript">
  var _dcq = _dcq || [];
  var _dcs = _dcs || {};
  _dcs.account = '2982259';

  (function() {
    var dc = document.createElement('script');
    dc.type = 'text/javascript'; dc.async = true;
    dc.src = '//tag.getdrip.com/2982259.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(dc, s);
  })();
</script>
<!-- end Drip -->
</body>
</html>