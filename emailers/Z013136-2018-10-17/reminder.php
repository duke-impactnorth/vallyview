<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (Z013136.psd) -->
</p>
<table width="700" height="3599" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td  colspan="2" align="center">A Collection of Detached Homes. If you are having problems viewing this email properly, please scroll down for the text version.<a href="https://thevalleyview.ca/emailers/Z013063-2018-07-24/" target="_blank"></a></td>
  </tr>
	<tr>
	  <td  colspan="2">&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_01a.png" width="700" height="343" alt="Valleyview Bowmanville"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_02a.png" width="700" height="539" alt="You’re Invited to our Grand Opening Saturday, October 27th At 11 am Sharp!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><strong><a href="https://thevalleyview.ca/emailers/Z013111-2018-10-10/thanks2.php"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/GrandOpening-animation.gif" width="700" height="289" alt="Click To Confirm Your Attendance "></a></strong></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://goo.gl/maps/8djtz9HfZe72"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_04.png" width="700" height="179" alt="SALES OFFICE 2021 Green Rd. Bowmanville, On L1C 3K7"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_05.png" width="700" height="45" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_06.png" width="700" height="371" alt="Attend for a chance to win an all-inclusive Trip for Two*"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_07.png" width="700" height="574" alt="Starting from the 500’s Things To Bring • Goverment issued ID • Personal cheques"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_08.png" width="700" height="242" alt="A Collection of Detached Homes - Click To View Floorplans"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_09.jpg" width="700" height="523" alt="Win A Brand New Ford F150*! The first 30 firm deals will be entered in a big draw for a chance to win a brand new Ford F150*. "></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_10.png" width="700" height="345" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_11.png" width="700" height="131" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_12.png" width="700" height="103" alt="All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. *Winner of the vehicle draw will receive a 3 year paid lease. Must be 18 years of age or older to qualify. Other restrictions may apply. E.&amp; O.E."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_13.png" width="350" height="22" alt=""></a></td>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013136-2018-10-17/images/index_14.png" width="350" height="22" alt=""></a></td>
	</tr>
	<tr>
	  <td colspan="2" align="center" ><p>&nbsp;</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>You’re Invited to our Grand Opening</p>
      <p>Start's Tomorrow at 11 am Sharp!</p>
      <p><a href="https://thevalleyview.ca/emailers/Z013111-2018-10-10/thanks2.php">Click To Confirm Your Attendance </a></p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72">SALES OFFICE<br>
        2021 Green Rd.<br>
        Bowmanville, On L1C 3K7<br>
        </a>Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
      <p>Attend for a chance to win an all-inclusive Trip for Two*</p>
      <p>Starting from the 500’s</p>
      <p>Things To Bring<br>
        • Goverment issued ID<br>
        • Personal cheques</p>
      <p>A Collection of Detached Homes - <a href="https://thevalleyview.ca/floorplans.html">Click To View Floorplans</a></p>
      <p>Win A Brand New Ford F150*!<br>
        The first 30 firm deals will be entered in a big draw for a chance to win a brand new Ford F150*. </p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. *Winner of the vehicle draw will receive a 3 year paid lease. Must be 18 years of age or older to qualify. Other restrictions may apply. E.&amp; O.E.</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>