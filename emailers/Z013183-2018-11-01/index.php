<html>
<head>
<title>A Collection of Detached Homes</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (Z013136.psd) -->
</p>
<table width="700" height="2881" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td   colspan="2" align="center"><p>A Collection of Detached Homes. Starting from the $500’s.
      If you are having problems viewing this email properly, please scroll down for the text version.<a href="https://thevalleyview.ca/emailers/Z013063-2018-07-24/" target="_blank"></a></p></td>
  </tr>
	<tr>
	  <td  colspan="2">&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_02.png" width="700" height="527" alt="Valleyview Bowmanville Sales Office Now Open"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_03.png" width="700" height="620" alt="Hours Of Operation Monday - Thursday1 pm - 7 pm Saturday &amp; Sunday11 am - 5 pm Friday - Closed"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_04.jpg" width="700" height="528" alt="A Collection of Detached Homes Starting from the $500’s"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_05.png" width="700" height="382" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/animation-click.gif" width="700" height="120" alt="Click To View Floorplans"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://goo.gl/maps/8djtz9HfZe72"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_07a.png" width="700" height="189" alt="SALES OFFICE 2021 Green Rd. Bowmanville, ON L1C 3K7 "></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_08.png" width="700" height="46" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_09.png" width="700" height="51" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_10.png" width="700" height="81" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_11.png" width="700" height="88" alt="All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. E.&amp;O.E."></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_12.png" width="700" height="111" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_13.png" width="350" height="19" alt="unsubscript from this community"></a></td>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;" src="https://thevalleyview.ca/emailers/Z013183-2018-11-01/images/index_14.png" width="350" height="19" alt="unsubscribe from WP development"></a></td>
	</tr>
	<tr>
	  <td colspan="2" align="center"  ><p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>Sales Office Now Open</p>
      <p>Hours Of Operation<br>
        Monday - Thursday1 pm - 7 pm<br>
        Saturday &amp; Sunday11 am - 5 pm<br>
        Friday - Closed</p>
      <p>A Collection of Detached Homes</p>
      <p>Starting from the $500’s</p>
      <p><a href="https://thevalleyview.ca/floorplans.html">Click To View Floorplans</a></p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72">SALES OFFICE<br>
        2021 Green Rd.<br>
        Bowmanville, ON L1C 3K7<br></a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. E.&amp; O.E.</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>