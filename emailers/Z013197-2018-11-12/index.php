<html>
<head>
<title>A Collection of Detached Homes</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (Z013197.psd) -->
</p>
<table width="730" height="3925" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td  colspan="2" align="center">A Collection of Detached Homes. Starting from the $500’s.
      If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td   colspan="2">&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_01.png" width="730" height="377" alt="Breaking News! Our Grand Opening Was A Huge Success!"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_02.png" width="730" height="199" alt="Due To The High Demand, New Lots Released! You Asked... We Listened."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_03.jpg" width="730" height="618" alt="Our Grand Opening attracted hundreds of people and the buyers loved the designs offered at Valleyview. The homes sold quickly and due to high demand, new lots have been released this week. Come in today and find your dream home!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_04.png" width="730" height="616" alt="Hurry, They Won’t Last Long!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_05.png" width="730" height="121" alt="Click To View Floor Plans"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_06.png" width="730" height="631" alt="Hours Of Operation  Monday - Thursday 1 pm - 7 pm Saturday &amp; Sunday 11 am - 5 pm Friday - Closed"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_07.jpg" width="730" height="454" alt="A Collection of Detached Homes Starting from the $500’s"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_08.png" width="730" height="512" alt="SALES OFFICE 2021 Green Rd. Bowmanville, ON L1C 3K7"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_09.png" width="730" height="48" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_10.png" width="730" height="50" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_11.png" width="730" height="277" alt="All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications, terms and conditions are subject to change without notice. E.&amp; O.E."></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_12.png" width="365" height="22" alt=""></a></td>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/index.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013197-2018-11-12/images/index_13.png" width="365" height="22" alt=""></a></td>
	</tr>
	<tr>
	  <td  colspan="2" align="center"><p>&nbsp;</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>Breaking News!</p>
      <p>Our Grand Opening Was A Huge Success!</p>
      <p>Due To The High Demand, New Lots Released!</p>
      <p>You Asked... We Listened.</p>
      <p>Our Grand Opening attracted hundreds of people and the buyers loved the designs offered at Valleyview. The homes sold quickly and due to high demand, new lots have been released this week. Come in today and find your dream home!</p>
      <p>Hurry, They Won’t Last Long!</p>
      <p><a href="https://thevalleyview.ca/floorplans.html">Click To View Floor Plans</a></p>
      <p>Hours Of Operation<br>
        Monday - Thursday  1 pm - 7 pm<br>
        Saturday &amp; Sunday  11 am - 5 pm<br>
        Friday -   Closed</p>
      <p>A Collection of Detached Homes</p>
      <p>Starting from the $500’s</p>
      <p><a href="https://thevalleyview.ca/floorplans.html">Click To View Floorplans</a></p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72">SALES OFFICE<br>
        2021 Green Rd.<br>
        Bowmanville, ON L1C 3K7<br>
        </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
        Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications,   terms and conditions are subject to change without notice. E.&amp; O.E.</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>