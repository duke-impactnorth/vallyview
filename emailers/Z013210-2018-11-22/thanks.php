<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p><!-- Save for Web Slices (Z013210.psd) -->
</p>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013210-2018-11-22/images/index_01.png" width="700" height="222" alt="Meet Santa Valleyview would like to thank all of our precious customers by inviting Santa to our Sales Office on Saturday, December 8th from 1pm to 4pm."></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013210-2018-11-22/images/index_02.jpg" width="700" height="473" alt="Due To The High Demand, New Lots Released!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="images/index_03a.png" width="700" height="394" alt="You asked... we listened."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="images/index_10a.png" width="700" height="93" alt="Starting from the upper $500’s"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://goo.gl/maps/8djtz9HfZe72"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013210-2018-11-22/images/index_11.png" width="700" height="134" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013210-2018-11-22/images/index_12.png" width="700" height="45" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013210-2018-11-22/images/index_13.png" width="700" height="56" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013210-2018-11-22/images/index_14.png" width="700" height="273" alt=""></a></td>
	</tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>