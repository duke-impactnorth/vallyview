<html>
<head>
<title>Valleyview Bowmanville</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center">
<table width="700" height="4095" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
	  <td   colspan="2" align="center">Come in today and find your dream home!
      If you are having problems viewing this email properly, please scroll down for the text version.</td>
  </tr>
	<tr>
	  <td   colspan="2">&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_01.png" width="700" height="421" alt="Meet Santa Valleyview would like to thank all of our precious customers by inviting Santa to our Sales Office on Saturday, December 8th from 1pm to 4pm."></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_02.jpg" width="700" height="473" alt="Due To The High Demand, New Lots Released!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_03.png" width="700" height="324" alt="You asked... we listened."></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_03b.png" width="700" height="253" alt="You asked... we listened."></td>
	</tr>
	<tr>
	  <td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/emailers/Z013241-2018-12-05/thanks.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/animation-click.gif" width="700" height="100" alt="Our Grand Opening attracted hundreds of people and the buyers loved the designs offered at Valleyview. The homes sold quickly and due to high demand, new lots have been released this week. Come in today and find your dream home!"></a></td>
  </tr>
	<tr>
	  <td height="60"  colspan="2" style="font-size: 0px;">&nbsp;</td>
  </tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_04.png" width="700" height="380" alt="Our Grand Opening attracted hundreds of people and the buyers loved the designs offered at Valleyview. The homes sold quickly and due to high demand, new lots have been released this week. Come in today and find your dream home!"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_05.png" width="700" height="422" alt="Hours Of Operation Monday - Thursday 1 pm - 7 pm Saturday &amp; Sunday 11 am - 5 pm Friday - Closed"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/floorplans.html">
		<a href="https://thevalleyview.ca/floorplans.html" target="_blank">
			<img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_06.png" width="700" height="120" alt="Click To View Floor Plans">
		</a>
		</td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_07.png" width="700" height="525" alt="SALES OFFICE 2021 Green Rd. Bowmanville, ON L1C 3K7"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://thevalleyview.ca/floorplans.html"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_08.png" width="700" height="105" alt="A Collection of Detached Homes"></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_09.jpg" width="700" height="454" alt=""></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_10.png" width="700" height="89" alt="Starting from the upper $500’s"></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="https://goo.gl/maps/8djtz9HfZe72"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_11.png" width="700" height="134" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="tel:905-419-6888"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_12.png" width="700" height="45" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="mailto:info@thevalleyview.ca"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_13.png" width="700" height="56" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;"  colspan="2"><a href="http://wpdevelopment.ca/"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_14.png" width="700" height="273" alt=""></a></td>
	</tr>
	<tr>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_15.png" width="350" height="21" alt=""></a></td>
		<td style="font-size: 0px;" ><a href="https://thevalleyview.ca/emailers/unsubscribe/valleyview.php"><img style="display:block;line-height:0;font-size:0;"  src="https://thevalleyview.ca/emailers/Z013241-2018-12-05/images/index_16.png" width="350" height="21" alt=""></a></td>
	</tr>
	<tr>
	  <td  colspan="2" align="center"><p>&nbsp;</p>
      <p><a href="https://thevalleyview.ca/">Valleyview Bowmanville</a></p>
      <p>Meet Santa</p>
      <p>Valleyview would like to thank all of our precious customers by inviting Santa to our Sales Office on Saturday, December 8th from 1pm to 4pm.</p>
      <p><strong>ONE COMPLIMENTARY PROFESSIONAL PHOTO INCLUDED FOR EACH FAMILY.</strong></p>
      <p><a href="https://thevalleyview.ca/emailers/Z013217-2018-11-28/thanks.php">Click To RSVP</a></p>
      <p>Due To The High Demand, New Lots Released!</p>
      <p>You asked... we listened.</p>
      <p>Our Grand Opening attracted hundreds of people and the buyers loved the designs offered at Valleyview. The homes sold quickly and due to high demand, new lots have been released this week. Come in today and find your dream home!</p>
      <p>Hurry, they won’t last long!</p>
      <p><a href="https://thevalleyview.ca/floorplans.html">Click To View Floor Plans</a></p>
      <p>Hours Of Operation<br>
        Monday - Thursday  1 pm - 7 pm<br>
        Saturday &amp; Sunday  11 am - 5 pm<br>
        Friday -   Closed</p>
      <p>A Collection of Detached Homes</p>
      <p><a href="https://goo.gl/maps/8djtz9HfZe72">SALES OFFICE<br>
        2021 Green Rd.<br>
        Bowmanville, ON L1C 3K7<br>
        </a>Phone: <a href="tel:905-419-6888">905-419-6888</a><br>
        Email: <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a></p>
      <p><a href="http://wpdevelopment.ca/">WP Development</a></p>
      <p>All illustrations are artist’s concept. All dimensions are approximate. Prices, specifications,   terms and conditions are subject to change without notice. E.&amp; O.E.</p></td>
  </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>