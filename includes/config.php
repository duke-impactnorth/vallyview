<?php

/***************************************************************
*                                                              *
*                                                              *
*              Global Constants                                *
*                                                              *
*                                                              *
***************************************************************/
define('STATUS_LOCAL', -1);
define('STATUS_STAGING', 1);
define('STATUS_LIVE', 2);


/***************************************************************
*                                                              *
*                                                              *
*              Database Configuration                          *
*                                                              *
*                                                              *
***************************************************************/
if (stripos($_SERVER['HTTP_HOST'],"valleyview.tst") !== false) {
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => 'root',
			'password' => '',
			'database' => 'impactno_valleyview'

		),
		'Site' => array(
			'name' => 'Valleyview',
			'url' => 'http://valleyview.tst:8084',
			'email' => 'davidh@impactnorth.com',
			'live' => STATUS_LOCAL
		)
	);
}
elseif (stripos($_SERVER['HTTP_HOST'], "localhost") !== false) {
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => 'root',
			'password' => '',
			'database' => 'impactno_valleyview'
		),
		'Site' => array(
			'name' => 'Valleyview',
			'url' => 'http://localhost/valleyview',
			'email' => 'davidh@impactnorth.com',
			'live' => STATUS_LOCAL
		)
	);
}
elseif (stripos($_SERVER['HTTP_HOST'], "production.impactnorth.com") !== false) {
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => 'impactno_valleyv',
			'password' => 'W41H0cr2lF307EkEj7',
			'database' => 'impactno_valleyview'
		),
		'Site' => array(
			'name' => 'Valleyview',
			'url' => 'https://production.impactnorth.com/valleyview',
			'email' => 'davidh@impactnorth.com',
			'live' => STATUS_LOCAL
		)
	);
}
elseif (stripos($_SERVER['HTTP_HOST'], "thevalleyview.ca") !== false) {
	//LIVE
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => 'thvllyvw_valleyv',
			'password' => 'zxe78QZSJm6RxgLDRVVkQ2rmgR6uaBgsqmGgmrUj8dmrW2KvZPhxSjkFFqwbQ7xsd8z79wNpPNDWWXGRfHereKq5fPeeNLJmWsDy',
			'database' => 'thvllyvw_db1'
		),
		'Site' => array(
			'name' => 'Valleyview',
			'url' => 'https://thevalleyview.ca',
			'email' => 'davidh@impactnorth.com',
			'live' => STATUS_LIVE
		)
	);
}
elseif (stripos($_SERVER['HTTP_HOST'], "valleyviewbowmanville.com") !== false) {
	//LIVE
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => '',
			'password' => '',
			'database' => ''
		),
		'Site' => array(
			'name' => 'Valleyview',
			'url' => 'https://valleyviewbowmanville.com',
			'email' => 'davidh@impactnorth.com',
			'live' => STATUS_LIVE
		)
	);
}
elseif (stripos($_SERVER['HTTP_HOST'], "valleyviewbowmanville.ca") !== false) {
	//LIVE
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => '',
			'password' => '',
			'database' => ''
		),
		'Site' => array(
			'name' => 'Valleyview',
			'url' => 'https://valleyviewbowmanville.ca',
			'email' => 'davidh@impactnorth.com',
			'live' => STATUS_LIVE
		)
	);
}
/***************************************************************
*                                                              *
*                                                              *
*              Config Constants                                *
*                                                              *
*                                                              *
***************************************************************/
//Database Info
define('DB_SERVER', $config['Database']['server']);
define('DB_CHARSET', 'utf8mb4');
define('DB_USER', $config['Database']['username']);
define('DB_PASS', $config['Database']['password']);
define('DB_NAME', $config['Database']['database']);

//Site Info
define('SITE_URL', $config['Site']['url']);
define('SITE_NAME', $config['Site']['name']);
define('SITE_EMAIL', $config['Site']['email']);
define('SITE_LIVE', $config['Site']['live']);

//Directories
define('SITE_IMAGES', SITE_URL . "/images/");
define('SITE_CSS', SITE_URL . "/css/");
define('SITE_JS', SITE_URL . "/js/");

?>