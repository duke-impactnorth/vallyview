<?php

$refresh_url = 'https://thedelve.ca/recent-instagram-post.php?client_id=217';



//Get root directory
$current_file = $_SERVER['SCRIPT_FILENAME'];
$current_directory = substr($current_file, 0, strripos(rtrim($current_file,"pages/"), "pages/"));

if(empty($current_directory)){
	$current_directory = substr($current_file, 0, strripos(rtrim($current_file,"/"), "/"));
}

//Determine files locations
$full_file_path = $current_directory . "/includes/data.json";
$file_refresh_time = 720; //12 hours

//check if file exists
$minutes_difference = -1;
if (file_exists($full_file_path) && filesize($full_file_path)) {
	$last_modified = new Datetime(date("Y-m-d H:i:s", filemtime($full_file_path)));
	$current_dt = (new Datetime())->diff($last_modified);

	//Difference in minutes
	$minutes_difference = ($current_dt->days * 24 * 60) + ($current_dt->h * 60) + $current_dt->i;
}

//d($refresh_url, $minutes_difference);
//If the file doesn't exist for some reason, re-create it.
//If the file is older than $file_refresh_time, update it.
if ($minutes_difference < 0 || $minutes_difference > $file_refresh_time) {
	//reload from website
	$json_string = file_get_contents($refresh_url);

	//Save into file
	// var_dump($minutes_difference);
	// var_dump($full_file_path);
	// var_dump($json_string);
	file_put_contents($full_file_path, $json_string);
}
else{
	//just get it from the file
	$json_string = file_get_contents($full_file_path);
}


// var_dump($minutes_difference);
$json_arr = json_decode($json_string);

?>



<footer>
	<div class="container-fluid bg-white">
		<div class="container">
			<div class="row justify-content-center pb-4">
        <div class="col-12 text-center">
				<a href="https://www.instagram.com/valleyview_bowmanville/" target="_blank" rel="noopener noreferrer">
          <img src="<?= SITE_IMAGES ?>instagram-logo.png" alt="" class="img-instagram-widget">
				</a>

        </div>
				<div class="col-12 col-lg-8 col-xl-6 d-flex flex-wrap justify-content-center align-items-center">
					<?php foreach($json_arr as $key => $value_array){ ?>
							<a href="https://instagram.com/p/<?= $value_array->shortcode; ?>" target="_blank" rel="noopener noreferrer">
								<div class="div-instagram-square" style="background: url('<?= $value_array->display_url; ?>');   background-position: center center;background-size: cover;background-repeat: no-repeat;">
								</div>
							</a>

					<?php } ?>
				</div>
			</div>




			<div class="row">
				<div class="col-12 col-sm-12 col-md-6 col-lg-3 pt-1">
					<a href="http://wpdevelopment.ca/" target="_blank">
						<img src="<?= SITE_IMAGES ?>logos/logo_wp_developments.jpg" class="mx-auto d-block ml-md-0 logo img-grow" alt="">
					</a>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<p class="text-copyright text-center text-sm-left text-md-left "><strong>Sales Office</strong><br />
						<a href="https://goo.gl/maps/mWJAzBwz75NiAKuy5" target="_blank" rel="noopener noreferrer">2021 Green Rd., Bowmanville,<br />
							ON L1C 3K7</a></p>
					<p class="text-copyright text-center text-sm-left text-md-left"><strong>Email:</strong> <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a><br />
						<!-- <strong>Phone:</strong> <a href="tel:+1905-419-6888">905-419-6888</a></p> -->
						<strong>Phone:</strong> <a href="tel:+1905-597-0466">905.597.0466</a></p>

				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<p class="text-copyright text-center text-sm-left  text-md-left">
						<strong>Office Hours:</strong><br />
						By Appointment Only
						<?php /*
						<strong>Mon-Thur:</strong> 1pm - 7pm<br />
						<strong>Friday:</strong> CLOSED<br />
						<strong>Sat/Sun/Holidays:</strong> 11am - 5pm
						*/
						?>

					</p>
					<!-- <p class="text-copyright text-center text-md-left">Holiday Hours <br />sales office is closed from <br />December 21<sup>st</sup> – January 4<sup>th</sup></p> -->
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-3">
        <div class="div-hr-line"></div>
					<p class="text-copyright text-center text-md-left ">Copyright © <?= date('Y') ?> WP Development and built by Ballantry Homes. All rights reserved.</p>
					<!-- <p class=" text-center text-md-left"><a  data-toggle="modal" data-target="#privacyPolicy" href="#">PRIVACY POLICY</a></p> -->
					<p class=" text-center text-md-left mb-0 mb-md-1"><a href="privacy-policy.html">PRIVACY POLICY</a></p>
					<p class=" text-center text-md-left">
						<a data-toggle="modal" data-target="#privacyPolicy" href="#">DISCLAIMER</a>
					</p>
				</div>
			</div>
		</div>
	</div>

	<!--Privacy Policy  Modal -->
	<div class="modal fade bd-example-modal-lg" id="privacyPolicy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">DISCLAIMER</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					This Campaign is open only to those who purchased from March 30th, 2019 to the end of second release for *Valleyview or further notice from WP Development Inc. and who are 19 years of age or older as of the date of entry.The Campaign is only open to legal residents of Canada, and is void where prohibited by law. Entries will be accepted at 104-2021 Green Rd, Bowmanville.The Purchaser(s) is/are automatically entered after a home is purchased, and only become valid when all conditions (mortgage and lawyer review of contract) are fulfilled. Prize will be given on/after successful closing of the house purchased.Every Purchaser(s) will receive the following: bonus package (approximate value $25,000), luxury features (approximate value $25,000). One in every 10 successful buyers will enter a draw to win a ($25,000) deduction off purchase price on closing of house purchased. The sum of the total giveaway is approximately $1,000,000.00. Actual/appraised value may differ at time of prize award. The odds of winning depend on the number of eligible entries received. The specifics of the prize shall be solely determined by WP Development Inc.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script src="<?= SITE_JS ?>jquery-3.4.1.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?= SITE_JS ?>bootstrap.min.js?v=1.1"></script>

<script src="<?= SITE_JS ?>moby.js"></script>

<script src="<?= SITE_JS ?>main.js?v=1.2"></script>



<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114987483-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'UA-114987483-1');
</script>
