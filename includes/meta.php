<?php include ("config.php"); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	 <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


	<link href="<?= SITE_CSS ?>bootstrap.min.css?ver=2.1" rel="stylesheet">

	<link href="<?= SITE_IMAGES ?>favicon.png" rel="shortcut icon" type="image/x-icon"  />
	<link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link href="<?= SITE_CSS ?>moby.css" rel="stylesheet">
  <link href="<?= SITE_CSS ?>custom.css?ver=9.32" rel="stylesheet">


  <?php if (STATUS_LIVE == SITE_LIVE){ ?>
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2146428142333140');
        fbq('track', 'PageView');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114987483-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-114987483-1');
	</script>
 	<!-- Global site tag (gtag.js) - Google AdWords: 805980863 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-805980863"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'AW-805980863');
	</script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2146428142333140&ev=PageView&noscript=1" /></noscript>
  <?php } ?>