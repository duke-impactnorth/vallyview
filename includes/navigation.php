<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white bg-nav" id="main-nav">
	<a class="navbar-brand" href="<?= SITE_URL ?>/">
		<img src="<?= SITE_IMAGES ?>logos/Valleyview-Logo.png" class="img-grow img-nav-logo" alt="Valleyview Bowmanville">
	</a>
	<a class="moby-navbar-brand" href="<?= SITE_URL ?>/">
		<img src="<?= SITE_IMAGES ?>index/leaf-left.png" class="img-grow img-moby-nav-logo d-none" alt="">
	</a>


	<div class="d-block d-lg-none" id="nav-icon">
		<span></span>
		<span></span>
		<span></span>
	</div>

	<div id="navbarNavDropdown" class="navbar-collapse collapse">
		<ul class="navbar-nav mr-auto">
			<li></li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item px-0 px-xl-2" id="nav-floorplan">
				<a class="nav-link" href="<?= SITE_URL ?>/floorplans.html">Floorplans</a>
			</li>
			<li class="nav-item px-0 px-xl-2" id="nav-site-plan">
				<a class="nav-link" href="<?= SITE_URL ?>/site-plan.html">Site Plan</a>
			</li>

			<li class="nav-item px-0 px-xl-2" id="nav-interiors">
				<a class="nav-link" href="<?= SITE_URL ?>/interiors.html">Interiors</a>
			</li>
			<li class="nav-item px-0 px-xl-2" id="nav-area-amenities">
				<a class="nav-link" href="<?= SITE_URL ?>/area-amenities.html">Area Amenities</a>
			</li>
			<li class="nav-item px-0 px-xl-2" id="nav-the-team">
				<a class="nav-link" href="<?= SITE_URL ?>/the-team.html">The Team</a>
			</li>
			<li class="nav-item px-0 px-xl-2" id="nav-location">
				<a class="nav-link" href="<?= SITE_URL ?>/location.html">Location</a>
			</li>
			<li class="nav-item px-0 px-xl-2" id="nav-news">
				<a class="nav-link" href="<?= SITE_URL ?>/news.html">News</a>
			</li>
			<li class="px-0 px-xl-2 nav-item li-nav-register animated flash" id="nav-register">
				<a class="nav-link btn-navbar-register" href="<?= SITE_URL ?>/register.html">Register</a>
			</li>
		</ul>
	</div>

</nav>
