<?php
/*
Add the news post snippets here
*/

$news_array = array(
  'sales-office-open-final' => [
    "title" => "Valleyview Bowmanville Reopens Its Sales Centre to the Public",
    "excerpt" => "Valleyview Bowmanville has opened its doors to the public as of Saturday...",
    "date" => "06/30/20",
    "image" => SITE_IMAGES . 'posts/' . "sales-office-open-final.jpg",
    "url" => SITE_URL . "/posts/sales-office-reopening.html",
  ],
  'donation-to-local-clarington-food-banks' => [
    "title" => "Valleyview in Bowmanville Donates $30,000 to Local Clarington Food Banks",
    "excerpt" => "WP Development is a proud partner in the Bowmanville/Clarington com...",
    "date" => "05/13/20",
    "image" => SITE_IMAGES . 'posts/' . "donation-to-local-clarington-food-banks.jpg",
    "url" => SITE_URL . "/posts/donation-to-local-clarington-food-banks.html",
  ],
  'online-and-in-person-appointments' => [
    "title" => "Valleyview Bowmanville Status Update – Online and In-Person Appointments",
    "excerpt" => "At Valleyview, the well-being of our employees...",
    "date" => "03/26/20",
    "image" => SITE_IMAGES . 'posts/' . "online-and-in-person-appointments.jpg",
    "url" => SITE_URL . "/posts/online-and-in-person-appointments.html",
  ],
  'things-to-do-in-bowmanville-during-march-break' => [
    "title" => "Things to do in Bowmanville during March Break",
    "excerpt" => "Save your money on traveling and bring fun to you in Bowmanville...",
    "date" => "03/11/20",
    "image" => SITE_IMAGES . 'posts/' . "things-to-do-in-bowmanville-during-march-break.jpg",
    "url" => SITE_URL . "/posts/things-to-do-in-bowmanville-during-march-break.html",
  ],
  'go-train-coming-to-bowmanville' => [
    "title" => "There’s a GO Train Coming to Bowmanville",
    "excerpt" => "Where do you need to GO? Get ready for quick commuting...",
    "date" => "03/04/20",
    "image" => SITE_IMAGES . 'posts/' . "go-train-coming-to-bowmanville.jpg",
    "url" => SITE_URL . "/posts/go-train-coming-to-bowmanville.html",
  ],
  'best-sales-office-award' => [
    "title" => "Valleyview Wins DRHBA Best Sales Office Award ",
    "excerpt" => "Valleyview in Bowmanville displays beauty on every corner.",
    "date" => "02/11/20",
    "image" => SITE_IMAGES . 'posts/' . "best-sales-office-award.jpg",
    "url" => SITE_URL . "/posts/best-sales-office-award.html",
  ],
  'new-community-centre-coming' => [
    "title" => "Valleyview – Bowmanville is Getting A New Community Centre in 2023!",
    "excerpt" => "Coming to Bowmanville in 2023 is a fun-filled, extravagant Community Centre...",
    "date" => "01/06/20",
    "image" => SITE_IMAGES . 'posts/' . "new-community-centre-coming.png",
    "url" => SITE_URL . "/posts/new-community-centre-coming.html",
  ],
  'new-release' => [
    "title" => "New Release!",
    "excerpt" => "Check out some photos from our new release on November 16th, 2019!",
    "date" => "11/16/19",
    "image" => SITE_IMAGES . 'posts/' . "new-release-1-thumb.jpg",
    "url" => SITE_URL . "/posts/new-release.html",
  ],
  'truck-raffle' => [
    "title" => "Truck Raffle",
    "excerpt" => "Grand Success at Valleyview Bowmanville with a Ford F-150 promotion. ",
    "date" => "06/23/19",
    "image" => SITE_IMAGES . 'posts/' . "truck-raffle-1.JPG",
    "url" => SITE_URL . "/posts/truck-raffle.html",
  ],
  'mortgage-seminar' => [
    "title" => "Mortgage Seminar",
    "excerpt" => "Valleyview held a Free Information seminar discussing various topics for purchasers. ",
    "date" => "05/23/19",
    "image" => SITE_IMAGES . 'posts/' . "mortgage-seminar-3.JPG",
    "url" => SITE_URL . "/posts/mortgage-seminar.html",
  ],
  'second-phase' => [
    "title" => "Second Phase",
    "excerpt" => "Phase 2 brought more purchasers to Valleyview in Bowmanville with another successful release of homes.",
    "date" => "03/30/19",
    "image" => SITE_IMAGES . 'posts/' . "second-phase-1.JPG",
    "url" => SITE_URL . "/posts/second-phase.html",
  ],
  'grand-opening' => [
    "title" => "Grand Opening",
    "excerpt" => "Grand Opening was a huge success with over 80% sold out.",
    "date" => "10/27/18",
    "image" => SITE_IMAGES . 'posts/' . "grand-opening-1.JPG",
    "url" => SITE_URL . "/posts/grand-opening.html",
  ],
);
?>