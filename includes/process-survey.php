<?php
date_default_timezone_set('America/Toronto');
include ("kint.phar");


//Check if POST
$error_messages = array();
$is_post = false;
$yes_no_array = ['Yes', 'No'];

//variables
$first_name = "";
$last_name = "";
$email = "";
$cemail = "";
$postal = "";
$phone = "";
$city = "";
$province = "";


$interested_purchaser = "";
$new_price = "";
$how_long = "";
$how_did_you_hear = "";
$not_purchasing_reason = "";
$new_home_preference = "";
$size_lot = "";
$square_footage = "";
$number_of_bedrooms = "";
$price = "";
$when_looking_to_purchase = "";
$familiar_with_bowmanville = "";
$familiar_with_bowmanville_as_a_community = "";
$did_you_know_average_price = "";
$did_you_know_price_is_lower = "";
$reason_most_appealing = "";
$looking_for_most = "";
$thank_you_visa = "";
$casl = "";

if(isset($_POST['casl']) && $_POST['casl'] == "Yes"){

  $is_post = true;

  $first_name = ucwords(strtolower(trim(filter_var($_POST['first_name'], FILTER_SANITIZE_STRING))));
  $last_name = ucwords(strtolower(trim(filter_var($_POST['last_name'], FILTER_SANITIZE_STRING))));
  $email = trim(strtolower(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL)));
  $cemail = trim(strtolower(filter_var($_POST['cemail'], FILTER_SANITIZE_EMAIL)));
  $postal = trim(strtoupper(filter_var($_POST['postal'], FILTER_SANITIZE_STRING)));
  $phone = trim(filter_var($_POST['phone'], FILTER_SANITIZE_STRING));
  $city = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
  $province = filter_var($_POST['province'], FILTER_SANITIZE_STRING);

  $interested_purchaser = filter_var($_POST['interested_purchaser'], FILTER_SANITIZE_STRING);
  $new_price = filter_var($_POST['new_price'], FILTER_SANITIZE_STRING);
  $how_long = filter_var($_POST['how_long'], FILTER_SANITIZE_STRING);
  $how_did_you_hear = filter_var($_POST['how_did_you_hear'], FILTER_SANITIZE_STRING);

/* removed
  if($interested_purchaser == "No"){
    $not_purchasing_reason = filter_var($_POST['not_purchasing_reason'], FILTER_SANITIZE_STRING);
  }
  else if($interested_purchaser == "Yes"){
    $new_home_preference = filter_var($_POST['new_home_preference'], FILTER_SANITIZE_STRING);
  }


  //Clear out fields depending on the new home preference
  if($new_home_preference == "Sq.ft"){
    $square_footage = filter_var($_POST['square_footage'], FILTER_SANITIZE_STRING);
  }
  else if($new_home_preference == "# of Bedrooms"){
    $number_of_bedrooms = filter_var($_POST['number_of_bedrooms'], FILTER_SANITIZE_STRING);
  }
  else if($new_home_preference == "Lot Size"){
    $size_lot = filter_var($_POST['size_lot'], FILTER_SANITIZE_STRING);
  }
  else if($new_home_preference == "Price"){
    $price = filter_var($_POST['price'], FILTER_SANITIZE_STRING);
  }

*/



  // $when_looking_to_purchase = filter_var($_POST['when_looking_to_purchase'], FILTER_SANITIZE_STRING);
  // $familiar_with_bowmanville = filter_var($_POST['familiar_with_bowmanville'], FILTER_SANITIZE_STRING);
  // $familiar_with_bowmanville_as_a_community = filter_var($_POST['familiar_with_bowmanville_as_a_community'], FILTER_SANITIZE_STRING);

  $did_you_know_average_price = filter_var($_POST['did_you_know_average_price'], FILTER_SANITIZE_STRING);
  $did_you_know_price_is_lower = filter_var($_POST['did_you_know_price_is_lower'], FILTER_SANITIZE_STRING);
  $reason_most_appealing = filter_var($_POST['reason_most_appealing'], FILTER_SANITIZE_STRING);
  $looking_for_most = filter_var($_POST['looking_for_most'], FILTER_SANITIZE_STRING);
  $thank_you_visa = filter_var($_POST['thank_you_visa'], FILTER_SANITIZE_STRING);

  $casl = filter_var($_POST['casl'], FILTER_SANITIZE_STRING);





  if (empty($first_name)) $error_messages[] = "Please fill in your first name";
  elseif (empty($last_name)) $error_messages[] = "Please fill in your last name";
  elseif (empty($email) || empty($cemail)) $error_messages[] = "Please fill in your email";
  elseif ($email != $cemail) $error_messages[] = "Your Email addresses do not match";
  elseif (empty($phone)) $error_messages[] = "Please fill in your phone number";
  elseif (empty($city)) $error_messages[] = "Please fill in your city";
  elseif (empty($province)) $error_messages[] = "Please fill in your province";
  elseif (empty($postal)) $error_messages[] = "Please fill in your postal code";


  elseif (empty($interested_purchaser) || !in_array($interested_purchaser, $yes_no_array)) $error_messages[] = "Please fill in your interest as a purchaser";
  // elseif (empty($familiar_with_bowmanville) || !in_array($familiar_with_bowmanville, $yes_no_array)) $error_messages[] = "Please fill in if you're familiar with WP Development & Valleyview in Bowmanville";
  // elseif (empty($familiar_with_bowmanville_as_a_community) || !in_array($familiar_with_bowmanville_as_a_community, $yes_no_array)) $error_messages[] = "Please fill in you're familiar with Bowmanville as a community";

  elseif (empty($did_you_know_average_price) || !in_array($did_you_know_average_price, $yes_no_array)) $error_messages[] = "Please fill in if you're aware of the average home price in Bowmanville";
  elseif (empty($did_you_know_price_is_lower) || !in_array($did_you_know_price_is_lower, $yes_no_array)) $error_messages[] = "Please fill in if you're aware of that Bowmanville homes are $60,000 lower than in Whitby";
  elseif (empty($reason_most_appealing) || !in_array($reason_most_appealing, array_keys($what_is_most_appealing_array))) $error_messages[] = "Please fill in what you find most appealing in a potential home";
  elseif (empty($looking_for_most) || !in_array($looking_for_most, array_keys($what_are_you_looking_for_array))) $error_messages[] = "Please fill in what you are looking forward to most in a Valleyview home";
  elseif (empty($thank_you_visa) || !in_array($thank_you_visa, $yes_no_array)) $error_messages[] = "Please fill in whether you want a Thank You Visa card";

  //are they one of the available options
  /*
  if(!empty($not_purchasing_reason) && !in_array($not_purchasing_reason, array_keys($if_not_purchasing_array))){
    $error_messages[] = "Please choose a valid not purchasing home reason";
  }
  if(!empty($new_home_preference) && !in_array($new_home_preference, array_keys($new_home_preference_array))){
    $error_messages[] = "Please choose a valid not purchasing home reason";
  }


  if(!empty($size_lot) && !in_array($size_lot,  array_keys($size_lot_array))){

    $error_messages[] = "Please choose a valid lot size";
  }
  if(!empty($square_footage) && !in_array($square_footage, array_keys($square_footage_array))){
    $error_messages[] = "Please choose a valid square footage";
  }
  if(!empty($number_of_bedrooms) && !in_array($number_of_bedrooms, array_keys($bedroom_array))){
    $error_messages[] = "Please choose a valid number of bedrooms";
  }*/

  if(!empty($how_long) && !in_array($how_long, array_keys($how_long_looking_new_home_array))){
    $error_messages[] = "Please choose a valid option";
  }
  if(!empty($new_price) && !in_array($new_price, array_keys($new_price_array))){
    $error_messages[] = "Please choose a valid price range";
  }

  if(!empty($how_did_you_hear) && !in_array($how_did_you_hear, array_keys($how_did_you_hear_array))){
    $error_messages[] = "Please choose a valid option";
  }
  /*if(!empty($price) && !in_array($price, array_keys($price_array))){
    $error_messages[] = "Please choose a valid price range";
  }*/


  /*
  elseif (empty($not_purchasing_reason)) $error_messages[] = "Please fill in your last name";
  elseif (empty($size_lot)) $error_messages[] = "Please fill in your last name";
  elseif (empty($square_footage)) $error_messages[] = "Please fill in your last name";
  elseif (empty($number_of_bedrooms)) $error_messages[] = "Please fill in your last name";
  elseif (empty($price)) $error_messages[] = "Please fill in your last name";
  elseif (empty($when_looking_to_purchase)) $error_messages[] = "Please fill in your last name";


  */

  //test error messages
  // $error_messages[] = "Please fill in your interest as a purchaser";
  // $error_messages[] = "Please fill in your interest as a purchaser22";

  if(count($error_messages)== 0){
    $host = DB_SERVER;
    $db   = DB_NAME;
    $user = DB_USER;
    $pass = DB_PASS;
    $charset = 'utf8mb4';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $options = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    try {

        $pdo = new PDO($dsn, $user, $pass, $options);

    } catch (\PDOException $e) {

        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }

    //check for duplicate email
    $stmt = $pdo->prepare('SELECT * FROM survey WHERE email = ?');
    $stmt->execute([$email]);
    $dups = $stmt->rowCount();

    // $dups = 0; //for testing

    if($dups >= 1){

      $error_messages[] = "You have already participated in this survey";

    }else{
      $created = date("Y-m-d H:i:s");
      $modified = $created;



      $sql = "INSERT INTO `survey`(`first_name`, `last_name`, `email`, `cemail`, `postal`, `phone`, `city`, `province`, `interested_purchaser`, `new_home_preference`, `not_purchasing_reason`, `size_lot`, `square_footage`, `number_of_bedrooms`, `price`, `when_looking_to_purchase`, `familiar_with_bowmanville`, `familiar_with_bowmanville_as_a_community`, `did_you_know_average_price`, `did_you_know_price_is_lower`, `reason_most_appealing`, `looking_for_most`,  `new_price`, `how_long`, `how_did_you_hear`, `thank_you_visa`, `casl`, `created`, `modified`) VALUES " .
      "(" . str_repeat('?,', 28). "?)";

      $stmt = $pdo->prepare($sql);
      $stmt_result = $stmt->execute([
        $first_name, $last_name, $email, $cemail, $postal, $phone, $city, $province, $interested_purchaser, $new_home_preference, $not_purchasing_reason, $size_lot, $square_footage, $number_of_bedrooms,
        $price, $when_looking_to_purchase, $familiar_with_bowmanville, $familiar_with_bowmanville_as_a_community, $did_you_know_average_price, $did_you_know_price_is_lower, $reason_most_appealing, $looking_for_most, $new_price, $how_long, $how_did_you_hear, $thank_you_visa, $casl, $created, $modified
      ]);






      if ($stmt_result) {

        $html = '';

        $html .= '<p>Hello Valleyview,</p>';
        $html .= '<p>' . $first_name . " " . $last_name . ' has filled out the survey. Their answers are listed below:</p>';

        $html .= 'Name:<br>' . $first_name . " " . $last_name . "<br><br>";
        $html .= 'Email:<br>' . $email . "<br><br>";
        $html .= 'Postal Code:<br>' . $postal . "<br><br>";
        $html .= 'Phone:<br>' . $phone . "<br><br>";
        $html .= 'City:<br>' . $city . "<br><br>";
        $html .= 'Province:<br>' . $province . "<br><br>";
        $html .= 'Are you looking for a new home?<br>' . $interested_purchaser . "<br><br>";
        $html .= 'How long have you been looking for a new home?<br>' . $how_long . "<br><br>";
        $html .= 'What price range are you looking to spend?<br>' . $new_price . "<br><br>";
        $html .= 'How did you hear about Valleyview Bowmanville?<br>' . $how_did_you_hear . "<br><br>";
        $html .= 'Did you know that the average home price (of homes sold) in Bowmanville has increased $30,000 since April 2019?<br>' . $did_you_know_average_price . "<br><br>";
        $html .= 'Did you know that, as of December 2019, the average home price in Bowmanville/Clarington is approximately $60,000 lower than in Whitby?<br>' . $did_you_know_price_is_lower . "<br><br>";
        $html .= 'What is most appealing to you about a potential home purchase in Bowmanville?<br>' . $reason_most_appealing . "<br><br>";
        $html .= 'What are you looking for most from a Valleyview home?<br>' . $looking_for_most . "<br><br>";
        $html .= 'As a thank you for completing this survey, would you like the chance to win a $100 VISA gift card?<br>' . $thank_you_visa . "<br><br>";



        $html .= '<p>Regards,<br />Valleyview</p>';





        $data['to'] = 'info@thevalleyview.ca';
        $data['to_next'] = 'kzhang@wealthpower-group.com';
        $data['to_backup'] = 'davidh@impactnorth.com';

        $data['from'] = 'info@thevalleyview.ca';
        $data['subject'] = 'Registrant Survey Completed - Valleyview';
        $data['html'] = $html;
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: Valleyview<'.$data['from'].'>' . "\r\n";

        mail($data['to'], $data['subject'], $data['html'], $headers);
        mail($data['to_next'], $data['subject'], $data['html'], $headers);
        // mail($data['to_backup'], $data['subject'], $data['html'], $headers);

        //reset variables
        $first_name = "";
        $last_name = "";
        $email = "";
        $cemail = "";
        $postal = "";
        $phone = "";
        $city = "";
        $province = "";


        $interested_purchaser = "";
        $new_price = "";
        $how_long = "";
        $how_did_you_hear = "";
        $not_purchasing_reason = "";
        $new_home_preference = "";
        $size_lot = "";
        $square_footage = "";
        $number_of_bedrooms = "";
        $price = "";
        $when_looking_to_purchase = "";
        $familiar_with_bowmanville = "";
        $familiar_with_bowmanville_as_a_community = "";
        $did_you_know_average_price = "";
        $did_you_know_price_is_lower = "";
        $reason_most_appealing = "";
        $looking_for_most = "";
        $thank_you_visa = "";
        $casl = "";

      }



    }
  }
}
