<?php

$province_array = [
  "Alberta" => "Alberta",
  "British Columbia" => "British Columbia",
  "Manitoba" => "Manitoba",
  "New Brunswick" => "New Brunswick",
  "Newfoundland and Labrador" => "Newfoundland and Labrador",
  "Nova Scotia" => "Nova Scotia",
  "Ontario" => "Ontario",
  "Prince Edward Island" => "Prince Edward Island",
  "Quebec" => "Quebec",
  "Saskatchewan" => "Saskatchewan",
  "Northwest Territories" => "Northwest Territories",
  "Nunavut" => "Nunavut",
  "Yukon" => "Yukon"
];

$new_home_preference_array = [
  "Sq.ft" => "Sq.ft",
  "# of Bedrooms" => "# of Bedrooms",
  "Lot Size" => "Lot Size",
  "Price" => "Price",
];




$not_purchasing_home_array = [
  "I'm not ready to move" => "I'm not ready to move",
  "I recently purchased a home" => "I recently purchased a home",
  "I want a semi-detached or townhome" => "I want a semi-detached or townhome",
  "Budget" => "Budget",
  "Other" => "Other"
];

$size_lot_array = [
  "34" => "34'",
  "37" => "37'",
  "40" => "40'",
  "48" => "48'",
];

$square_footage_array = [
  "1,500-2,000 sq.ft." => "1,500-2,000 sq.ft.",
  "2,001-2,500 sq.ft." => "2,001-2,500 sq.ft.",
  "2,501-3,000 sq.ft." => "2,501-3,000 sq.ft.",
  "3,001-3,500 sq.ft." => "3,001-3,500 sq.ft.",
  "Other" => "Other"
];


$bedroom_array = [
  "3 Beds" => "3 Beds",
  "4 Beds" => "4 Beds",
  "4 Beds With Extras" => "4 Beds with Den/Office/Library"
];

$price_array = [
  "$600,000-$649,999" => "$600,000-$649,999",
  "$650,000-$699,999" => "$650,000-$699,999",
  "$700,000-$750,000" => "$700,000-$750,000",
  "$751,000-$799,999" => "$751,000-$799,999",
  "$800,000-$849,999" => "$800,000-$849,999",
  "$850,000-$899,999" => "$850,000-$899,999"
];

$new_price_array = [
  "$600k-$699k" => "$600k-$699k",
  "$700k-$799k" => "$700k-$799k",
  "$800k-$899k" => "$800k-$899k",
  "$900k-$990k" => "$900k-$990k",
  "Over 1 Million" => "Over 1 Million",
];

$if_not_purchasing_array = [
  "Affordability" => "Affordability",
  "Location" => "Location",
  "Size of Home" => "Size of Home",
  "Style of Home" => "Style of Home",
  "Not Ready" => "Not Ready",
];

$when_to_purchase_array = [
  "Immediately" => "Immediately",
  "A month to 6 months" => "A month to 6 months",
  "6 months to a year" => "6 months to a year",
  "A year or more  " => "A year or more"
];


$how_long_looking_new_home_array = [
  "Less than a month" => "Less than a month",
  "1-3 Months" => "1-3 Months",
  "4-6 Months" => "4-6 Months",
  "7-12 Months" => "7-12 Months",
  "Over one year" => "Over one year",
];

$how_did_you_hear_array = [
  "Google" => "Google",
  "Facebook" => "Facebook",
  "Instagram" => "Instagram",
  "Radio" => "Radio",
  "GO Station" => "GO Station",
  "Friends" => "Friends",
  "Agent" => "Agent"
];

$what_is_most_appealing_array = [
  "Avoid the hustle and bustle of the city" => "Avoid the hustle and bustle of the city",
  "Access to major highways (i.e. Highway 401, 418, 407, 35/115 )" => "Access to major highways (i.e. Highway 401, 418, 407, 35/115 )",
  "More Affordable Housing (Bigger Home for less $$)" => "More Affordable Housing (Bigger Home for less $$)",
  "Area amenities that are available" => "Area amenities that are available"
];

$what_are_you_looking_for_array = [
  "Value" => "Value",
  "Exterior elevation options (Transitional | Traditional)" => "Exterior elevation options (Transitional | Traditional)",
  "Interior Finishes" => "Interior Finishes"

];
?>