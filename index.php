<?php include('includes/meta.php'); ?>
	<title>Valleyview in Bowmanville</title>
</head>
<body>
<?php include('includes/navigation.php'); ?>


<div class="position-fixed contact-bar rounded-left" style="margin-right: -200px;">

	<div class="row align-items-center py-2 px-2">
		<div class="col-12">
			<a href="tel:905.597.0466" class="text-white font-weight-normal m-0" ><img src="<?= SITE_IMAGES; ?>call-button.png" alt="Call Us" class="img-fluid"></a>
		</div>
	</div>


	<div class="row align-items-center py-2 px-2">
		<div class="col-12">
			<a href="mailto:info@thevalleyview.ca" class="text-white font-weight-normal m-0" ><img src="<?= SITE_IMAGES; ?>email-button.png" alt="Email Us" class="img-fluid"></a>
		</div>

	</div>

</div>
<div id="heroSlider" class="carousel slide carousel-fade pt-0 pt-lg-5" data-ride="carousel">
	<div class="carousel-inner">
        <div class="carousel-item slide mt-5 mt-md-0 active" id="slide-0">
            <img src="<?= SITE_IMAGES ?>misc/blank.png" class="w-100" alt="">
            <div class="carousel-caption d-block d-md-block ">
                <p class="background-blue py-2 px-4 d-inline-block mb-0">Come For The Community <span class="span-slider-bullet">&bullet;&nbsp;</span><span class="span-slider-text">Stay For The Recreation</span></p>
            </div>
        </div>
		<div class="carousel-item slide mt-5 mt-md-0" id="slide-1">
            <img src="<?= SITE_IMAGES ?>misc/blank.png" class="w-100" alt="">
            <div class="carousel-caption d-block d-md-block">
				<p class="background-blue py-2 px-4 d-inline-block mb-0">Come For The Community <span class="span-slider-bullet">&bullet;&nbsp;</span><span class="span-slider-text">Stay For The Lifestyle</span></p>
            </div>
		</div>
		<div class="carousel-item slide mt-5 mt-md-0" id="slide-2">
            <img src="<?= SITE_IMAGES ?>misc/blank.png" class="w-100" alt="">
            <div class="carousel-caption d-block d-md-block">
				<p class="background-blue py-2 px-4 d-inline-block">Come For The Community <span class="span-slider-bullet">&bullet;&nbsp;</span><span class="span-slider-text">Stay For The Nature</span></p>
            </div>
		</div>
		<div class="carousel-item slide mt-5 mt-md-0" id="slide-3">
            <img src="<?= SITE_IMAGES ?>misc/blank.png" class="w-100" alt="">
            <div class="carousel-caption d-block d-md-block">
				<p class="background-blue py-2 px-4 d-inline-block">Come For The Community <span class="span-slider-bullet">&bullet;&nbsp;</span><span class="span-slider-text">Stay For The Style</span></p>
            </div>
		</div>
		<div class="carousel-item slide mt-5 mt-md-0" id="slide-4">
            <img src="<?= SITE_IMAGES ?>misc/blank.png" class="w-100" alt="">
            <div class="carousel-caption d-block d-md-block">
				<p class="background-blue py-2 px-4 d-inline-block">Come For The Community <span class="span-slider-bullet">&bullet;&nbsp;</span><span class="span-slider-text">Stay For The Amenities</span></p>
            </div>
        </div>
	</div>
</div>

<div id="sidebar-contact" class="index-container-size">
	<div class="row no-gutters pt-5">
		<div class="col-12 col-lg-8 pt-5 py-1 text-center text-lg-left order-2 order-lg-1">
			<h1 class="h2-index">Detached homes in bowmanville</h1>
			<p class="p-index">14 acres of rollng hills , valleys and meadows. Country charm with city amenities. Steps from highways and future GO station. Moments to schools, parks , trails, shops and services. Scenic trails hugging the lakeshore beside lapping waves. A historic downtown with culture, fine dining, festivals and more. A beautiful, connected, affordable place to live, grow and raise a family A place that feels like home. Welcome to Valleyview in Bowmanville.</p>
		</div>

		<div class="col-12 col-lg-4 text-center text-lg-right middle-height py-1 order-1 order-lg-2">
			<h3 class="h3-index"><strong>Open</strong></h3>
			<h5>By Appointment Only</h5>
			<p class="index-visit-sales mb-0"><strong>Sales Office</strong></p>
			<a class="a-index" href="<?= SITE_URL ?>/location.html"><strong>2021 Green Road</strong></a>
		</div>
	</div>
</div>

<div class="row no-gutters pt-5 position-relative">
	<div class="col-12 col-md-6 pt-5">
		<img src="<?= SITE_IMAGES ?>index/render-1.jpg" class="img-fluid mx-auto d-block w-100" alt="Traditional Render">
	</div>
	<div class="col-12 col-md-6 overflow-hidden pt-0 pt-md-5">
		<img src="<?= SITE_IMAGES ?>index/render-2.jpg" class="img-fluid mx-auto d-block w-100" alt="Transitional Render">
	</div>
  <img src="<?= SITE_IMAGES ?>index/leaf-left.png" class="position-absolute leaf-left-pos d-block d-xl-block" alt="Leafs">
  <img src="<?= SITE_IMAGES ?>index/leaf-right-cropped.png" class="position-absolute leaf-right-pos d-block d-xl-block" alt="Leafs">
</div>

<div class="row no-gutters bg-dark-blue">

	<div class="container py-5">
		<div class="col-12 text-center pt-3">
			<h4 class="h4-index"><span class="h4-span">Traditional</span> &amp; <span class="h4-span">Transitional</span> home designs</h4>
		</div>
		<div class="col-12 text-center pb-3">
			<p class="p-index-2">Every family is different. By starting from this basic premise, we design our homes to meet real needs of real people. Perhaps you seek inspiration from timeless values. Then our traditional 2-storey homes with heritage brick and stone exteriors are just right for you. Or you may have a taste for modern design, something like our transitional series, a blend of classic and modern styles. Our designs and floorplans are driven by the needs of today’s families. That’s the key difference we bring to our product, the prime reason you will always feel at home with us.</p>
		</div>
	</div>

</div>








	<?php include('includes/footer.php'); ?>
	<?php include 'pages/popup.php'; ?>

	<script>

	$('.carousel').carousel({
  	  interval: 10000,
      pause: false
	});
	//	var totalItems = $('.carousel-item').length;
		var currentIndex = $('div.active').index() + 1;

	$('#heroSlider').on('slid.bs.carousel', function () {
		currentIndex = $('div.active').index() + 1;
		$('#bubble1').attr('src' , '<?= SITE_IMAGES ?>bubble-'+currentIndex+'.png')

	});

		setInterval(function(){
			$('.first-caption').toggle( "slow", function(){
				$('.first-caption').animate({
					opacity:.9
				}, 500);
			} );
			$('.second-caption').toggle( "slow" );
		}, 3000);

    $(window).on('load',function(){
       $('#exampleModalCenter').modal('show');

    });

    $('#closeBtn').click(function(){
        $('#exampleModalCenter').modal('hide');
    })

    $(window).on('resize',function(){
        //on resize
        matchSlideHeight();
    });

    //calculate height of first slide
    $(document).ready(function(){

        //matchSlideHeight();

    });

    function matchSlideHeight(){
        if($("#slide-0").hasClass("active")){
            var slide = document.getElementById('slide-image-0-mobile');
            if($(window).width() > 992){
                slide = document.getElementById('slide-image-0');
            }

                var width = slide.clientWidth;
                var height = slide.clientHeight;

                $("#heroSlider .slide").css("height", height);
                $(".carousel-inner").css("height", height);

            slide.onload = function () {
                var width = slide.clientWidth;
                var height = slide.clientHeight;
                $("#heroSlider .slide").css("height", height);
                $(".carousel-inner").css("height", height);

            };

        }

    }


	</script>


</body>
</html>
