$( document ).ready(function() {


    var mobyMenu = new Moby({
		menu: $('#main-nav'), // The menu that will be cloned
		mobyTrigger: $('#nav-icon'), // Button that will trigger the Moby menu to open
		breakpoint: 992,
		enableEscape: true,
		menuClass: 'top-full',
		onClose: function(){
      $("nav").addClass('fixed-top');
      $("nav .navbar-brand").removeClass('d-none');
      $('.navbar-collapse').collapse('hide');


      $(".moby-menu nav").addClass("bg-white");
      $(".moby-menu .img-moby-nav-logo").addClass("d-none");
      $(".moby-menu .li-nav-register").addClass("animated flash");
    },
		onOpen: function(){
		  $("nav").removeClass('fixed-top');
      $("nav .navbar-brand").addClass('d-none');
      $('.navbar-collapse').collapse('show');

      $(".moby-menu nav").removeClass("bg-white");
      $(".moby-menu .img-moby-nav-logo").removeClass("d-none");
      $(".moby-menu .li-nav-register").removeClass("animated flash");
    },
		overlay: true,
		overlayClass: 'menu-mobile-overlay',
		subMenuOpenIcon: '<span>&#x25BC;</span>',
		subMenuCloseIcon: '<span>&#x25B2;</span>',
		template: '<div class="moby-wrap"><div class="moby-close"><span class="moby-close-icon"></span></div><div class="moby-menu"></div></div>'
    });




});

$(window).on('scroll', function(){
  if($('#sidebar-contact').length){

    clearTimeout($.data(this, 'scrollTimer'));
    $.data(this, 'scrollTimer', setTimeout(function() {
      if( $(window).scrollTop() < 200){

        $('.contact-bar').animate({
          'margin-right': '-200',
        }, 400);
      }else{

        $('.contact-bar').animate({
          'margin-right': '0',
        }, 400);

      }
    }, 150));
  }
});