<?php 	include('../includes/meta.php'); ?>
<title>Register | Valleyview in Bowmanville</title>
<style>
	.invalid input:required:invalid{border: 1px solid #CB0006!important;}
	.invalid select:required:invalid{border: 1px solid #CB0006!important;}
	.invalid input:required:valid{border: 1px solid #666666!important;}

</style>
<script>
  function onSubmit(token) {
	document.getElementById("registrationForm").submit()
  }

  function validate(event) {
    event.preventDefault();
    if(
			(!document.getElementById('first_name').value) ||
			(!document.getElementById('last_name').value) ||
			(!document.getElementById('email').value) ||
			(!document.getElementById('how_did_you_hear').value) ||
			(!document.getElementById('broker').value) ||
			(!document.getElementById('postal_zip').value) ||
			(!document.getElementById('phone').value)
	  ){
		alert("Please Check Required Fields *");
	  document.getElementById("registrationForm").classList.add('invalid');
		} else if (document.getElementById('email').value !== document.getElementById('confirm_email').value) {
			alert('Confirm Email Not Matching.');
			document.getElementById("registrationForm").classList.add('invalid');
  		}

	else {

      grecaptcha.execute();
    }
  }


  function onload() {
    var element = document.getElementById('btnSubmit');
    element.onclick = validate;
  }
</script>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>


</head>
<body id="register">
<?php include('../includes/navigation.php'); ?>
<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-md-6 mt-5">
			<img src="images/popup/popup.png" class="img-fluid mt-5" alt="">
				<!-- <p style="font-size: 50px;" class="text-grey">Phase 1 <strong><span style="font-family:'AvantGarde_Bold';">SOLD OUT</span></strong></p>
				<img src="images/register/coming-soon.png" class="img-fluid py-3" alt="">
				<p class="text-grey" style="font-family:'AvantGarde_Bold';font-size:27px;">
					Register for our Special One Weekend
					<br>
					Only Sales Event.
				</p>
				<p class="text-grey" style="font-size:27px;">
					<span style="font-family:'AvantGarde_Bold';">Detached Homes</span> in Bowmanville starting
					<br> in the low $600’s
				</p>
				<p class="" style="font-size:37px;color:#588CC7;">
					<span style="font-family:'AvantGarde_Bold';line-height:2rem;">Don’t miss</span> this opportunity to <br>
					<span style="line-height:3rem"> purchase in Durham’s  <br>
					MOST successful community.</span>
				</p> -->
				<!-- <h3 class="text-grey">Want to Know more?</h3>
				<h2 class="text-grey">REGISTER NOW</h2>
				<h3 class="text-grey mb-4">Find out about our latest promotions.</h3>
				<p><strong>Pre-register for Valleyview today and you’ll be entitled to:</strong> </p>


				<ul>
					<li>Advance access to information about the homes and community</li>
					<li>A ‘sneak peek’ at the designs and site BEFORE the general public</li>
					<li>VIP Preview invitation with your choice of the best lots at a preferred price</li>
				</ul> -->

<!--				<p class="mt-5"><strong>Sales office will be closed from <br /></b>Dec 20<sup>th</sup>, 2018 to Jan 4<sup>th</sup>, 2019</strong></p>-->

		</div>
		<div class="col-xs-12 col-md-6 mt-4">
		<p class="text-center text-md-right"><small>Fields marked in <span class="text-green">GREEN *</span> are required.</small></p>
		<form action="https://thevalleyview.ca/subscription/valleyview/processor/process_register.php" method="post" id="registrationForm" class="">
			<div class="form-group">
				<label for="first_name"><span class="text-green">First Name*</span></label>
				<input type="text" class="form-control" id="first_name" name="first_name" required>
				<input name="middle_name" id="middle_name">
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Last Name*</span></label>
				<input type="text" class="form-control" id="last_name" name="last_name" required>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Email Address*</span></label>
				<input type="email" class="form-control" id="email" name="email" required>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Confirm Email Address*</span></label>
				<input type="email" class="form-control" id="confirm_email" name="confirm_email" required>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">How Did You hear about Us?*</span></label>
				<select name="how_did_you_hear" id="how_did_you_hear" class="form-control" required>
					<option value="">Select one...</option>
					<option value="Agent/Friend Referral">Agent/Friend Referral</option>
					<option value="BuzzBuzz Homes">BuzzBuzz Homes</option>
					<option value="Facebook">Facebook</option>
					<option value="Flyers">Flyers</option>
					<option value="GO Station">GO Station</option>
					<option value="Google">Google</option>
					<option value="Instagram">Instagram</option>
					<option value="Radio">Radio</option>
					<option value="Street-Ad">Street-Ad</option>
					<option value="Text Message">Text Message</option>
					<option value="TV/Magazine">TV/Magazine</option>
				</select>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Are you a Broker?*</span></label>
				<select id="broker" name="broker" required class="form-control">
					<option value="">Select one...</option>
					<option value="YES">YES</option>
					<option value="NO">NO</option>
				</select>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="">Brokerage</span></label>
				<input type="text" class="form-control" id="brokerage" name="brokerage">
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Postal Code*</span></label>
				<input type="text" class="form-control" id="postal_zip" name="postal_zip" required>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Telephone*</span></label>
				<input type="text" class="form-control" id="phone" name="phone" required>
				<input name="fax_number" id="fax_number" value="416.661.7866">
			</div>
			 <div id='recaptcha' class="g-recaptcha"
          data-sitekey="6Lc1FX4UAAAAAIKdM3F4fE7AEEYl02BQc3tUhXPM"
          data-callback="onSubmit"
          data-size="invisible"></div>

			<button type="submit"  id="btnSubmit" class="btn text-right">Submit</button>

		</form>
		<div class="clearfix"></div>
		<p class="disclaimer">By submitting this form, you are agreeing to receive  communications from WP Development. We do not share information with third parties.</p>

		</div>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
    <script>
		$('#nav-register').addClass(' active');
	</script>
	<script>onload();</script>
</body>
</html>
