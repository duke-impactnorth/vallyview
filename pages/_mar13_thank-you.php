<?php 	include('../includes/meta.php'); ?>

<!-- Google Code for Valleyview Bowmanville Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 805980863;
var google_conversion_label = "fY9iCKCmpoEBEL-VqYAD";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script><noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//www.googleadservices.com/pagead/conversion/805980863/?label=fY9iCKCmpoEBEL-VqYAD&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '2049667131947620');

fbq('track', 'CompleteRegistration');

</script>

<noscript>

<img height="1" width="1"

src="https://www.facebook.com/tr?id=2049667131947620&ev=PageView

&noscript=1"/>

</noscript>

 <img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&.yp=10076558"/>
<!-- End Facebook Pixel Code -->

<title>Thank You | Valleyview in Bowmanville</title>
</head>
<body id="the-team">
<?php include('../includes/navigation.php'); ?>
<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-12 mt-4">
			<h2 class="text-grey text-center">Thank you</h2>
			<h4 class="text-grey text-center mt-4">Your registration was submitted successfully.</h4>
			<p class="text-center mt-5">
				we have sent a confirmation email your way. If you don’t receive this confirmation message in your inbox, make sure to check your spam/junk mailboxes, and please take a moment to add <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a> to your contact list. Thank you.
			</p>
		</div>

	</div>
</div>
<div class="container-fluid pt-3 pb-5" style="background-color: #A5CD43;">
	<div class="row">
		<div class="col-12 pb-5" >
			<img src="/images/thank-you.jpg" class="img-fluid d-block mx-auto mt-5 mb-5" alt="Thank You">

		</div>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
</body>
</html>
