<?php 	include('../includes/meta.php'); ?>

<!-- Google Code for Valleyview Bowmanville Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 805980863;
var google_conversion_label = "fY9iCKCmpoEBEL-VqYAD";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"  
src="//www.googleadservices.com/pagead/conversion.js">
</script><noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  
src="//www.googleadservices.com/pagead/conversion/805980863/?label=fY9iCKCmpoEBEL-VqYAD&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '2049667131947620'); 

fbq('track', 'CompleteRegistration');

</script>

<noscript>

<img height="1" width="1" 

src="https://www.facebook.com/tr?id=2049667131947620&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->

<title>Thank You | Valleyview in Bowmanville</title>
</head>
<body id="the-team">
<?php include('../includes/navigation.php'); ?>
<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-12 mt-4">
			<h2 class="text-grey text-center">Thank you</h2>
			<h4 class="text-grey text-center mt-4">Your registration was submitted successfully.</h4>
		</div>

	</div>
</div>
<div class="container-fluid pt-3 pb-5" style="background-color: #A5CD43;">
	<div class="row">
		<div class="col-12 pb-5" >
			<img src="/images/win-a-free-trip.png" class="img-fluid d-block mx-auto mt-5 mb-5" alt="Thank You">
			<p class="text-center" style="font-size: 14px; line-height: 28px;">*No purchase necessary. Must be 18 years or older to enter or qualified to win.  One (1) winner will be chosen at random based on valid contest entry. Limit one (1) entry per person. Duplicate entries and/or incomplete entries will be disqualified. Sponsor is not responsible for lost, late, illegible or incomplete entries. No cash value.</p>
		</div>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
</body>
</html>