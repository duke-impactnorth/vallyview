<?php 	include('../includes/meta.php'); ?>
<title>Register | Valleyview in Bowmanville</title>
<style>
	.meet-santa{
		color: #CB0006;
		font-size: 80px;
		font-weight: 800;
		font-family: AvantGarde_Bold;
		margin-top: 60px;
		line-height: 50px;
	}
	.contact-details{
		background-image: url('/images/santa-images//santa.png');
		background-repeat: no-repeat;
		background-position: right top;
		min-height: 600px;
	}
	.new-release-lot{ 
		font-size: 28px;
		color: #666666;
		margin-top: 25px;
	}
	.santa-details{
		font-size: 20px;
		line-height: 33px;
		padding-top: 25px;
		color: #3C4C54;
	}
	.santa-details strong{color: #000; font-family: AvantGarde_Bold;}
	@media screen and (max-width: 992px){
		.meet-santa{font-size:42px;}
	}
	
</style>
</head>
<body id="register">
<?php include('../includes/navigation.php'); ?>
<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-md-6 mt-4">
				<h3 class="text-grey">Want to Know more?</h3>
				<h2 class="text-grey">REGISTER NOW</h2>
				<h3 class="text-grey mb-4">Find out about our latest promotions.</h3>
<!--				<p><strong>Pre-register for Valleyview today and you’ll be entitled to:</strong> </p>-->
				
				<h4 class="meet-santa">Meet Santa</h4>
				
				<p class="new-release-lot">New lots released due to high demand.</p>
				
				<p class="santa-details">Valleyview would like thank all our precious customers by inviting Santa to our Sales Office on<strong> December 8<sup>th</sup></strong>, located at 2021 Green Road, Bowmanville. </p>
				
				<p class="santa-details contact-details">For more details contact  <br />
				<a href="tel:+1905-419-6888" style="color: #000;">905-419-6888</a><br />
				or <a href="mailto:info@thevalleyview.ca" style="color: #CB0006">info@thevalleyview.ca</a></p>
<!--
				<ul>
					<li>Advance access to information about the homes and community</li>
					<li>A ‘sneak peek’ at the designs and site BEFORE the general public</li>
					<li>VIP Preview invitation with your choice of the best lots at a preferred price</li>
				</ul>
-->
		</div>
		<div class="col-xs-12 col-md-6 mt-4">
		<p class="text-center text-md-right"><small>Fields marked in <span class="text-green">GREEN *</span> are required.</small></p>
		<form action="https://thevalleyview.ca/subscription/valleyview/processor/process_register.php" method="post">
			<div class="form-group">
				<label for="first_name"><span class="text-green">First Name*</span></label>
				<input type="text" class="form-control" id="first_name" name="first_name" required>
				<input name="middle_name" id="middle_name">
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Last Name*</span></label>
				<input type="text" class="form-control" id="last_name" name="last_name" required>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Email Address</span></label>
				<input type="email" class="form-control" id="email" name="email" required>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">How Did You hear about Us?*</span></label>
				<select name="how_did_you_hear" id="how_did_you_hear" class="form-control" required>
					<option value="">Select one...</option>
					<option value="Agent/Friend Referral">Agent/Friend Referral</option>
					<option value="BuzzBuzz Homes">BuzzBuzz Homes</option>
					<option value="Facebook">Facebook</option>
					<option value="Flyers">Flyers</option>
					<option value="GO Station">GO Station</option>
					<option value="Google">Google</option>
					<option value="Instagram">Instagram</option>
					<option value="Radio">Radio</option>
					<option value="Street-Ad">Street-Ad</option>
					<option value="Text Message">Text Message</option>
					<option value="TV/Magazine">TV/Magazine</option>
				</select>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Are you a Broker?*</span></label>
				<select id="broker" name="broker" required class="form-control">
					<option value="">Select one...</option>
					<option value="YES">YES</option>
					<option value="NO">NO</option>
				</select>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="">Brokerage</span></label>
				<input type="text" class="form-control" id="brokerage" name="brokerage">
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Postal Code</span></label>
				<input type="text" class="form-control" id="postal_zip" name="postal_zip" required>
			</div>
			<div class="form-group">
				<label for="first_name"><span class="text-green">Telephone</span></label>
				<input type="text" class="form-control" id="phone" name="phone" required>
				<input name="fax_number" id="fax_number" value="416.661.7866">
			</div>
	
			<button type="submit" class="btn text-right">Submit</button>
		</form>
		<div class="clearfix"></div>
		<p class="disclaimer">By submitting this form, you are agreeing to receive  communications from WP Development. We do not share information with third parties.</p>	
		
		</div>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
    <script>
		$('#nav-register').addClass(' active');
	</script>
</body>
</html>