	<style>
	.modal-backdrop.show{opacity: .9;}
	.modal-content{background-color: transparent!important;}
</style>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content bg-trans border-0">
      <div class="modal-body">
      	<img src="/images/santa-images/pop-up-santa_01.png" id="closeBtn" class="img-fluid d-block" alt="">
      	<img src="/images/santa-images/pop-up-santa_02.png" class="img-fluid d-block" alt="">
      	<a href="tel:+19054196888">
   			<img src="/images/santa-images/pop-up-santa_03.png" class="img-fluid d-block" alt="">
      	</a>
		<a href="mailto:info@valleyview.ca">
		   	<img src="/images/santa-images/pop-up-santa_04.png" class="img-fluid d-block" alt="">
		</a>
      	<img src="/images/santa-images/pop-up-santa_05.png" class="img-fluid d-block" alt="">
    
        </div>
    </div>
  </div>
</div>

