<?php 	include('../includes/meta.php'); ?>
<title>Area Amenities | Valleyview in Bowmanville</title>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
</head>
<body id="area-amenities">
<?php include('../includes/navigation.php'); ?>
<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<h1 class="text-grey">Stay for the Amenities</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<p>A full, rich life is in view… Valleyview Bowmanville offers your family a rich variety of amenities, all just a short drive from Valleyview. Enjoy great schools, shops, parks and restaurants nearby. Find inspiring outdoor recreation at Bowmanville Harbour Conservation Area, Bowmanville Golf and Country Club, Canadian Tire Motorsport Park and more. Join in community festivals such as BluesBERRY, Summerfest, Maple Syrup and more. Explore over 14 acres of beautiful countryside rich in trails, natural streams, meadows and parks. This is a community that likes to live, play, shop, dine and enjoy life close to home. </p>
			<p class="mb-5"><a href="<?= SITE_URL ?>/pdf/area-amenities.pdf" target="_blank" class="btn light">DOWNLOAD AMENITIES MAP PDF</a></p>
		</div>
		<div class="col-xs-12 col-sm-12">
			<img src="<?= SITE_IMAGES ?>area-amenities.jpg" class="mx-auto d-block img-fluid" alt="Area Amenities, Valleyview in Bowmanville">
			<img src="<?= SITE_IMAGES ?>area-amenities-label.png" class="mx-auto d-block img-fluid" alt="Area Amenities, Valleyview in Bowmanville">
		</div>
	</div>
	<div class="row mt-5 mb-5 pb-4">
<?php for($i=1; $i<= 10 ; $i++)  { ?>
		<div class="amenities-img-wrapper">
			<a href="<?= SITE_IMAGES ?>amenities/amenities<?= $i ?>.jpg" data-fancybox="gallery">
				<img src="<?= SITE_IMAGES ?>amenities/th_amenities<?= $i ?>.jpg" class="mx-auto img-fluid d-block w-100 img-grow" alt="Area Amenities <?= $i ?>">
			</a>
		</div>
<?php } ?>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
	<script>
		$('#nav-area-amenities').addClass(' active');
	</script>
</body>
</html>