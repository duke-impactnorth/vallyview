<?php 	include('../includes/meta.php'); ?>
<title>Site Plan | Valleyview in Bowmanville</title>
</head>
<body id="site-plan">
<?php include('../includes/navigation.php'); ?>
<!-- <div class="container-fluid p-0">
	<img src="/images/hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="Site Plan">
</div> -->
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left pb-3">Vacation Draw</h1> 
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<p class="pb-3">No purchase necessary to enter. Limit of one person per submission when attending Valleyview Bowmanville Sales Office during the Grand Opening weekend.</p>
			<p class="pb-3">Only tickets filled from Oct 27th to Oct 28th before 6pm will qualify for the draw. The draw will take place on Oct 28th at approx 6:05pm in Valleyview Bowmanville Sales Office by on site sales representatives.</p>
			<p class="pb-3">Winners will be notified by email with email provided on the ticket. Prizes will be awarded to the individual named on the ticket. It is the responsibility of the primary tickethoder to allocate the prize if necessary.</p>
			<p class="pb-3">Participants must be at least 18 years or older.</p>
			<p class="pb-3">Vacation prizes will be awarded as a voucher to the primary ticketholder for the value of the trip as advertised. the prize winner may contact the prize supplier to make travel arrangements should they wish to take the trip as described. Voucher has no cash value.</p>
			<p class="pb-3">Prize winner is asked to claim prizes in a timely manner following notification. Any prizes which are not claimed shall be secured for a period of (1) month from the date of the draw. Unclaimed prizes will be null and void. </p>
			<p class="pb-3">This Terms and Conditions is subject to change. We reserve the right to amend or modify this Terms and Conditions at any time.</p>
			<p class="pb-3">Privacy Statement: WP Development is committed to protect your privacy. Personal information collected will be used to administer and evaluate the draw, including fulfilling draw prizes and contacting and publicizing the name and municipality of residence of prize winner. We may also use your personal information to connect with you to share other news, coming events of WP Development. If you wish to be removed from our contact list; Please email <a style="font-family: sans-serif!important;" href="mailto:info@wpdevelopment.ca"><strong>info@wpdevelopment.ca</strong></a> </p>
		</div>

	</div>
</div>
<?php include('../includes/footer.php'); ?>
	<script>
		// $('#nav-site-plan').addClass(' active');
	</script>
</body>
</html>