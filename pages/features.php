<?php 	include('../includes/meta.php'); ?>
<title>Features | Valleyview in Bowmanville</title>
</head>
<body id="features">
<?php include('../includes/navigation.php'); ?>
<div class="container-fluid p-0">
	<img src="/images/hero/features.jpg" class="img-fluid w-100 d-block" alt="Features">
</div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<h3>Distinguished Exteriors</h3>
			<ul class="featured-list">
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
			</ul>
			
			<h3>Connoisseur Kitchens</h3>
			<ul class="featured-list">
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
			</ul>
			
			<h3>Beautiful Bathrooms</h3>
			<ul class="featured-list">
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
			</ul>
			
			<h3>Convenient Laundry Room</h3>
			<ul class="featured-list">
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
			</ul>
			
			<h3>Detailed Interior Trim</h3>
			<ul class="featured-list">
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
			</ul>
			
			<h3>Kitchens</h3>
			<ul class="featured-list pb-5">
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
				<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</li>
				<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
				<li>olores et quas molestias</li>
			</ul>
		</div>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
	<script>
		$('#nav-features').addClass(' active');
	</script>
</body>
</html>