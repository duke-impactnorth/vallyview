<?php 
	include('../includes/meta.php');
	$xml = simplexml_load_file("../xml/floorplans.xml") or die("Error: Cannot create object, XML ERROR XX1010!");
	if(isset($_GET['id']) && isset($_GET['lot'])){
?>

<title><?= ucfirst($_GET['id'])  ?>| Valleyview in Bowmanville</title>
<?php } else { ?>

<title>Floorplans | Valleyview in Bowmanville</title>
<?php } ?>
</head>
<body id="floorplans">
<?php 
	include('../includes/navigation.php');
	if(!isset($_GET['id']) && !isset($_GET['lot'])){
	//SHOW ALL HOUSE
?>
<div class="container-fluid p-0">
	<img src="/images/hero/floorplans.jpg" class="img-fluid w-100 d-block" alt="Floorplans">
</div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<h1 class="text-grey pr-5 pt-4 text-center text-sm-left">Come for the Beauty… <br /> Stay for the Value</h1>
		</div>
		<div class="col-xs-12 col-sm-6 pt-4">
			<p class="text-grey pb-5">Picture a beautiful family community with elegantly designed homes, stunning streetscapes and pathways to open meadows and trails. This is Valleyview, a place where kids can play freely, a place rich in character and country charm. Most houses are on stunning ravine lots, backing on to scenic nature where you might even see a deer with her fawn. No two houses are alike; a rich array of designs and styles keeps the streetscape varied and interesting. Spectacular stone and brick facades elevate the look of the homes, giving the community an upscale flair.</p>
		</div>
	</div>
</div>
<div class="container">
<?php 
$activeLots = array();
foreach($xml->lots as $lot) {
	if($lot['active'] == "1"){
		$activeLots[]  = $lot['size']; 
	}
}
foreach($xml->lots as $lot) {
	if($lot['active'] == "1"){

?>
	<div class="row mb-5" id="<?= $lot['id'] ?>">
		<div class="col-xs-12 col-sm-6">
		<div class="lot-size mx-auto ml-sm-0 d-block">
			<div class="lg" style="background-color: #<?=  $lot['color'] ?>"><strong><?= $lot['size'] ?>&prime;</strong></div>
			<div class="md"></div>
			<div class="sm light">LOT</div>
		</div>
		</div>
		<div class="col-xs-12 col-sm-6 text-center text-sm-right ">
<?php foreach($activeLots as $key => $activelot){ ?>
			<a href="#lot-<?= $activelot ?>" class="fp-link"><?= $activelot ?>&prime; Lots</a><?php if($key < (sizeof($activeLots) - 1)) echo " | ";  ?> 
<?php } ?>
		</div>
<?php foreach($lot->house as $home){
		if($home['active'] == "1"){
?>
		<div class="col-xs-12 col-sm-6 col-md-4">
<!--			<a href="/register.html" class="fp-link">-->
			<a href="/floorplans/<?=$lot['id'] ?>/<?= $home['id'] ?>.html" class="fp-link">
				<img src="/images/renderings/<?= $home['id'] ."-".strtolower($home->elevations['default']) ?>.jpg" class="img-fluid w-100 d-block img-rendering img-grow mb-4 mt-4" alt="">
				<p class="mb-5"><span class="house-name" style="color: #<?= $lot['color'] ?>"><?= $home->name ?></span><br />
				<span class="house-sqft"><?= $home->sqft ?> sq. ft. | Elevation <?= strtoupper($home->elevations['default']) ?></span></p>
			</a>
		</div>
<?php } } ?>		
	</div>
<?php }}  ?>
</div>
<?php } else { //SHOW THE SPECIFIC FLOORPLAN 
$id = $_GET['id'];
$lotId = $_GET['lot'];
$lotExists = false;
$elevations = array();	
foreach($xml->lots as $lot) {
	if(($lot['id'] == $lotId) && ($lot['active'] == "1")){
		foreach($lot as $house){
			if(($house['id'] == $id) && ($house['active'] == "1")){
				$lotExists = true;
				$name = $house->name;
				$lotSize = $lot['size'];
				$color = $lot['color'];
				$sqft = $house->sqft;
				$name = $house->name;
				foreach($house->elevations as $elevation){
					foreach($elevation as $elev){
						$elevations[] = $elev->name;
					}
				}
			} 
		}
	} 
}
if($lotExists){
?>
<div class="container-fluid fp-top bg-white">
	<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<p><span class="house-name" style="color:#<?= $color ?>"><?= $name ?> | </span>  <span class="house-sqft"><?= $sqft ?> sq. ft. </span></p>
		</div>
<?php foreach($elevations as $elev){ ?>
	  <div class="col">
			<img src="/images/renderings/<?= $id ."-".strtolower($elev) ?>.jpg" class="mx-auto ml-md-0 img-fluid d-block img-elevation " alt="Elevation <?= strtoupper($elev) ?> , <?= $name ?>">
			<p class="text-center text-md-left">Elevation <?= strtoupper($elev) ?></p>
		</div>
<?php } ?>
		<div class="col-xs-12  col-lg-12 col-xl-4">
			<div class="lot-size mx-auto ml-xl-0 d-block">
				<div class="lg" style="background-color: #<?=  $color ?>"><strong><?= $lotSize ?>&prime;</strong></div>
				<div class="md"></div>
				<div class="sm light">LOT</div>
		  </div>
		  <div class="download-links">
		  	<p class="text-center text-xl-right mt-4">
			  	<a href="/pdf/<?= $id ?>.pdf" class="btn " target="_blank">DOWNLOAD FLOORPLAN PDF</a>
			 </p>
			 <p class="text-center text-xl-right">
			 	<a href="/floorplans.html" class="btn">&lsaquo; BACK</a>
			 </p>
		  </div>
		</div>
	</div>
	</div>
	</div>
	<div class="container">
	<div class="row mt-5 mb-5" id="Ground">
		<div class="col-xs-12 col-sm-6"><h3>Ground Floor</h3></div>
		<div class="col-xs-12 col-sm-6 text-center text-sm-right">
			<a href="#Ground" class="fp-link">Ground Floor</a> |
			<a href="#Second" class="fp-link">Second Floor</a> |
			<a href="#Base" class="fp-link">Basement</a> 
		</div>
		<div class="col-xs-12 col-sm-12">
			<img src="/images/floorplans/<?= $id ?>-ground.png" class="mx-auto d-block img-fluid" alt="<?=  $name ?> Floorplan , Ground">
		</div>
	</div>
	<div class="row mt-5 mb-5" id="Second">
		<div class="col-xs-12 col-sm-6"><h3>Second Floor</h3></div>
		<div class="col-xs-12 col-sm-6 text-center text-sm-right">
			<a href="#Ground" class="fp-link">Ground Floor</a> |
			<a href="#Second" class="fp-link">Second Floor</a> |
			<a href="#Base" class="fp-link">Basement</a> 
		</div>
		<div class="col-xs-12 col-sm-12">
			<img src="/images/floorplans/<?= $id ?>-second.png" class="mx-auto d-block img-fluid" alt="<?=  $name ?> Floorplan , Second">
		</div>
	</div>
	<div class="row mt-5 mb-5" id="Base">
		<div class="col-xs-12 col-sm-6"><h3>Basement</h3></div>
		<div class="col-xs-12 col-sm-6 text-center text-sm-right">
			<a href="#Ground" class="fp-link">Ground Floor</a> |
			<a href="#Second" class="fp-link">Second Floor</a> |
			<a href="#Base" class="fp-link">Basement</a> 
		</div>
		<div class="col-xs-12 col-sm-12">
			<img src="/images/floorplans/<?= $id ?>-base.png" class="mx-auto d-block img-fluid" alt="<?=  $name ?> Floorplan , basement">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<p class="e-o-e">Plans and specifications are subject to change without notice.<br />
E. & O. E. Actual usable floor space may vary from the stated floor area. All renderings are artist’s concept.</p>
		</div>
	</div>
</div>		
<?php } //if Lot exists
		else { //if No Lot Exits ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 mt-5 mb-5 text-center">
				<h1 class=" mt-5 mb-5">Sorry the Lot/House doesn't Exist.</h1>
				<a href="/floorplans.html" class="">Click here to go BACK</a>
			</div>
		</div>
	</div>
<?php		}
	}
include('../includes/footer.php'); ?>
	<script>
		$('#nav-floorplan').addClass(' active');
	</script>
</body>
</html>