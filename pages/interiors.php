<?php 	include('../includes/meta.php'); ?>
<title>Interiors | Valleyview in Bowmanville</title>
</head>
<body id="interiors">
<?php include('../includes/navigation.php'); ?>
<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-sm-12">
			<img src="<?= SITE_IMAGES ?>interiors-design.jpg" class="mx-auto d-block img-fluid" alt="Come for the design">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<h1 class="text-grey">Come for the Design...</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<p>Cook up a storm in this beautiful, modern, gourmet chef-style kitchen. You will love the fine millwork on the cabinetry, as well as the convenience of extra storage with its taller upper cabinets. The sleek granite countertop is more than just a useful work surface, it’s easy to clean and looks stunning to your guests. Most kitchens are spacious enough to have a breakfast island, perfect for rushed school mornings or homework in the evenings.</p>
		</div>
	</div>
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-sm-12">
			<img src="<?= SITE_IMAGES ?>interiors-style.jpg" class="mx-auto d-block img-fluid" alt="Stay for the Style">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<h1 class="text-grey">Stay for the Style...</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<p>The spacious, open-concept dining room is distinguished by large windows and soaring ceilings. Premium plank hardwood flooring elevates the interior and gives it a rich, luxurious look. The expansive space can easly accommodate all your dining room furniture, while giving you the freedom to decorate it to your taste and need.</p>
		</div>
	</div>
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-sm-12">
			<img src="<?= SITE_IMAGES ?>interiors-luxury.jpg" class="mx-auto d-block img-fluid" alt="Stay for the Luxury">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<h1 class="text-grey">Stay for the Luxury...</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<p>The luxury and serenity of a spa in the comfort and privacy of your home. The sumptuous ensuite bathroom – available in most homes – is designed with a contemporary aesthetic, with chic countertops, double His and Her vanities, gleaming porcelain tiles, designer fixtures and more. Sink into the indulgent luxury of a deep soaker tub and let the soothing waters melt away the stress of your day. There’s also a sleek frameless glass shower enclosure. Bright, clean, modern – timeless style at its best.</p>
		</div>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
	<script>
		$('#nav-interiors').addClass(' active');
	</script>
</body>
</html>