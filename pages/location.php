<?php 	include('../includes/meta.php'); ?>
<title>Location | Valleyview in Bowmanville</title>
</head>
<body id="location">
<?php include('../includes/navigation.php'); ?>
<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-md-6 mt-4">
				<h1 class="text-grey">Come home to <br />Bowmanville. . .</h1>
				<p>Part of the thriving Municipality of Clarington, Bowmanville is among the fastest growing communities in the GTA, the main reasons being its affordable single family housing and easy commute to Toronto. You want to be away from the hustle and bustle, breathe the fresh, pure country air and enjoy an active outdoor lifestyle while staying connected to work and play. Bowmanville offers your family all this and more. The new 407 extension and the planned GO station have made it one of the hottest residential communities in the GTA. This is your great opportunity – you don’t have to sacrifice quality of life for living in Toronto. There has never been a better time to think of starting a new life in Bowmanville. </p>
		</div>
		<div class="col-xs-12 col-md-6 mt-4">
				<div id="map">

				</div>
		</div>
	</div>
	<div class="row location-details">
		<div class="col-xs-12 col-sm-12">
			<p class="text-center text-sm-left"><span class="boldText">SALES OFFICE</span><br /><a href="https://goo.gl/maps/5jkBBdHw5oP2" target="_blank" class="text-grey">2021 Green Rd<br />Bowmanville,<br />ON L1C 3K7</a></p>
			<p class="text-center text-sm-left">Email : <a href="mailto:info@thevalleyview.ca" class="text-grey">info@thevalleyview.ca</a></p>
			<p class="text-center text-sm-left">Phone : <a href="tel:+19055970466" class="text-grey">905.597.0466</a></p>
		</div>
	<?php /*?>	<div class="col-xs-12 col-sm-4 location-div">
			<p class="text-center text-sm-left"><br />Phone :  <a href="tel:+19055970466" class="text-grey">905.597.0466</a><br />Fax :
			<a href="fax:+19055970466" class="text-grey">905.597.0466</a></p>
		</div>
		<div class="col-xs-12 col-sm-6">
			<p class="text-center text-sm-left"><br />Email : <a href="mailto:info@thevalleyview.ca" class="text-grey">info@thevalleyview.ca</a></p>
		</div>
		<?php */?>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 mt-5">
			<img src="<?= SITE_IMAGES ?>location.jpg" class="img-fluid mx-auto d-block" alt="Location, Valleyview in Bowmanville">
		</div>


	</div>
</div>
<?php include('../includes/footer.php'); ?>
    <script>
var historicalOverlay;
function initMap() {
 var myLatLng = {lat: 43.894738, lng: -78.708244};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: myLatLng,
	        scrollwheel: false,
		    gestureHandling: 'cooperative',
		 	styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]}]
  });
	var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!',
		  icon: '<?= SITE_IMAGES ?>map/map-icon.png'
        });

  var imageBounds = {
    north: 43.926008,
    south: 43.880816,
    east: -78.722738,
    west: -78.685054
  };

  historicalOverlay = new google.maps.GroundOverlay(
      '<?= SITE_IMAGES ?>map/overlay.png',
      imageBounds);
  historicalOverlay.setMap(map);
}
    </script>
  <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA14956sOffNcnbMakuFvpOBJw0xIT3mEc&callback=initMap">
    </script>
	<script>
		$('#nav-location').addClass(' active');
	</script>
</body>
</html>