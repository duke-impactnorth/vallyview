<?php include('../includes/meta.php'); ?>
<?php include('../includes/news-snippets.php'); ?>

<?php

//manage pagination
$page_number = 1;
$posts_per_page = 9;

if(isset($_GET['page']) && is_numeric($_GET['page'])){
  $page_number = $_GET['page'];
}
$offset = ($page_number - 1) * $posts_per_page;

$filtered_news_array = array();
$count = 0;
$news_count = 0;

$previous_page = $page_number > 1 && $page_number < count($news_array) ? ($page_number - 1) : "";

$next_page = ($offset + $posts_per_page) >= count($news_array) ? "" : $page_number + 1;


foreach($news_array as $key => $value_array){
  if($count >= $offset){
    $filtered_news_array[$key] = $value_array;
    $news_count++;
  }

  $count++;

  if($news_count >= $posts_per_page){
    break;
  }
}


?>
<title>News | Valleyview in Bowmanville</title>

</head>

<body id="news">
  <?php include('../includes/navigation.php'); ?>



  <div class="container-fluid p-0">
    <img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="text-grey  pt-4 text-center text-sm-left mb-5">News</h1>
      </div>
    </div>
    <div class="row">
      <?php foreach ($filtered_news_array as $key => $value_array) { ?>
        <div class="col-12 col-sm-6 col-lg-4 mb-5 pb-2">


          <div class="card">
            <a href="<?= $value_array['url']; ?>"><img src="<?= $value_array['image']; ?>" class="card-img-top" alt=""></a>
            <div class="card-body news-card-body">
              <div class="news-card-top">
                <h5 class="card-title"><?= $value_array['title']; ?></h5>
                <h6><?= $value_array['date']; ?></h6>
                <p class="card-text news-card-text"><?= $value_array['excerpt']; ?></p>
              </div>
              <a href="<?= $value_array['url']; ?>" class="btn btn-primary">Read More</a>
            </div>
          </div>


        </div>
      <?php } ?>
    </div>

    <div class="col-12 mb-4">
      <?php if(!empty($previous_page)){ ?>
      <a href="<?= SITE_URL . '/news.html?page=' . $previous_page; ?>">
      <button class="btn btn-alert">Previous Page</button>
      </a>
      <?php } ?>
      <?php if(!empty($next_page)){ ?>
      <a href="<?= SITE_URL . '/news.html?page=' . $next_page; ?>">
      <button class="mx-3 btn btn-alert">Next Page</button>
      </a>
      <?php } ?>
    </div>
  </div>
  <?php include('../includes/footer.php'); ?>
</body>

</html>