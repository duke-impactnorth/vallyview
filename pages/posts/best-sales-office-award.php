<?php include('../../includes/meta.php'); ?>
<?php include('../../includes/news-snippets.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>Best Sales Office Award | Valleyview in Bowmanville</title>
</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left"><?= $news_array['best-sales-office-award']['title']; ?></h1>
            <h5><?= $news_array['best-sales-office-award']['date']; ?></h5>
		</div>
	</div>
	<div class="row">
        <div class="col-12">
        <img src="<?= $news_array['best-sales-office-award']['image']; ?>" class="mx-auto img-fluid d-block w-100" alt="New Community Centre Coming" style="max-width: 380px;">
        </div>
        <div class="col-12 my-5">
            <p>Valleyview in Bowmanville displays beauty on every corner. Our homes are built with careful attention to detail, bringing luxury and sophistication with every step. The homes we build are about more than just being a place to live &ndash; the importance lies in the experience you have when you walk in the front door. That&rsquo;s what our sales office is designed to do: provide a special moment that makes you feel like you&rsquo;ve truly found your home. That&rsquo;s why, in 2019, Valleyview proudly won the DRHBA for Best Sales Office award.</p>
            <p>The Valleyview sales office welcomes everyone to walk through and see first-hand the contemporary styles available to create your dream home. Our team is honoured to have been recognized as being one of the best by the Durham Region Home Builders&rsquo; Association <a href="https://www.drhba.com/">(DRBHA)</a>.</p>
            <p>What is it that makes Valleyview stand out? Every home is unique and crafted with the finest features and finishes, offering a new collection of 37', 40' &amp; 48'&nbsp;detached homes starting from the low $700s.</p>
            <p>Our goal is to provide each person with the highest standard of living. Whether it be from the spacious gourmet style kitchen, open-concept dining area, or the spa-inspired bathroom, each room is designed to bring the utmost satisfaction. Living here will make you excited to wake up in the morning and have you looking forward to coming home at the end of the day. There are plenty of opportunities that come with living at Valleyview, from great schools to delicious restaurants. &nbsp;</p>
            <p>Bowmanville is a growing community and the perfect destination to raise a family. Valleyview is a sought-out neighborhood in the GTA that provides convenience and charm in one place. Look out your window and you will see the picturesque views of valleys and meadows moments away. The natural greenery adds to the serene atmosphere, making every day extraordinary. Travelling around Bowmanville is simple especially with the new highways 418 and 407 through highway 115/35 giving you easy access to big city amenities.</p>
            <p>The DRHBA has encouraged us at Valleyview to continue striving for excellence in everything that we do. We wish the best of luck to this year&rsquo;s participants on the upcoming award ceremonies. Come see our award-winning Sales Office located at 2021 Green Road.</p>
            <p>Follow our social platforms to stay up-to-date on new releases and promos.</p>
        </div>

        <div class="col-12 col-md-6 text-center">
            <p>Instagram:</p>
            <p><a href="https://www.instagram.com/valleyview_bowmanville/" target="_blank" rel="noopener noreferrer">@valleyview_bowmanville</a></p>
            <p><a href="https://www.instagram.com/wp.development/" target="_blank" rel="noopener noreferrer">@wp.development</a></p>
        </div>

        <div class="col-12 col-md-6 text-center">
            <p>Facebook:</p>
            <p><a href="https://www.facebook.com/Valleyview-Bowmanville-100255251401058/" target="_blank" rel="noopener noreferrer">@Valleyviewbowmanville</a></p>
            <p><a href="https://www.facebook.com/WP-Development-Inc-168421577165999/" target="_blank" rel="noopener noreferrer">@WPDevelopmentInc</a></p>
        </div>

    </div>
</div>
<?php include('../../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>
</html>