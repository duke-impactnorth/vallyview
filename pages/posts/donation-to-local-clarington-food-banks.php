<?php include('../../includes/meta.php'); ?>
<?php include('../../includes/news-snippets.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>Valleyview in Bowmanville Donates $30,000 to Local Clarington Food Banks | Valleyview in Bowmanville</title>
</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left"><?= $news_array['donation-to-local-clarington-food-banks']['title']; ?></h1>
            <h5><?= $news_array['donation-to-local-clarington-food-banks']['date']; ?></h5>
		</div>
	</div>
	<div class="row">
        <div class="col-12">
        <img src="<?= $news_array['donation-to-local-clarington-food-banks']['image']; ?>" class="mx-auto img-fluid d-block w-100" alt="New Community Centre Coming" style="max-width: 380px;">
        </div>
        <div class="col-12 my-5">
            <p>WP Development is a proud partner in the Bowmanville/Clarington community. Our passion goes beyond building quality crafted homes, as we love to support and give back to those in need. During this difficult time, we understand that many organizations in the area are struggling to provide the necessities for those who cannot feed their families. We want to be there to help &ndash; WP Development will be donating $30,000 to the City of Clarington that will fund three local food banks: Feed the Need in Durham, Clarington East Food Bank and Bethesda House.</p>

            <p>The Mayor of Clarington, Adrian Foster helped make this donation an honourable success. We are confident that as a community, if we all continue to come together, we can get through this global pandemic and come out of it stronger. Let&rsquo;s take this opportunity to live and grow in harmony.</p>

            <p>All proceeds with go directly to the acquired Food Bank. The <a href="https://feedtheneedindurham.ca/" rel="noopener noreferrer" target="_blank">Feed the Need in Durham</a> will receive $20,000, <a href="http://claringtoneastfoodbank.ca/" rel="noopener noreferrer" target="_blank">Clarington East Food Bank</a>&nbsp; and <a href="https://bethesdahouse.ca/" rel="noopener noreferrer" target="_blank">Bethesda House</a> will receive $5,000 each.</p>

            <p>Valleyview in Bowmanville is excited to be a part of this amazing cause. We are offering virtual appointments so you can still purchase your dream home. Contact our team via phone, email or video conference at <a href="tel:905-567-0466">905-567-0466</a> or <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a>.</p>

        </div>

        <?php
        /*
        <div class="col-12 col-md-6 text-center">
            <p>Instagram:</p>
            <p><a href="https://www.instagram.com/valleyview_bowmanville/" target="_blank" rel="noopener noreferrer">@valleyview_bowmanville</a></p>
            <p><a href="https://www.instagram.com/wp.development/" target="_blank" rel="noopener noreferrer">@wp.development</a></p>
        </div>

        <div class="col-12 col-md-6 text-center">
            <p>Facebook:</p>
            <p><a href="https://www.facebook.com/Valleyview-Bowmanville-100255251401058/" target="_blank" rel="noopener noreferrer">@Valleyviewbowmanville</a></p>
            <p><a href="https://www.facebook.com/WP-Development-Inc-168421577165999/" target="_blank" rel="noopener noreferrer">@WPDevelopmentInc</a></p>
        </div>
        */
        ?>

    </div>
</div>
<?php include('../../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>
</html>