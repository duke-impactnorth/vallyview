<?php include('../../includes/meta.php'); ?>
<?php include('../../includes/news-snippets.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>There's a GO Train Coming to Bowmanville | Valleyview in Bowmanville</title>
</head>

<body id="news">
    <?php include('../../includes/navigation.php'); ?>

    <div class="container-fluid p-0">
        <img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-grey  pt-4 text-center text-sm-left"><?= $news_array['go-train-coming-to-bowmanville']['title']; ?></h1>
                <h5><?= $news_array['go-train-coming-to-bowmanville']['date']; ?></h5>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <img src="<?= $news_array['go-train-coming-to-bowmanville']['image']; ?>" class="mx-auto img-fluid d-block w-100" alt="There's a GO Train coming to Bowmanville" style="max-width: 380px;">
            </div>
            <div class="col-12 my-5">

                <p>Where do you need to GO? Get ready for quick commuting around the GTA when you live at Valleyview in Bowmanville. The Durham Region proposed to Metrolinx the idea to expand GO Train rail service beyond Oshawa in Durham Region to Bowmanville with two-way train service for an easier, faster and more convenient option for residents to commute by train all the way to <a href="https://globalnews.ca/news/6548364/bowmanville-go-transit-durham-region/" target="_blank" rel="noopener noreferrer">Union Station in Downtown Toronto</a>. The country charm of living in Valleyview is making its way into the city one stop at a time.</p>

                <p>The GO train service proposal looks to expand eastward from the current GO station in Oshawa utilizing the Canadian Pacific Railways General Motors spur line and possibly adding four new stations at Thornton&rsquo;s Corners East, Ritson Road, Courtice and Bowmanville. Currently, GO Transit provides bus service from Bowmanville to Oshawa every 30 minutes.</p>

                <p>This project is long overdue. There were many roadblocks that have prevented a GO train from being introduced in the past but Metrolinx, along with others are working to make this <a href="http://www.metrolinx.com/en/greaterregion/projects/bowmanville-expansion.aspx" target="_blank" rel="noopener noreferrer">exciting project a reality</a>. Residents at Valleyview may be able to look forward to a more affordable option when travelling out of town. Driving is convenient with access to Hwy 407 and 401, nevertheless, filling up on gas can become costly. Early morning commutes can put you at ease knowing you can hop on the train moments away and make it to your destination in no time.</p>

                <p>Bowmanville, in the municipality of Clarington, continues to grow and become an even more wonderful place to live. Experience country charm in a serene atmosphere while still having access to urban amenities with many convenient transportation options.</p>

                <p>Valleyview homes offer ravine lots that rest alongside picturesque greenspaces and scenic nature trails. Raise your family in a place where your children can play in the peaceful parks, pathways and open meadows, yet still have the conveniences of a city amenities. These homes are designed and styled with stone and brick facades, creating an upscale appearance. Have an experience like no other when you purchase one of our luxurious bungalows or 2-storey 37&rsquo;, 40&rsquo; &amp; 48&rsquo; single-detached homes.</p>

                <p>Come see our award-winning Sales Office located at <a href="https://goo.gl/maps/mWJAzBwz75NiAKuy5" target="_blank" rel="noopener noreferrer">2021 Green Road</a>.</p>

                <p>Follow our social platforms to stay up-to-date on new releases and promos.</p>
            </div>

            <div class="col-12 col-md-6 text-center">
                <p>Instagram:</p>
                <p><a href="https://www.instagram.com/valleyview_bowmanville/" target="_blank" rel="noopener noreferrer">@valleyview_bowmanville</a></p>
                <p><a href="https://www.instagram.com/wp.development/" target="_blank" rel="noopener noreferrer">@wp.development</a></p>
            </div>

            <div class="col-12 col-md-6 text-center">
                <p>Facebook:</p>
                <p><a href="https://www.facebook.com/Valleyview-Bowmanville-100255251401058/" target="_blank" rel="noopener noreferrer">@Valleyviewbowmanville</a></p>
                <p><a href="https://www.facebook.com/WP-Development-Inc-168421577165999/" target="_blank" rel="noopener noreferrer">@WPDevelopmentInc</a></p>
            </div>

        </div>
    </div>
    <?php include('../../includes/footer.php'); ?>
    <script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>

</html>