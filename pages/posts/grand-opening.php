<?php 	include('../../includes/meta.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">

<title>Grand Opening | Valleyview in Bowmanville</title>



</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container p-0 mt-3">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey pt-4 text-left">Grand Opening</h1>
            <h5>10/27/18</h5>
		</div>
	</div>
	<div class="row">
        <div class="col-12 my-5">
            Grand Opening was a huge success with over 80% sold out.
        </div>

        <?php for($i = 1; $i < 60; $i++){ ?>
        <?php //skip the following
            if(in_array($i, [2, 3, 4, 20, 21, 22, 23, 24, 42,43,46, 47, 48, 50, 51, 55])) continue;
        ?>
        <div class="col-12 col-sm-6 col-md-3 my-3">
            <a data-fancybox="gallery" href="<?= SITE_IMAGES; ?>/posts/grand-opening-<?= $i; ?>.JPG">
                <img src="<?= SITE_IMAGES; ?>/posts/grand-opening-<?= $i; ?>.JPG" class="mx-auto img-fluid d-block w-100 img-grow" alt="Grand Opening <?= $i ?>">
            </a>
        </div>
        <?php } ?>
    </div>
</div>
<?php include('../../includes/footer.php'); ?>

<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>


</body>
</html>