<?php 	include('../../includes/meta.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>Mortgage Seminar | Valleyview in Bowmanville</title>
</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left">Mortgage Seminar</h1>
            <h5>05/23/19</h5>
		</div>
	</div>
	<div class="row">
        <div class="col-12 my-5">
            Valleyview held a Free Information seminar discussing various topics for purchasers.  Topics included finding the best mortgage, trends in Mortgage rates, Steps to being pre-approved, resale vs. new construction and so much more.
        </div>

        <?php for($i = 1; $i < 16; $i++){ ?>
        <?php //skip the following
            if(in_array($i, [1, 2, 5, 6, 12, 14, 15, 16])) continue;
        ?>
        <div class="col-12 col-sm-6 col-md-3 my-3">
            <a data-fancybox="gallery" href="<?= SITE_IMAGES; ?>/posts/mortgage-seminar-<?= $i; ?>.JPG">
                <img src="<?= SITE_IMAGES; ?>/posts/mortgage-seminar-<?= $i; ?>.JPG" class="mx-auto img-fluid d-block w-100 img-grow" alt="Grand Opening <?= $i ?>">
            </a>
        </div>
        <?php } ?>
    </div>
</div>
<?php include('../../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>
</html>