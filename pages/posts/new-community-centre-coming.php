<?php 	include('../../includes/meta.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>New Community Centre Coming | Valleyview in Bowmanville</title>
</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left">Valleyview– Bowmanville is Getting A New Community Centre in 2023!</h1>
            <h5>01/06/20</h5>
		</div>
	</div>
	<div class="row">
        <div class="col-12">
        <img src="<?= SITE_IMAGES; ?>/posts/new-community-centre-coming.png" class="mx-auto img-fluid d-block w-100" alt="New Community Centre Coming" style="max-width: 380px;">
        </div>
        <div class="col-12 my-5">

        <p>Coming to Bowmanville in 2023 is a fun-filled, extravagant Community Centre that will only be steps away from our Valleyview new home community. The Clarington municipal government has approved plans for the building of a $48 million facility that will benefit all the citizens in the region. This is just the beginning. The plan is to construct the community centre in two phases to create a convenient and functional public space.</p>
        <p>This community centre is going to be the start of Bowmanville&rsquo;s transformational new journey as the area continues to thrive and be a wonderful gathering place for new and growing families. Plenty of research and planning went into the design of the Community Centre based on demographics, trends and overall needs. It is an investment that the Municipality of Clarington is making towards the future of Bowmanville.</p>
        <p>The province of Ontario will be contributing 73% of funding that will be finalized in summer 2020. The new community centre will be an extension to the existing Bowmanville indoor soccer facilities located on Baseline Road. Representing a central point in Bowmanville, this exciting project is a way to serve locals and be a main attraction for visitors looking to host sporting events and more. The project will play a crucial role in the initiative to grow Bowmanville&rsquo;s population, which is a priority for the community and will no doubt be a booming success.</p>
        <p>Upon completion of the community centre, you will be able to participate in many leisurely activities, including:</p>
        <p>Phase 1</p>
        <ul>
        <li>Ice skating rinks</li>
        <li>An indoor walking track</li>
        <li>A full-sized gymnasium</li>
        <li>Community and administrative office space</li>
        <li>A branch of the Clarington Public Library and multi-purpose community and programming space</li>
        </ul>
        <p>Anticipated Phase 2</p>
        <ul>
        <li>Eight-lane pool</li>
        <li>A large therapeutic pool and fitness facility</li>
        </ul>
        <p>Valleyview is a gorgeous new home development, featuring 2-storey single-detached homes and bungalows with both traditional and transitional designs. Modern kitchens with a gourmet chef-style appearance, granite countertops and fine millwork on the cabinetry and spacious living room designs provide an elegant experience in a luxurious setting. Every room is unique and rich in its own way.</p>
        <p>Along with the stunning interiors, the neighbourhood overlooks the beautiful scenery of the surrounding valleys and meadows. This is the perfect destination to fit your family needs and lifestyle.</p>
        <p>The Community Centre is going to be an excellent addition that will make living at Valleyview enjoyable. Bowmanville is near countless amenities, so you do not have to travel far for great shopping, dining and entertainment. Nevertheless, if you wish to explore outside of Bowmanville into the city, the 407 and 418 are now open, giving you easy access from home.</p>
        <p>For more information on buying a new detached home at Valleyview Bowmanville, schedule an appointment to visit our Sales Office located at <a href="https://goo.gl/maps/mWJAzBwz75NiAKuy5" target="_blank" rel="noopener noreferrer">2021 Green Road</a>. To arrange your visit, call <a href="tel:9054196888">905.419.6888</a> or email <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a>.&nbsp;&nbsp;</p>
        <p>Follow Valleyview Bowmanville on social media <a href="https://www.instagram.com/valleyview_bowmanville/">@valleyview_bowmanville</a> &amp; <a href="https://www.instagram.com/wp.development/">@wp.development</a></p>

        </div>

        <div class="col-12 col-md-6 my-3">
            <a data-fancybox="gallery" href="<?= SITE_IMAGES; ?>/posts/rec-centre-preliminary-concept.jpg">
                <img src="<?= SITE_IMAGES; ?>/posts/rec-centre-preliminary-concept.jpg" class="mx-auto img-fluid d-block w-100 img-grow" alt="New Rec Centre Additions">
            </a>
        </div>

        <div class="col-12 col-md-6 my-3">
            <a data-fancybox="gallery" href="<?= SITE_IMAGES; ?>/posts/new-community-centre-coming-animated.gif">
                <img src="<?= SITE_IMAGES; ?>/posts/new-community-centre-coming-animated.gif" class="mx-auto img-fluid d-block w-100 img-grow" alt="Anticipated Phase 2 Activities">
            </a>
        </div>



    </div>
</div>
<?php include('../../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>
</html>