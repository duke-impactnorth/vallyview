<?php 	include('../../includes/meta.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>New Release! | Valleyview in Bowmanville</title>
</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey pt-4 text-center text-sm-left">New Release!</h1>
            <h5>11/16/19</h5> 
		</div>
	</div>
	<div class="row">
        <div class="col-12 my-5">
          Check out some photos from our new release on November 16th, 2019!
        </div>

        <?php for($i = 1; $i < 9; $i++){ ?>
        <div class="col-12 col-sm-6 col-md-3 my-3">
            <a data-fancybox="gallery" href="<?= SITE_IMAGES; ?>/posts/new-release-<?= $i; ?>.JPG">
                <img src="<?= SITE_IMAGES; ?>/posts/new-release-<?= $i; ?>.JPG" class="mx-auto img-fluid d-block w-100 img-grow" alt="New Release <?= $i ?>">
            </a>
        </div>
        <?php } ?>
    </div>
</div>
<?php include('../../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>
</html>