<?php include('../../includes/meta.php'); ?>
<?php include('../../includes/news-snippets.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>Online and In-Person Appointments | Valleyview in Bowmanville</title>
</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left"><?= $news_array['online-and-in-person-appointments']['title']; ?></h1>
            <h5><?= $news_array['online-and-in-person-appointments']['date']; ?></h5>
		</div>
	</div>
	<div class="row">
        <div class="col-12">
        <img src="<?= $news_array['online-and-in-person-appointments']['image']; ?>" class="mx-auto img-fluid d-block w-100" alt="New Community Centre Coming" style="max-width: 380px;">
        </div>
        <div class="col-12 my-5">
            <p>At Valleyview, the well-being of our employees and customers remain our top priority. At this moment, the best way for us to protect each other is by practicing social, and physical, distancing. Given the rapidly evolving COVID-19 situation, we are revising our policies and procedures to ensure we stay updated with the latest information from government and health officials. Effective immediately, our sales office will be open by appointment only.</p>
            <p>In addition to being accessible by phone and email, we are also offering online virtual appointments. If you are interested in learning more about Valleyview Bowmanville, or have questions and concerns that need to be answered, our team is here to help.</p>
            <p>We will continue to monitor the situation closely and provide further information as it becomes available.</p>
            <p>We hope everyone stays safe and healthy during these uncertain times. Together we can flatten the curve and make for a healthier tomorrow!</p>
            <p>To book an online virtual appointment or if you have any questions or inquiries regarding Valleyview in Bowmanville, please do not hesitate to reach out to our representatives. Please contact our sales office at <a href="tel:19055970466">905.597.0466</a> or <a href="mailto:info@thevalleyview.ca">info@thevalleyview.ca</a>.</p>
            <p>To stay up-to-date on news about our current development follow our social platforms:</p>
        </div>

        <div class="col-12 col-md-6 text-center">
            <p>Instagram:</p>
            <p><a href="https://www.instagram.com/valleyview_bowmanville/" target="_blank" rel="noopener noreferrer">@valleyview_bowmanville</a></p>
            <p><a href="https://www.instagram.com/wp.development/" target="_blank" rel="noopener noreferrer">@wp.development</a></p>
        </div>

        <div class="col-12 col-md-6 text-center">
            <p>Facebook:</p>
            <p><a href="https://www.facebook.com/Valleyview-Bowmanville-100255251401058/" target="_blank" rel="noopener noreferrer">@Valleyviewbowmanville</a></p>
            <p><a href="https://www.facebook.com/WP-Development-Inc-168421577165999/" target="_blank" rel="noopener noreferrer">@WPDevelopmentInc</a></p>
        </div>

    </div>
</div>
<?php include('../../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>
</html>