<?php include('../../includes/meta.php'); ?>
<?php include('../../includes/news-snippets.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>Valleyview Bowmanville Reopens Its Sales Centre to the Public | Valleyview in Bowmanville</title>
</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left"><?= $news_array['sales-office-open-final']['title']; ?></h1>
            <h5><?= $news_array['sales-office-open-final']['date']; ?></h5>
		</div>
	</div>
	<div class="row">
        <div class="col-12">
        <img src="<?= $news_array['sales-office-open-final']['image']; ?>" class="mx-auto img-fluid d-block w-100" alt="Sales Office Reopening" style="max-width: 380px;">
        </div>
        <div class="col-12 my-5">
            <p>Valleyview Bowmanville has opened its doors to the public as of Saturday, May 30th, 2020. We are excited to be back and are ready to serve you. Working remotely has allowed our team to prepare and bring forth a new level of skill that go towards helping you find your dream home. Valleyview welcomes you to join us for this amazing opportunity to live in Bowmanville. </p>

            <p>We would like to remind you that while we are again offering in-person appointments, the health and safety of our staff and customers is of the utmost importance. As we transition into phase 1 openings, the next steps are to be taken with extreme caution and care. This includes implementing the following protocols: </p>

            <ul>
              <li>Booking private, 1-on-1 meetings in our controlled environment </li>
              <li>A carefully sanitized showroom </li>
              <li>All visitors must sign a disclosure form with COVID-19 questions prior to entry</li>
              <li>NO food or drinks will be allowed in the building</li>
              <li>NO public washroom access</li>
              <li>All visitors must bring your own masks and gloves </li>
              <li>All information will be shown via touchless presentation </li>
              <li>Agreements will be finalized with electronic signatures</li>
            </ul>

            <p>It is important that everyone respects the social and physical distancing regulations for a smooth transition. If you have any questions or require assistance, we are here to help.</p>

            <p>Situated in the picturesque Bowmanville, Ontario surrounded by acres of valleys and meadows, the country charm at Valleyview delivers tranquility with every step. Offering the best value of 37’,40’ & 48’ detached homes in the GTA from the low $700s. There are many reasons why Valleyview in Bowmanville is the ideal living destination, located in an established neighbourhood minutes from highways 401, 407, and the upcoming Bowmanville GO station.  </p>

            <p>We look forward to seeing you at our sales centre and will do everything we can to help find your new home. To book an appointment, contact a sales representative via phone or email at <a href="tel:905-597-0466">905.597.0466</a> or <a href="mailto:info@valleyview.ca">info@valleyview.ca</a></p>

            <p>For updates, follow us on social media! </p>

        </div>



        <div class="col-12 col-md-6 text-center">
            <p>Instagram:</p>
            <p><a href="https://www.instagram.com/valleyview_bowmanville/" target="_blank" rel="noopener noreferrer">@valleyview_bowmanville</a></p>
            <p><a href="https://www.instagram.com/wp.development/" target="_blank" rel="noopener noreferrer">@wp.development</a></p>
        </div>

        <div class="col-12 col-md-6 text-center">
            <p>Facebook:</p>
            <p><a href="https://www.facebook.com/Valleyview-Bowmanville-100255251401058/" target="_blank" rel="noopener noreferrer">@Valleyviewbowmanville</a></p>
            <p><a href="https://www.facebook.com/WP-Development-Inc-168421577165999/" target="_blank" rel="noopener noreferrer">@WPDevelopmentInc</a></p>
        </div>



    </div>
</div>
<?php include('../../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>
</html>