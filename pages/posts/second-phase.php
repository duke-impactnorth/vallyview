<?php 	include('../../includes/meta.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>Second Phase | Valleyview in Bowmanville</title>
</head>
<body id="news">
<?php include('../../includes/navigation.php'); ?>

<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left">Second Phase</h1>
            <h5>03/30/19</h5>
		</div>
	</div>
	<div class="row">
        <div class="col-12 my-5">
            Phase 2 brought more purchasers to Valleyview in Bowmanville with another successful release of homes.
        </div>

        <?php for($i = 1; $i < 30; $i++){ ?>
        <?php //skip the following
            if(in_array($i, [4, 6, 20, 21, 22, 23, 24, 25, 27, ])) continue;
        ?>
        <div class="col-12 col-sm-6 col-md-3 my-3">
            <a data-fancybox="gallery" href="<?= SITE_IMAGES; ?>/posts/second-phase-<?= $i; ?>.JPG">
                <img src="<?= SITE_IMAGES; ?>/posts/second-phase-<?= $i; ?>.JPG" class="mx-auto img-fluid d-block w-100 img-grow" alt="Grand Opening <?= $i ?>">
            </a>
        </div>
        <?php } ?>
    </div>
</div>
<?php include('../../includes/footer.php'); ?>
<script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>
</html>