<?php include('../../includes/meta.php'); ?>
<?php include('../../includes/news-snippets.php'); ?>
<link href="<?= SITE_URL ?>/css/jquery.fancybox.min.css" rel="stylesheet">
<title>Things to do in Bowmanville during March Break | Valleyview in Bowmanville</title>
</head>

<body id="news">
    <?php include('../../includes/navigation.php'); ?>

    <div class="container-fluid p-0">
        <img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="News Page">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-grey  pt-4 text-center text-sm-left"><?= $news_array['things-to-do-in-bowmanville-during-march-break']['title']; ?></h1>
                <h5><?= $news_array['things-to-do-in-bowmanville-during-march-break']['date']; ?></h5>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <img src="<?= $news_array['things-to-do-in-bowmanville-during-march-break']['image']; ?>" class="mx-auto img-fluid d-block w-100" alt="Things to do in Bowmanville during March Break" style="max-width: 380px;">
            </div>
            <div class="col-12 my-5">

                <p>Save your money on traveling and bring fun to you in Bowmanville this March Break. Enjoy a well-deserved rest from the hustle and bustle of preparing school lunches, carpooling and after school pickups. It&rsquo;s time to plan an exciting week with the kids to ensure everyone is happy and relaxed. There are plenty of activities happening around Bowmanville that are just minutes from the exciting new home community of Valleyview. Let your staycation begin.</p>

                <p>From day camps to community events, check it out below.</p>

                <ul>
                    <li>The Clarington library is hosting a <a href="https://www.clarington-library.on.ca/calendar-node-field-mt-event-date/month/2020-03?field_mt_event_audience_tid=All&amp;field_mt_event_location_tid=All&amp;field_mt_event_category_tid=34" target="_blank" rel="noopener noreferrer">week full of activities starting March 16th</a>. </li>
                    <li>The Visual Arts Centre of Clarington is having a <a href="https://www.vac.ca/march-break-camps.html" target="_blank" rel="noopener noreferrer">March Break camp</a>.</li>
                    <li>Full day camps begin at 9:00 am and finishes at 4:00 pm.</li>
                    <li>Morning Half day camps begin at 9:00 am and finish at 12:00 pm.</li>
                    <li>Afternoon Half Day camps begin at 1:00 pm and finish at 4:00 pm.</li>
                    <li>The Clarington Museum is having FREE <a href="https://claringtonmuseums.com/programs-and-events/" target="_blank" rel="noopener noreferrer">March Break drop-in events</a> during March 17<sup>th</sup> , 18<sup>th</sup> and 19<sup>th</sup> from 10:00am-12:00pm.</li>
                    <li>There&rsquo;s a unique event taking place on March 15<sup>th</sup> called, <a href="https://picksandgiggles.com/all-things-handmade-market/" target="_blank" rel="noopener noreferrer">&lsquo;All Things Handmade&rsquo;</a>.</li>
                </ul>
                <p>If you would like to explore the streets of Bowmanville, within minutes of the site are picturesque trails and conservation areas. You can walk through the stunning paths and take in the outstanding beauty that makes up the area. Here are a few must-visits:</p>
                <ul>
                    <li>Bowmanville Harbour Conservation Area, which is only 10 minutes away.</li>
                    <li>Bowmanville Valley Conservation Area, which is only 4 minutes away.</li>
                    <li>Soper Creek Trail, which is only 10 minutes away.</li>
                </ul>
                <p><strong>Why is Bowmanville an Ideal Destination? </strong></p>
                <p>Valleyview is offering an exceptional collection of bungalows and 2-storey 37&rsquo;, 40&rsquo; &amp; 48&rsquo; single-detached homes situated in an ideal location surrounded by natural green spaces and parklands. There is so much to do and love in Bowmanville, but what exactly is it that makes this community such a wonderful place to call home? &nbsp;&nbsp;&nbsp;</p>
                <ul>
                    <li>In 2019, Bowmanville ranked as the <a href="https://www.moneysense.ca/spend/real-estate/where-to-buy-2019-durham/" target="_blank" rel="noopener noreferrer">best place to live the in Durham region</a> by Moneysense Magazine</a>. The blend of country charm and big city amenities creates a beautiful and connected place that&rsquo;ll truly feel like home.</li>
                    <li>Valleyview offers the best value of Detached Homes, compared to other areas in the GTA. High traffic areas such as Aurora, Vaughan or Mississauga tend to be more expensive due to population and other affiliated factors. Whereas, Valleyview offers homes starting from the low $700s, which is still lower than the average price in the Durham/Clarington region.</li>
                </ul>
                <p>Every home displays quality craftmanship that exemplifies beauty all throughout &ndash; a gourmet style kitchen, elegant dining area and a spa inspired bathroom bring you comfort in the comfort of your own home.</p>
                <ul>
                    <li>Situated minutes to Hwy 401, 407 and future GO Station, you will have easy access to the city and other areas in the GTA. Explore the downtown core and trendy hot-spots just moments from where you live.</li>
                    <li>This is a family friendly neighborhood that provides a safe and caring environment. With plenty of schools and annual events that take place, this is the perfect destination to raise your family.</li>
                </ul>
                <p>Construction has started at Valleyview and you can move in 2020! The real estate market is picking up, so act fast as these are an early closing. Visit our Sales Centre located at <a href="https://goo.gl/maps/mWJAzBwz75NiAKuy5" target="_blank" rel="noopener noreferrer">2021 Green Road</a>.</p>
            </div>



        </div>
    </div>
    <?php include('../../includes/footer.php'); ?>
    <script src="<?= SITE_URL; ?>/js/jquery.fancybox.min.js"></script>
</body>

</html>