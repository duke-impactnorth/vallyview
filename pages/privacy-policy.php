<?php 	include('../includes/meta.php'); ?>
<title>Site Plan | Valleyview in Bowmanville</title>
</head>
<body id="site-plan">
<?php include('../includes/navigation.php'); ?>
<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>/hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="Site Plan">
</div>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 mt-4">
			<h2 class="text-grey">Privacy Policy</h2>
			<!-- <h4 class="text-grey">Come for the Quality…Stay for the Affordability!</h4> -->
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 mt-4">
				<p>This Privacy Policy governs the manner in which WP Development collects, uses, maintains and discloses information collected from users (each, a "User") of the wpdevelopment.ca website ("Site"). This privacy policy applies to the Site and all products and services offered by WP Development.</p>
				<h2 class="h6 font-weight-bold">Personal identification information</h2>
				<p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>
				<h3 class="h6 font-weight-bold">Non-personal identification information</h3>
				<p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>
				<h3 class="h6 font-weight-bold">Cookies and Personalization Technologies</h3>
				<p>The WP Development&#8217;s website uses cookies, a small text file (up to 4KB) created by our website through your browser that is stored on your digital device either temporarily for that session only or permanently on the device&#8217;s memory storage hardware. Cookies may provide a way for us to recognize you and keep track of your behavior and preferences to allow us to offer you individual and personalized service on website pages and commercial electronic communications such as email.</p>
				<p>In addition to cookies, our website may use session variables, web beacons/tracking tags, other technologies and third-party software services to provide you with unique individual experiences.</p>
				<p>Depending on the digital devices and the software you use to access our website, you may have the option to disable or clear the cookies and other technology that may identify you. Consult your user guide or other resources on how to do this. However, by doing so you will not benefit from the personalization of service we can provide. You may not be able to use all the features of our website, store your preferences, our webpages may not display properly and you may not receive personalized and timely commercial electronic messages with high value offers.</p>
				<p><strong>Third-party Cookies</strong> In addition to our own cookies, we may also use third-party cookies for the purposes of gathering and storing web traffic data, advertising and media services, social media channels and other uses.</p>
				<h3 class="h6 font-weight-bold">How we use collected information</h3>
				<p>WP Development may collect and use Users personal information for the following purposes:</p>
				<ul>
				<li><em>To improve customer service</em><br> Information you provide helps us respond to your customer service requests and support needs more efficiently.</li>
				<li><em>To personalize user experience</em><br> We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.</li>
				<li><em>To improve our Site</em><br> We may use feedback you provide to improve our products and services.</li>
				<li><em>To send periodic emails</em><br> We may use the email address to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</li>
				</ul>
				<h3 class="h6 font-weight-bold">How we protect your information</h3>
				<p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>
				<h3 class="h6 font-weight-bold">Sharing your personal information</h3>
				<p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>
				<h3 class="h6 font-weight-bold">Third party websites</h3>
				<p>Users may find content on our Site that links to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website&#8217;s own terms and policies.</p>
				<h3 class="h6 font-weight-bold">Changes to this privacy policy</h3>
				<p>WP Development has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
				<h3 class="h6 font-weight-bold">Your acceptance of these terms</h3>
				<p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>
				<h4 class="h6 font-weight-bold pt-4">Contacting Us</h4>
				<p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:</p>
				<p>WP Development<br> 230-7181 Woodbine Ave.<br> Markham. ON.<br> L3R 1A3 Canada</p>
				<p><a href="tel:19056695003">905-669-5003</a></p>
				<p><a href="mailto:info@wpdevelopment.ca" target="_blank">info@wpdevelopment.ca</a></p>
				<p><a href="https://www.wpdevelopment.ca/" target="_blank">WPDevelopment.ca</a></p>
				<p class="pt-5">This document was last updated on October 8, 2019</p>

		</div>
	</div>
	<div class="row">

<!--
		<div class="col-12 pb-5">
			<a href="/pdf/site-plan.pdf" target="_blank">
				<button class="btn mx-auto d-block">Download PDF</button>
			</a>
		</div>
-->
	</div>
</div>
<?php include('../includes/footer.php'); ?>
	<script>
		$('#nav-site-plan').addClass(' active');
	</script>
</body>
</html>
