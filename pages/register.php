<?php 	include('../includes/meta.php'); ?>
<title>Register | Valleyview in Bowmanville</title>
<style>
	.invalid input:required:invalid{border: 1px solid #CB0006!important;}
	.invalid select:required:invalid{border: 1px solid #CB0006!important;}
	.invalid input:required:valid{border: 1px solid #666666!important;}

</style>
<script>
  function onSubmit(token) {
	document.getElementById("registrationForm").submit()
  }

  function validate(event) {
    event.preventDefault();
    if(
			(!document.getElementById('first_name').value) ||
			(!document.getElementById('last_name').value) ||
			(!document.getElementById('email').value) ||
			(!document.getElementById('how_did_you_hear').value) ||
			(!document.getElementById('broker').value) ||
			(!document.getElementById('postal_zip').value) ||
			(!document.getElementById('phone').value)
	  ){
		alert("Please Check Required Fields *");
	  document.getElementById("registrationForm").classList.add('invalid');
		}else if(isNaN(document.getElementById('phone').value)){
			alert("Only numbers allowed in Telephone");
			document.getElementById("registrationForm").classList.add('invalid');
		}else if(document.getElementById('phone').value.toString().length < 10){
			alert("Minimum numbers for Telephone is 'Ten'");
			document.getElementById("registrationForm").classList.add('invalid');
		}
		else if (document.getElementById('email').value !== document.getElementById('confirm_email').value) {
			alert('Confirm Email Not Matching.');
			document.getElementById("registrationForm").classList.add('invalid');
  		}

	else {

      grecaptcha.execute();
    }
  }


  function onload() {
    var element = document.getElementById('btnSubmit');
    element.onclick = validate;
  }
</script>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>
<style>
	.txt-bold{
		font-family: 'AvantGarde_Bold';
		/* font-weight: 100; */
	}
	.color-light-blue{
		color: #2D7FC0;
	}
	.thur{
		font-size:30px;
		color: #004490;
		padding: 50px 50px 0 50px;
	}
	.vally{
		font-size: 22pt;
		color: #004490;
		padding: 0 50px 50px 50px;
	}
	ul.custom-list-img {
	list-style: none;
	color: #3C4C54;
	text-align: left;
	margin-top: 2rem;
	font-size: 22px;
	padding: 0 0 0 5px;;
	line-height: 3rem;
	}
	ul.custom-list-img li{
	display: inline-flex;
	width: 100%;
	}
	ul.custom-list-img li span{
	position: relative;
	left:10px;
	margin-right: 20px;
	}
	ul.custom-list-img li:before {
	content: '-';
	}
	ul.custom-list-img li.notick{
	color: black;
	}
	ul.custom-list-img li.notick:before {
	content: '';
	margin-left: 10px;
	}
</style>

</head>
<body id="register">
<?php include('../includes/navigation.php'); ?>

<!-- <div class="container mt-5">
	<div class="row no-gutters">
		<div class="col-lg-6" style="background:#E9F0F2;">
			<img src="images/register/couple.jpg" class="img-fluid w-100" alt="">
			<p class="thur">
				<span class="boldText">Thursday, May 23, 2019</span>
				<br>
				at 7:00pm
			</p>
			<p class="vally">
				Valleyview Bowmanville <br>
				2021 Green Road <br>
				Unit 104-105 <br>
				Bowmanville, ON. L1C 3K7 <br>
			</p>
		</div>
		<div class="col-lg-6 p-4">
			<img src="images/register/rbc.png" class="img-fluid" alt="">
			<ul class="custom-list-img">
                    <li><span>What type of Mortgage is Best for Me ?</span></li>
                    <li><span>Steps to being Pre-Approved</span></li>
                    <li><span>How much Down payment do I require?</span></li>
                    <li><span>Trends in Mortgage Rates</span></li>
                    <li><span>How much can I Afford?</span></li>
                    <li><span>Resale vs. New Construction</span></li>
                    <li><span>Current Real Estate Market</span></li>
                    <li><span>Primary Residence or Investment Property</span></li>
                    <li><span>Government Stress Test and How it will Affect me</span></li>
            </ul>
		</div>
	</div>
	<div class="row no-gutters">
		<div class="col-lg-6 text-center">
			<a href="mailto:dave.stone@rbc.com"><img src="images/register/rep.png" class="" alt=""></a>
		</div>
		<div class="col-lg-6 px-5 pt-5">
			<a href="https://thevalleyview.ca/"><img src="images/register/logo-valleyview.png" class="float-left" alt=""></a>
			<a href="https://www.cmhc-schl.gc.ca/" target="_blank"><img src="images/register/logo-cmhc.png" class="float-right" alt=""></a>
		</div>
	</div>
</div> -->

<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-md-6 mt-5">
			<!-- <img src="images/popup/popup.png" class="img-fluid mt-5" alt=""> -->
			<h1 class="" style="color:#666666">
				Sales Office Now Open
			</h1>

			<p style="font-size:25px;color:#666666">
				A collection of
				<br>
				37&#8217;, 40&#8217; & 48&#8217; Lots in Bowmanville


			</p>
			<p>
				<span class="txt-bold" style="color:#3C4C54;font">From the low $700s</span>
			</p>
			<ul style="font-size:14px;color:#3C4C54">
				<li>Enjoy the best of Town and Country</li>
				<li>Surrounded by 10 acres of green space</li>
				<li>Close to  401, 407 and future GO Station</li>
				<li>Just 30 minutes east of Toronto</li>
				<li>community by WP Development</li>
			</ul>
			<div class="d-inline-block mt-4	text-white" style="background:#809F3D;padding:1rem 4rem 1rem 4rem">

				<h4 class="text-uppercase">Register Today!</h4>

			</div>


		</div>
		<div class="col-xs-12 col-md-6 mt-4">
		<p class="text-center text-md-right"><small>Fields marked in <span class="text-green">GREEN *</span> are required.</small></p>
		<form action="https://thevalleyview.ca/subscription/valleyview/processor/process_register.php" method="post" id="registrationForm" class="">
			<div class="form-group">
				<label for="first_name"><span class="text-green">First Name*</span></label>
				<input type="text" class="form-control" id="first_name" name="first_name" required>
			</div>
			<div class="form-group">
				<label for="last_name"><span class="text-green">Last Name*</span></label>
				<input type="text" class="form-control" id="last_name" name="last_name" required>
			</div>
			<div class="form-group">
				<label for="email"><span class="text-green">Email Address*</span></label>
				<input type="email" class="form-control" id="email" name="email" required>
			</div>
			<div class="form-group">
				<label for="confirm_email"><span class="text-green">Confirm Email Address*</span></label>
				<input type="email" class="form-control" id="confirm_email" name="confirm_email" required>
			</div>
			<div class="form-group">
				<label for="how_did_you_hear"><span class="text-green">How Did You hear about Us?*</span></label>
				<select name="how_did_you_hear" id="how_did_you_hear" class="form-control" required>
					<option value="">Select one...</option>
					<option value="Agent/Friend Referral">Agent/Friend Referral</option>
					<option value="BuzzBuzz Homes">BuzzBuzz Homes</option>
					<option value="Facebook">Facebook</option>
					<option value="Flyers">Flyers</option>
					<option value="GO Station">GO Station</option>
					<option value="Google">Google</option>
					<option value="Instagram">Instagram</option>
					<option value="Radio">Radio</option>
					<option value="Street-Ad">Street-Ad</option>
					<option value="Text Message">Text Message</option>
					<option value="TV/Magazine">TV/Magazine</option>
				</select>
			</div>
			<div class="form-group">
				<label for="broker"><span class="text-green">Are you a Broker?*</span></label>
				<select id="broker" name="broker" required class="form-control">
					<option value="">Select one...</option>
					<option value="YES">YES</option>
					<option value="NO">NO</option>
				</select>
			</div>
			<div class="form-group">
				<label for="brokerage"><span class="">Brokerage</span></label>
				<input type="text" class="form-control" id="brokerage" name="brokerage">
			</div>
			<div class="form-group">
				<label for="postal_zip"><span class="text-green">Postal Code*</span></label>
				<input type="text" class="form-control" id="postal_zip" name="postal_zip" required>
			</div>
			<div class="form-group">
				<label for="phone"><span class="text-green">Telephone*</span></label>
				<input type="text" class="form-control" id="phone" name="phone" required>
			</div>

            <?php if (STATUS_LIVE == SITE_LIVE){ ?>
			<div id='recaptcha' class="g-recaptcha"
				data-sitekey="6Lc1FX4UAAAAAIKdM3F4fE7AEEYl02BQc3tUhXPM"
				data-callback="onSubmit"
				data-size="invisible">
			</div>
            <?php } ?>


			<button type="submit"  id="btnSubmit" class="btn text-right">Submit</button>

		</form>
		<div class="clearfix"></div>
		<p class="disclaimer">By submitting this form, you are agreeing to receive  communications from WP Development. We do not share information with third parties.</p>

		</div>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
    <script>
		$('#nav-register').addClass(' active');
	</script>
	<script>onload();</script>
</body>
</html>
