<?php 	include('../includes/meta.php'); ?>
<title>Site Plan | Valleyview in Bowmanville</title>
</head>
<body id="site-plan">
<?php include('../includes/navigation.php'); ?>
<div class="container-fluid p-0">
	<img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="Site Plan">
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-grey  pt-4 text-center text-sm-left mb-5">Come for the Selection…  Stay for the Elegance</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-12  mb-5 pb-2">
			<img src="<?= SITE_IMAGES ?>site-plan.jpg?ver=<?= date('j') ?>" class="img-fluid d-block mx-auto" alt="Site Plan, Valleyview in Bowmanville">
		</div>
<!--
		<div class="col-12 pb-5">
			<a href="/pdf/site-plan.pdf" target="_blank">
				<button class="btn mx-auto d-block">Download PDF</button>
			</a>
		</div>
-->
	</div>
</div>
<?php include('../includes/footer.php'); ?>
	<script>
		$('#nav-site-plan').addClass(' active');
	</script>
</body>
</html>