<?php
include('../includes/meta.php');
include('../includes/survey-info.php');
include('../includes/process-survey.php');
?>
<title>Survey | Valleyview in Bowmanville</title>
</head>

<body id="news">
  <?php include('../includes/navigation.php'); ?>

  <div class="container-fluid p-0">
    <img src="<?= SITE_IMAGES ?>hero/site-plan.jpg" class="img-fluid w-100 d-block" alt="Survey Page">
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="text-grey  pt-4 text-center text-sm-left mb-5">Survey</h1>
        <?php if (count($error_messages) > 0) { ?>
          <p class="alert alert-danger">The following issues were encountered when processing your submission. Please correct them and try again:<br>
            <?= implode("<br>", $error_messages); ?>
          </p>
        <?php } else if ($is_post && count($error_messages) == 0) { ?>

          <p class="alert alert-success">Thank you for taking the time to complete this survey. We appreciate and value your feedback!</p>
        <?php } ?>
        <p>At WP Development, we value the feedback of our prospective homeowners in order to provide the best living experience possible. We appreciate you taking the time to fill out the short survey below to help us understand your needs.</p>
      </div>
    </div>

    <form action="<?= SITE_URL ?>/survey.html" method="post" id="survey" class="ajax">
      <div class="row">
        <div class="col-lg-10 col-12 offset-lg-1 px-lg-1">
          <div id="message"></div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-6 col-12 pb-2 px-lg-1">
          <!--[if IE 9]><label for="first_name">*First Name:</label><![endif]-->
          <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name*" required="" autocomplete="given-name" value="<?= $first_name; ?>">
        </div>
        <div class="col-lg-5 col-md-6 col-12 pb-2 px-lg-1">
          <!--[if IE 9]><label for="first_name">*Last Name:</label><![endif]-->
          <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name*" required="" autocomplete="family-name" value="<?= $last_name; ?>">
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-6 col-12 pb-2 px-lg-1">
          <!--[if IE 9]><label for="first_name">*Email:</label><![endif]-->
          <input type="email" class="form-control" name="email" id="email" placeholder="Email*" required="" autocomplete="email" value="<?= $email; ?>">
        </div>
        <div class="col-lg-5 col-md-6 col-12 pb-2 px-lg-1">
          <!--[if IE 9]><label for="first_name">*Confirm Email:</label><![endif]-->
          <input type="email" class="form-control" name="cemail" id="cemail" placeholder="Confirm Email*" required="" autocomplete="email" value="<?= $cemail; ?>">
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-6 col-12 pb-2 px-lg-1">
          <!--[if IE 9]><label for="phone">*Phone Number:</label><![endif]-->
          <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone Number*" required="" autocomplete="tel" value="<?= $phone; ?>">
        </div>
        <div class="col-lg-5 col-md-6 col-12 pb-2 px-lg-1">
          <!--[if IE 9]><label for="first_name">*City:</label><![endif]-->
          <input type="text" class="form-control" name="city" id="city" placeholder="City*" required="" value="<?= $city; ?>">
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-6 col-12 pb-2 px-lg-1">
          <!--[if IE 9]><label for="province">*Province:</label><![endif]-->
          <select class="form-control" name="province" id="province" required="">
            <option value="">Province*</option>
            <?php foreach ($province_array as $key => $value) { ?>
              <option value="<?= $key; ?>" <?= $province == $value ? 'selected' : ''; ?>><?= $value; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-lg-5 col-md-6 col-12 pb-2 px-lg-1">
          <!--[if IE 9]><label for="first_name">*Postal:</label><![endif]-->
          <input type="text" class="form-control" name="postal" id="postal" placeholder="Postal Code*" maxlength="7" required="" value="<?= $postal; ?>">
        </div>
      </div>

      <div class="row justify-content-center py-4">
        <div class="col-lg-3 col-12 pb-2 px-lg-1">
          <p class="small font-weight-medium d-inline-block m-0">Are you looking for a new home?</p>
        </div>
        <div class="col-lg-7 col-12 px-lg-1">
          <div class="form-check form-check-inline d-md-inline d-block pr-4">
            <input class="form-check-input circle-input align-middle" type="radio" name="interested_purchaser" id="purchaser_yes" value="Yes" required <?= $interested_purchaser == "Yes" ? 'checked' : ''; ?>>
            <label class="form-check-label text-secondary text-uppercase font-weight-medium small" for="purchaser_yes">Yes</label>
          </div>
          <div class="form-check form-check-inline d-md-inline d-block pr-4">
            <input class="form-check-input circle-input align-middle" type="radio" name="interested_purchaser" id="purchaser_no" value="No" required <?= $interested_purchaser == "No" ? 'checked' : ''; ?>>
            <label class="form-check-label text-secondary text-uppercase font-weight-medium small" for="purchaser_no">No</label>
          </div>
        </div>
      </div>

      <div class="row justify-content-center py-4 div_price">
        <div class="col-lg-3 col-12 pb-2 px-lg-1">
          <p class="small font-weight-medium d-inline-block m-0">How long have you been looking for a new home?</p>
        </div>
        <div class="col-lg-7 col-12 px-lg-1">
          <select class="form-control" name="how_long" id="how_long">
            <option value="">Please select</option>
            <?php foreach ($how_long_looking_new_home_array as $key => $value) { ?>
              <option value="<?= $key; ?>" <?= $how_long == $value ? 'selected' : ''; ?>><?= $value; ?></option>
            <?php } ?>
          </select>
        </div>
      </div>

      <div class="row justify-content-center py-4 div_price">
        <div class="col-lg-3 col-12 pb-2 px-lg-1">
          <p class="small font-weight-medium d-inline-block m-0">What price range are you looking to spend?</p>
        </div>
        <div class="col-lg-7 col-12 px-lg-1">
          <select class="form-control" name="new_price" id="new_price">
            <option value="">Please select</option>
            <?php foreach ($new_price_array as $key => $value) { ?>
              <option value="<?= $key; ?>" <?= $new_price == $value ? 'selected' : ''; ?>><?= $value; ?></option>
            <?php } ?>
          </select>
        </div>
      </div>


      <div class="row justify-content-center py-4 div_price">
        <div class="col-lg-3 col-12 pb-2 px-lg-1">
          <p class="small font-weight-medium d-inline-block m-0">How did you hear about Valleyview Bowmanville?</p>
        </div>
        <div class="col-lg-7 col-12 px-lg-1">
          <select class="form-control" name="how_did_you_hear" id="how_did_you_hear">
            <option value="">Please select</option>
            <?php foreach ($how_did_you_hear_array as $key => $value) { ?>
              <option value="<?= $key; ?>" <?= $how_did_you_hear == $value ? 'selected' : ''; ?>><?= $value; ?></option>
            <?php } ?>
          </select>
        </div>
      </div>

        <div class="row justify-content-center py-4">
          <div class="col-lg-3 col-12 pb-2 px-lg-1">
            <p class="small font-weight-medium d-inline-block m-0">Did you know that the average home price (of homes sold) in Bowmanville has increased $30,000 since April 2019 (as of February 1, 2020)?</p>
          </div>
          <div class="col-lg-7 col-12 px-lg-1">
            <div class="form-check form-check-inline d-md-inline d-block pr-4">
              <input class="form-check-input circle-input align-middle" type="radio" name="did_you_know_average_price" id="did_you_know_average_price_yes" value="Yes" required <?= $did_you_know_average_price == "Yes" ? 'checked' : ''; ?>>
              <label class="form-check-label text-secondary text-uppercase font-weight-medium small" for="did_you_know_average_price_yes">Yes</label>
            </div>
            <div class="form-check form-check-inline d-md-inline d-block pr-4">
              <input class="form-check-input circle-input align-middle" type="radio" name="did_you_know_average_price" id="did_you_know_average_price_no" value="No" required <?= $did_you_know_average_price == "No" ? 'checked' : ''; ?>>
              <label class="form-check-label text-secondary text-uppercase font-weight-medium small" for="did_you_know_average_price_no">No</label>
            </div>
          </div>
        </div>

        <div class="row justify-content-center py-4">
          <div class="col-lg-3 col-12 pb-2 px-lg-1">
            <p class="small font-weight-medium d-inline-block m-0">Did you know that, as of December 2019, the average home price in Bowmanville/Clarington is approximately $60,000 lower than in Whitby?</p>
          </div>
          <div class="col-lg-7 col-12 px-lg-1">
            <div class="form-check form-check-inline d-md-inline d-block pr-4">
              <input class="form-check-input circle-input align-middle" type="radio" name="did_you_know_price_is_lower" id="did_you_know_two_yes" value="Yes" required <?= $did_you_know_price_is_lower == "Yes" ? 'checked' : ''; ?>>
              <label class="form-check-label text-secondary text-uppercase font-weight-medium small" for="did_you_know_two_yes">Yes</label>
            </div>
            <div class="form-check form-check-inline d-md-inline d-block pr-4">
              <input class="form-check-input circle-input align-middle" type="radio" name="did_you_know_price_is_lower" id="did_you_know_two_no" value="No" required <?= $did_you_know_price_is_lower == "No" ? 'checked' : ''; ?>>
              <label class="form-check-label text-secondary text-uppercase font-weight-medium small" for="did_you_know_two_no">No</label>
            </div>
          </div>
        </div>


        <div class="row justify-content-center py-4">
          <div class="col-lg-3 col-12 pb-2 px-lg-1">
            <p class="small font-weight-medium d-inline-block m-0">What is most appealing to you about a potential home purchase in Bowmanville?</p>
          </div>
          <div class="col-lg-7 col-12 px-lg-1">
            <select class="form-control" name="reason_most_appealing" id="reason_most_appealing" required>
              <option value="">Please select</option>
              <?php foreach ($what_is_most_appealing_array as $key => $value) { ?>
                <option value="<?= $key; ?>" <?= $reason_most_appealing == $value ? 'selected' : ''; ?>><?= $value; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>


        <div class="row justify-content-center py-4">
          <div class="col-lg-3 col-12 pb-2 px-lg-1">
            <p class="small font-weight-medium d-inline-block m-0">What are you looking for most from a Valleyview home?</p>
          </div>
          <div class="col-lg-7 col-12 px-lg-1">
            <select class="form-control" name="looking_for_most" id="looking_for_most" required>
              <option value="">Please select</option>
              <?php foreach ($what_are_you_looking_for_array as $key => $value) { ?>
                <option value="<?= $key; ?>" <?= $looking_for_most == $value ? 'selected' : ''; ?>><?= $value; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>


        <div class="row justify-content-center py-4">
          <div class="col-lg-3 col-12 pb-2 px-lg-1">
            <p class="small font-weight-medium d-inline-block m-0">As a thank you for completing this survey, would you like the chance to win a $100 VISA gift card?</p>
          </div>
          <div class="col-lg-7 col-12 px-lg-1">
            <div class="form-check form-check-inline d-md-inline d-block pr-4">
              <input class="form-check-input circle-input align-middle" type="radio" name="thank_you_visa" id="thank_you_visa_yes" required value="Yes" <?= $thank_you_visa == "Yes" ? 'checked' : ''; ?>>
              <label class="form-check-label text-secondary text-uppercase font-weight-medium small" for="thank_you_visa_yes">Yes</label>
            </div>
            <div class="form-check form-check-inline d-md-inline d-block pr-4">
              <input class="form-check-input circle-input align-middle" type="radio" name="thank_you_visa" id="thank_you_visa_no" required value="No" <?= $thank_you_visa == "No" ? 'checked' : ''; ?>>
              <label class="form-check-label text-secondary text-uppercase font-weight-medium small" for="thank_you_visa_no">No</label>
            </div>
          </div>
        </div>




        <div class="row justify-content-center pt-2 pb-4">
          <div class="col-lg-10 col-12 px-lg-1">
            <div class="form-check form-check-inline d-md-inline d-block">
              <input class="form-check-input circle-input align-top " type="checkbox" name="casl" id="casl" value="Yes" required <?= $casl == "Yes" ? 'checked' : ''; ?>>
              <label class="form-check-label font-weight-light small w-90" for="casl">In accordance with Canadian Anti-Spam Legislation, I have read WP Development's Privacy Policy and give my consent to receive electronic communications from WP Development's regarding; communities of potential interest, news, events, promotions and other related subjects.</label>
            </div>
          </div>
        </div>

        <div class="row justify-content-center pt-2 pb-4">
          <div class="col-lg-10 col-12 px-lg-1">
            <div class="form-group">
              <input type="submit" value="Submit" id="survey" class="btn">
            </div>
          </div>
        </div>

      </div>

    </form>


  </div>


  <?php include('../includes/footer.php'); ?>


  <script>
    $(document).ready(function() {
      var initial_purchase_option = $("input[name=interested_purchaser]:checked").val();

    });



    $("select[name=new_home_preference]").on('change', function() {
      var new_home_val = $(this).val();

      // console.log("HERE");
      // console.log(new_home_val);

      toggleYesOptions(new_home_val);
    });


    $("input[name=interested_purchaser]").on('change', function() {
      // console.log($(this).val());
      var current_option = $(this).val();
    });


    function toggleYesOptions(new_val){
      //hide the fields
      $(".div_size_lot").hide();
      $(".div_square_footage").hide();
      $(".div_number_of_bedrooms").hide();
      $(".div_price").hide();

      if (new_val == "Sq.ft") {
        $(".div_square_footage").show();
        $("select[name=square_footage]").attr("required", "required");
        $(".div-rest").show();
      } else if (new_val == "# of Bedrooms") {
        $(".div_number_of_bedrooms").show();
        $("select[name=number_of_bedrooms]").attr("required", "required");
        $(".div-rest").show();
      } else if (new_val == "Lot Size") {
        $(".div_size_lot").show();
        $("select[name=size_lot]").attr("required", "required");
        $(".div-rest").show();
      } else if (new_val == "Price") {
        $(".div_price").show();
        $("select[name=price]").attr("required", "required");
        $(".div-rest").show();
      }
    }
  </script>

</body>

</html>