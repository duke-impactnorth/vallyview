<?php include('../includes/meta.php'); ?>
	<title>Valleyview in Bowmanville</title>
</head>
<body>
<?php include('includes/navigation.php'); ?>
<div id="heroSlider" class="carousel slide carousel-fade" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item slide active" id="slide-1">
			<img src="/images/misc/blank.png" class="w-100" alt="">
		</div>
	</div>
</div>
<div class="container" id="bubble-container">
	<div class="row">
		<div class="col">
			<!-- <h1 class="py-4 mb-5 text-center" style="background: grey; color: white;">Grand Opening Saturday October 27th at 11:00 AM</h1> -->
			<h1 class="py-4 mb-5 text-center" style="background: grey; color: white;">NOW OPEN</h1>
		</div>
	</div>
	<div class="row pt-3">
		<div class="col-xs-12 col-sm-6">
			<img src="/images/bubble-1.png" id="bubble1" class="bubble img-fluid op-1" alt="">
<!--
			<img src="/images/bubble-2.png" id="bubble2" class="bubble img-fluid "  alt="">
			<img src="/images/bubble-3.png" id="bubble3" class="bubble img-fluid "  alt="">
-->
		</div>
		<div class="col-xs-12 col-sm-6">

			<h1 class="text-grey pr-5">Detached Homes in Bowmanville</h1>
			<p class="text-grey pb-5">14 acres of rolling hills, valleys and meadows. Country charm with city amenities. Steps from highways and future GO station. Moments to schools, parks, trails, shops and services. Scenic trails hugging the lakeshore beside lapping waves. A historic downtown with culture, fine dining, festivals and more. A beautiful, connected, affordable place to live, grow and raise a family. A place that feels like home. Welcome to Valleyview in Bowmanville. </p>
		</div>
	</div>
</div>
<div class="container-fluid bg-white pt-5" id="home-designs">
<div class="container">
	<div class="row ">
		<div class="col-xs-12 col-sm-12 text-center">
			<h2 class="text-grey light">Traditional & Transitional Home Designs</h2>
			<!-- <h4 class="pb-4 text-grey">on 33&prime;, 37&prime; & 40&prime; lots</h4> -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 col-sm-6">
			<img src="/images/renderings/traditional.jpg" class="img-fluid mx-auto d-block w-100" alt="Traditional & Transitional Home Designs">
		</div>
		<div class="col-xs-6 col-sm-6">
			<img src="/images/renderings/transitional.jpg" class="img-fluid mx-auto d-block w-100" alt="Traditional & Transitional Home Designs">
		</div>
		<div class="col-xs-12 col-sm-12  pt-4">
			<p>Every family is different. By starting from this basic premise, we design our homes to meet real needs of real people. Perhaps you seek inspiration from timeless values. Then our traditional 2-storey homes with heritage brick and stone exteriors are just right for you. Or you may have a taste for modern design, something like our transitional series, a blend of classic and modern styles. Our designs and floorplans are driven by the needs of today’s families. That’s the key difference we bring to our product, the prime reason you will always feel at home with us.</p>
		</div>
	</div>
</div>
</div>
	<?php include('includes/footer.php'); ?>
	<?php //include 'pages/popup.php'; ?>

	<script>

	$('.carousel').carousel({
  		interval: 6000
	});
	//	var totalItems = $('.carousel-item').length;
		var currentIndex = $('div.active').index() + 1;

	$('#heroSlider').on('slid.bs.carousel', function () {
		currentIndex = $('div.active').index() + 1;
		$('#bubble1').attr('src' , '/images/bubble-'+currentIndex+'.png')

	});

		setInterval(function(){
			$('.first-caption').toggle( "slow", function(){
				$('.first-caption').animate({
					opacity:.9
				}, 500);
			} );
			$('.second-caption').toggle( "slow" );
		}, 3000);

    $(window).on('load',function(){
       $('#exampleModalCenter').modal('show');

    });

		$('#closeBtn').click(function(){
			$('#exampleModalCenter').modal('hide');
		})

	</script>


</body>
</html>
