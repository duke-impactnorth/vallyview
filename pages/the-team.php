<?php 	include('../includes/meta.php'); ?>
<title>The Team | Valleyview in Bowmanville</title>
</head>
<body id="the-team">
<?php include('../includes/navigation.php'); ?>
<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-xs-12 col-sm-12 mt-4">
			<h2 class="text-grey">The Team</h2>
			<h4 class="text-grey">Come for the Quality…Stay for the Affordability!</h4>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<img src="<?= SITE_IMAGES ?>logos/logo_wp_developments.jpg" alt="WP Development Logo" class="img-the-team img-fluid mb-5 mt-4">
			<p><span class="boldText text-grey">The Developer</span><br /><span class="boldText text-grey">WP Development</span> is part of the Wealthpower Group of Companies established in 2008. Specializing in residential land development, we have built outstanding communities in strategic locations across the GTA including Stouffville, Oakville, Markham and Clarington, offering new home buyers a consistently high quality product designed and built to exceed industry standards.</p>
			<p>Our portfolio includes a broad range of premium detached homes, townhouse and luxury condos, all exhibiting WP’s  signature attention to detail. We love what we do, and we do it well, focusing on creating living spaces that are both functional and beautiful, appointed with all of the modern comforts and conveniences.</p>
			<p class="mb-5">We pride ourselves on our innovation, providing home buyers with an ever-widening choice of styles and constantly searching for the latest in features, finishes and ways to improve our service. Meticulous planning, superior designs, high quality craftsmanship and excellent value - all our hallmarks of WP Development.</p>

		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 mt-4">
			<img src="<?= SITE_IMAGES ?>logos/logo_ballantry.png" alt="Ballantry Homes Logo" class="img-fluid mb-5 mt-4">
			<p><span class="boldText text-grey">The Builder</span><br /><span class="boldText text-grey">Ballantry Homes</span>. Dreams don’t all disappear when you wake up in the morning. The best ones can be made real... if you put your mind to it. We know. At Ballantry Homes, we build dreams.</p>
			<p>With over three decades of experience, we’ve designed and constructed thousands of homes, working closely with our family of buyers and our team of forward-thinking architects and interior designers. Ballantry has always treated our customers as #1.</p>
			<p>Having built some of the finest homes and estate communities in and around the GTA, Ballantry’s reputation for meticulous attention to detail and craftsmanship has earned the trust of thousands of satisfied homeowners.</p>
			<p class="mb-5">Ballantry’s expertise and superior craftsmanship have been clearly demonstrated in countless communities across the GTA. From Oakville’s Oak Park, Upper Glen Abbey and Joshua Creek to Markham’s Cornell, Heritage Meadows Rouge River Estates. And from Brampton’s The Estates of Credit Ridge to Park Ridge Forest in Oshawa, with many more communities in between.</p>
		</div>
		<div class="col-xs-12 col-md-6">
			<img src="<?= SITE_IMAGES ?>team-wp.jpg" class="img-fluid mx-auto d-block mb-5" alt="The Team, WP Development">
		</div>
		<div class="col-xs-12 col-md-6">
			<img src="<?= SITE_IMAGES ?>team-ballantry.jpg" class="img-fluid mx-auto d-block mb-5" alt="The Team, Ballantry Homes">
		</div>
	</div>
</div>
<?php include('../includes/footer.php'); ?>
	<script>
		$('#nav-the-team').addClass(' active');
	</script>
</body>
</html>