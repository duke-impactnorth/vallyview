<?php
require_once("includes/hquery.php");
use duzun\hQuery;

// Set the cache path - must be a writable folder
// If not set, hQuery::fromURL() whould make a new request on each call
hQuery::$cache_path = "cache";

// Time to keed request data in cache, seconds
// A value of 0 disables cahce
hQuery::$cache_expires = 3600; // default one hour

if(isset($_POST) && isset($_POST['url'])){
    $url = $_POST['url'];
    // GET the document
    $doc = hQuery::fromUrl($url, ['Accept' => 'text/html,application/xhtml+xml;q=0.9,*/*;q=0.8']);
    $body_html = $doc->html('body');

    echo $body_html;
}
else{
    echo "Not valid entry";
}

?>
