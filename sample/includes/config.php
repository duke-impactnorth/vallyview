<?php

//testing server and production server info
if($_SERVER['HTTP_HOST'] == "new.tst"){
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => 'root',
			'password' => '',
			'database' => 'ig_post'
		),
		'Site' => array(
			'name' => 'Jax Condos',
			'url' => 'http://new.tst/prod/jax',
            'email' => '',
            'live' => 0
		)
    );
}elseif($_SERVER['HTTP_HOST'] == "impactnorth.com"){
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => 'master_usr',
			'password' => '%TVFXtT@dq3x',
			'database' => 'jaxcondos'
		),
		'Site' => array(
			'name' => 'Jax Condos',
			'url' => 'https://impactnorth.com/prod/jax',
            'email' => 'david@fredimedia.com',
            'live' => 1
		)
    );
}
else{
	$config = array(
		'Database' => array(
			'server' => 'localhost',
			'username' => 'master_usr',
			'password' => '%TVFXtT@dq3x',
			'database' => 'jaxcondos'
		),
		'Site' => array(
			'name' => 'Jax Condos',
			'url' => 'https://jaxcondos.ca',
            'email' => 'ecast@jaxcondos.ca',
            'live' => 2
		)
    );
}

include("database.php");