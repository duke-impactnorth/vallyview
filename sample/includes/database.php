<?php
$db = mysqli_connect($config['Database']['server'], $config['Database']['username'], $config['Database']['password'], $config['Database']['database']) or die();


function instagram_data_insert($query){
    global $db;

    $stmt = $db->prepare($query);
    if(!$stmt->execute()) return false;
    //use $stmt->affected_rows to get # of rows changed

    $stmt->close();
    return true;
}


/*
function abc_select($options_array = array()){
    global $db;

    $query = "SELECT * FROM MyGuest";
    $stmt = $db->prepare($query);

    $stmt->execute();

    $results_array = retrieve_full_array($stmt->get_result());
    $stmt->free_result();
    $stmt->close();

    return $results_array;
}

function retrieve_full_array($result, $options_array = array()){
    $prepared_array = array();
    while($data = $result->fetch_assoc()){
        $prepared_array[] = $data;
    }

    return $prepared_array;
}*/