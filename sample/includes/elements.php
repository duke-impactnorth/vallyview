<?php 
if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/kint/kint.php")) include ($_SERVER["DOCUMENT_ROOT"] . "/kint/kint.php");
include ("config.php");
include ("functions.php");
include ("hquery.php");
?>

<?php function html_header($title, $options_array = array()){ ?>
<!DOCTYPE html>
<html lang="en">
	<head>
        <?php header_element($title); ?>
	</head>
	<body>
<?php } ?>

<?php function html_footer($options_array = array()){ ?>
        <?php script_element(); ?>            
<?php } ?>

<?php function html_closedocument($options_array = array()){ ?>
    </body>
</html>
<?php } ?>

<?php function header_element($title, $options_array = array()){ ?>
    <!-- TITLE -->    
    <title><?php echo empty($title) ? "" : $title . " | "; ?>Template Site</title>

    <!-- META TAGS -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <!-- FAVICON -->
    

    <!-- CSS -->  
    <link rel="stylesheet" href="css/main.css">    
<?php } ?>

<?php function navbar_element($options_array = array()){ ?>
  
<?php } ?>


<?php function footer_element($options_array = array()){ ?>

<?php } ?>

<?php function script_element($options_array = array()){ ?>
    <!-- JS Files -->
    <script src="js/main.js" defer></script>            
<?php }