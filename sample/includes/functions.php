<?php 

function general_splitAndUppercase($word, $delimiter = "_"){
	$result = implode(' ', array_map(function($a){return ucfirst($a);}, explode($delimiter,$word)));
	return $result; 
}


function general_getCurrentDatetime(){
  date_default_timezone_set('America/Toronto');
  $date_updated = New Datetime();
  $date_string = $date_updated->format("Y-m-d H:i:s");

  return $date_string;  
}

function general_getCurrentDate(){
  date_default_timezone_set('America/Toronto');
  $date_updated = New Datetime();
  $date_string = $date_updated->format("Y-m-d");

  return $date_string;  
}

function general_mysqlDatetime($date_string){
  $date_format = New Datetime($date_string);
  return $date_format->format("Y-m-d H:i:s");
}


function general_validateDatetime($date)
{
  $d = DateTime::createFromFormat('Y-m-d H:i:s', $date);
  return $d && $d->format('Y-m-d H:i:s') == $date;
}

function general_validateDate($date)
{
  $d = DateTime::createFromFormat('Y-m-d', $date);
  return $d && $d->format('Y-m-d') == $date;
}

function general_getNiceDateNoDay($date_string){
  if(strpos($date_string, "0000") !== FALSE){
	  return $date_string;
  }		
  $nice_date = new Datetime($date_string);
  return $nice_date->format("F Y");
}

function general_getNiceDatetime($date_string){
  if(strpos($date_string, "0000") !== FALSE){
	  return $date_string;
  }		
  $nice_date = new Datetime($date_string);
  return $nice_date->format("Y-m-d h:i a");
}

function general_getNiceDateWithWords($date_string){
  if(strpos($date_string, "0000") !== FALSE){
	  return $date_string;
  }		
  $nice_date = new Datetime($date_string);
  return $nice_date->format("F d, Y");
}

function general_getNiceDatetimeWithWords($date_string){
  if(strpos($date_string, "0000") !== FALSE){
	  return $date_string;
  }	
  $nice_date = new Datetime($date_string);
  return $nice_date->format("M d, Y, h:i a");
}