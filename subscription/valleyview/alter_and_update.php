<?php

require_once('includes/initialize_subscription.php');

// TO DO: MOVDE TABLE NAME TO INCUDE
$database_table_name = CURRENT_WEBSITE_PROJECT_DATABASE_TABLE_NAME;
$database_project_directory = SITE_PROJECT_DIRECTORY;

?>


<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Alter and Update</title>
</head>

<body>

<?php

$subscribeUnsubscribe = new BASSubscribeUnsubscribe();

$subscribeUnsubscribe->setDatabaseConnection($connection);
$subscribeUnsubscribe->setTableName($database_table_name);
$subscribeUnsubscribe->setProjectDirectory($database_project_directory);

/*
$alterQuery = "ALTER TABLE `" . $tableToBeAltered . "`  
				ADD `ip_address` VARCHAR(20) NOT NULL ,  
				ADD `confirm_subscription` TINYINT NOT NULL DEFAULT '0' ,  
				ADD `confirm_subscription_date` TIMESTAMP NOT NULL ,  
				ADD `unsubscribed` TINYINT NULL DEFAULT '0' ,  
				ADD `unsubscribe_url` VARCHAR(255) NOT NULL ,  
				ADD `subscribe_url` VARCHAR(255) NOT NULL ;";
*/				
						



if($subscribeUnsubscribe->checkIfTalbeExsists()){
	
	$backupTable = $subscribeUnsubscribe->backupTable();
	
	if($backupTable){
		
		echo '<h1 style="color:#0A7C2A;">Table backuped as ' . $backupTable . '</h1>';
		// this to run first time 
		if($subscribeUnsubscribe->addNewFieldsToTable()){
			echo '<h1 style="color:#0A7C2A;">Table alterd now attemting to update fields</h1>';
			$subscribeUnsubscribe->updateAllRowsURLs();
			echo '<h1 style="color:#ff0000;">Process completed now remove this file from the server</h1>';
		}else{
			echo '<h1 style="color:#ff0000;">ERROR</h1>';
			echo '<span style="color:#ff0000;"></span>Not able to alter the table.<br>Table may be alerady altered or check current table structure and alter table query used in class_subscribe_unsubscribe<br>';
		}
		
		// remove records with duplicate emails
		if($subscribeUnsubscribe->deleteEmailDuplicates("community")){
			echo '<h1 style="color:#0A7C2A;">Duplicate removed</h1>';
		}
		
	}else{
		echo '<h1 style="color:#ff0000;">Could not backup table "' . $database_table_name . '" contact site admin.</h1>';
	}
	
}else{
	echo '<h1 style="color:#ff0000;">Table "' . $database_table_name . '" does not exsits in the database</h1>';
}

mysql_close($connection);

?>

</body>
</html>