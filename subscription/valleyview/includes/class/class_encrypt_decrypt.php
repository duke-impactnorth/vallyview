<?php



class BASEncryptDecrypt {

	// do not change encryption key
	private $encryption_key = "ryan-design-inc";
 	
	public function decrypt($string)
	{
		$encryption_key = $this->encryption_key;
		
		assert(isset($string) === TRUE);
		assert(isset($encryption_key) === TRUE);
	
		$result = '';
		$string = base64_decode($string);
	
		for ($i = 0; $i < strlen($string); $i++)
		{
			$char    = substr($string, $i, 1);
			$keychar = substr($encryption_key, ($i % strlen($encryption_key)) - 1, 1);
			$char    = chr(ord($char) - ord($keychar));
			$result .= $char;
		}
	
		return $result;
	}
	
	
	public function encrypt($string)
	{
		$encryption_key = $this->encryption_key;
		
		assert(isset($string) === TRUE);
		assert(isset($encryption_key) === TRUE);
	
		$string = (string) $string;
		$result = '';
	
		for ($i = 0; $i < strlen($string); $i++)
		{
			$char    = substr($string, $i, 1);
			$keychar = substr($encryption_key, ($i % strlen($encryption_key)) - 1, 1);
			$char    = chr(ord($char) + ord($keychar));
			$result .= $char;
		}
	
		return base64_encode($result);
	
	
	}
	
	public function decryptForURL($string)
	{
		
		return (urldecode($this->decrypt($string)));
	}

	
	public function encryptForURL($string)
	{
		  
		return urlencode($this->encrypt($string));
	
	}


	
}

?>
