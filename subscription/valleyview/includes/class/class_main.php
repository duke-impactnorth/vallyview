<?php


/*

*/

class BASMain {

	private $projectName;
	private $projectEmailAddressFrom;
	private $projectEmailAddressReplyTo;

	// database connection and table
	private $connection;
	private $tableName;

	// form data array
	private $submittedFormDataArray;

	// user data
	private $email;
	private $fName;
	private $lName;
	private $currentRegistrantId;

	// admin data
	private $adminEmailAddressesArray;
	private $adminNameArray;

	// auto responder
	private $autoResponderTemplateLocation;
	private $autoResponderSubjectLine;

	// BASSubscribeUnsubscribe
	private $subscribeUnsubscribe; //
	private $subscribeURL;
	private $unSubscribeURL;


	function __construct() {

	}

	// setters

	// current project

	public function setProjectName($pass_projectName){
		$this->projectName = $pass_projectName;
	}

	public function setProjectEmailAddressFrom($pass_projectEmailAddressFrom){
		$this->projectEmailAddressFrom = $pass_projectEmailAddressFrom;
	}

	public function setProjectEmailAddressReplyTo($pass_setProjectEmailAddressReplyTo){
		$this->projectEmailAddressReplyTo = $pass_setProjectEmailAddressReplyTo;
	}

	// database connection and table
	public function setDatabaseConnection($pass_connection){
		$this->connection = $pass_connection;
	}

	public function setTableName($pass_table_name){
		$this->tableName = $pass_table_name;
	}

	// form data array
	public function setSubmittedFormDataArray($pass_submittedFormDataArray){
		$this->submittedFormDataArray = $pass_submittedFormDataArray;
	}

	// user data
	public function setEmail($pass_email){
		$this->email = $pass_email;
	}

	public function setFirstName($pass_fName){
		$this->fName = $pass_fName;
	}

	public function setLastName($pass_lName){
		$this->lName = $pass_lName;
	}

	// admin data
	public function setAdminEmailAddress($pass_adminEmailAddress, $pass_adminName){
		$this->adminEmailAddressesArray[] = $pass_adminEmailAddress;
		$this->adminNameArray[] = $pass_adminName;

	}


	// auto responder
	public function setAutoResponderTemplateLocation($pass_autoResponderTemplateLocation){
		$this->autoResponderTemplateLocation = $pass_autoResponderTemplateLocation;
	}

	public function setAutoResponderSubjectLine($pass_autoResponderSubjectLine){
		$this->autoResponderSubjectLine = $pass_autoResponderSubjectLine;
	}


	public function executeRegistration(){

		if(strlen(SKIP_OPT_IN_TEMPLATE_SUBJECT_LINE) > 5){
			$this->executeSkipOptIn();
		}else{
			$this->executeOptIn();
		}


	}

	private function executeSkipOptIn(){
		$checkQuery = "SELECT * FROM " . $this->tableName . " WHERE email = '" . $this->email . "' LIMIT 1;";
		$checkQueryResult = mysql_query($checkQuery);

		$num_rows = mysql_num_rows($checkQueryResult);

		if($num_rows == 1){
			// send just auto respondse
			$this->sendSkipOptInEmailer();

		}else{
			// send just auto respondse
			// add recored to database
			$this->sendSkipOptInEmailer();
			$this->currentRegistrantId = $this->insertNewRecoredIntoDatabase();

		}

		$this->sendAdminNotificationEmailer();
	}

	private function executeOptIn(){
		$checkQuery = "SELECT * FROM " . $this->tableName . " WHERE email = '" . $this->email . "' LIMIT 1;";
		$checkQueryResult = mysql_query($checkQuery);

		$num_rows = mysql_num_rows($checkQueryResult);

		if($num_rows == 1){
			// email already exsisted in our databse
			// echo("<h1>Already in system</h1>");
			$this->executeExistingRegistration($checkQueryResult);

		}else{
			// echo("<h1>New registratn</h1>");
			$this->executeNewRegistration();

		}

	}



	private function executeExistingRegistration($passQueryResult){
		// echo "<h2>executeExistingRegistration()</h2>";

		$row = mysql_fetch_assoc($passQueryResult);

		$this->currentRegistrantId = $row["id"];

		$this->initSubscribeUnsubscribe();

		if($row["confirm_subscription"] == 1 && $row["unsubscribed"] != 1){
			// registrant is alredy confirmed
			// send defualt auto resoponder if there is any
			// echo("<h1>send defualt auto resoponder if any</h1>");

			$this->sendDefaultAutoresponderEmailer();
		}else{
			// registrant not a confirm subscriber send subscition email again
			// echo("<h1>exsisting Registrant no confirm_subscription</h1>");
			$this->sendConfirmSubscriptionEmailer();
			$this->sendAdminNotificationEmailer();

		}



	}

	private function executeNewRegistration(){
		// echo "<h2>executeNewRegistration()</h2>";
		$this->currentRegistrantId = $this->insertNewRecoredIntoDatabase();

		$this->initSubscribeUnsubscribe();

		$this->sendConfirmSubscriptionEmailer();
		$this->sendAdminNotificationEmailer();
	}

	// innsert new row to databse and return newly inserted registrant id
	private function insertNewRecoredIntoDatabase(){
		// echo "<h2>insertNewRecoredIntoDatabase()</h2>";
		// inster new recored
		// Construct query string

		// echo "" . count($this->submittedFormDataArray) . "<br>";

		$allArray = $this->submittedFormDataArray;
		$max = count($allArray);

		$query = "insert into " . $this->tableName . "(";

		for($i = 0; $i < $max; $i++){
			$query .= $allArray[$i]["field"] . ", ";
		}

		$query = substr($query, 0, -2) . ") values('";

		for($i = 0; $i < $max; $i++){
			$query .= $allArray[$i]["data"] . "', '";
		}

		$query = substr($query, 0, -3) . ")";

		// echo ">> Query :: " . $query . "<br>";;

		mysql_query($query)  or die(mysql_error());

		// newly inserted row id
		return mysql_insert_id();

	}

	//-------------------------------------------------------------------------------------------------------------------------
	// SubscribeUnsubscribe
	//-------------------------------------------------------------------------------------------------------------------------

	// create subscribe and unsubscribe URLs
	private function initSubscribeUnsubscribe(){
		$this->subscribeUnsubscribe = new BASSubscribeUnsubscribe();

		$this->subscribeUnsubscribe->setDatabaseConnection($this->connection);
		$this->subscribeUnsubscribe->setId($this->currentRegistrantId);
		$this->subscribeUnsubscribe->setEmail($this->email);
		$this->subscribeUnsubscribe->setTableName($this->tableName);
		$this->subscribeUnsubscribe->setProjectDirectory(SITE_PROJECT_DIRECTORY);

		// update row with unsubscribe_url & subscribe_url
		$this->subscribeUnsubscribe->updateDatabaseURLs();

		$this->subscribeURL = $this->subscribeUnsubscribe->getSubscribeURL();
		$this->unSubscribeURL = $this->subscribeUnsubscribe->getUnSubscribeURL();
	}

	//-------------------------------------------------------------------------------------------------------------------------
	// Autoresponders
	//-------------------------------------------------------------------------------------------------------------------------



	public function sendAutoResponseAfterSubscribeConfirmed($pass_id, $pass_email, $pass_body, $pass_subject){
		// get email , first and last name

		$this->email = $pass_email;

		$checkQuery = "SELECT * FROM " . $this->tableName . " WHERE email = '" . $this->email . "' AND id = $pass_id LIMIT 1;";
		$checkQueryResult = mysql_query($checkQuery);

		$num_rows = mysql_num_rows($checkQueryResult);

		if($num_rows == 1){
			$row = mysql_fetch_assoc($checkQueryResult);

			$this->currentRegistrantId = $row["id"];

			$this->subscribeURL = $row["subscribe_url"];
			$this->unSubscribeURL = $row["unsubscribe_url"];
			$this->fName = $row["f_name"];
			$this->lName = $row["l_name"];

			$this->initSubscribeUnsubscribe();

			if($row["confirm_subscription"] == 1 && $row["unsubscribed"] != 1){
				// registrant is alredy confirmed
				// send defualt auto resoponder if there is any
				// echo("<h1>send defualt auto resoponder if any</h1>");

				$this->sendDefaultAutoresponderEmailerByBody($pass_body, $pass_subject);
			}


		}




	}

	public function sendAutoResponseNewsletterAfterSubscribeConfirmed($pass_id, $pass_email){
		// get email , first and last name

		$this->email = $pass_email;

		$checkQuery = "SELECT * FROM " . $this->tableName . " WHERE email = '" . $this->email . "' AND id = $pass_id LIMIT 1;";
		$checkQueryResult = mysql_query($checkQuery);

		$num_rows = mysql_num_rows($checkQueryResult);

		if($num_rows == 1){
			$row = mysql_fetch_assoc($checkQueryResult);

			$this->currentRegistrantId = $row["id"];

			$this->subscribeURL = $row["subscribe_url"];
			$this->unSubscribeURL = $row["unsubscribe_url"];
			$this->fName = $row["f_name"];
			$this->lName = $row["l_name"];

			$this->initSubscribeUnsubscribe();

			if($row["confirm_subscription"] == 1 && $row["unsubscribed"] != 1){
				// registrant is alredy confirmed
				// send defualt auto resoponder if there is any
				// echo("<h1>send defualt auto resoponder if any</h1>");

				$this->sendDefaultAutoresponderEmailer();
			}


		}




	}

	public function sendCustomAutoResponseNewsletterAfterSubscribeConfirmed($pass_id, $pass_email){
		// get email , first and last name

		$this->email = $pass_email;

		$checkQuery = "SELECT * FROM " . $this->tableName . " WHERE email = '" . $this->email . "' AND id = $pass_id LIMIT 1;";
		$checkQueryResult = mysql_query($checkQuery);

		$num_rows = mysql_num_rows($checkQueryResult);

		if($num_rows == 1){
			$row = mysql_fetch_assoc($checkQueryResult);

			$this->currentRegistrantId = $row["id"];

			$this->subscribeURL = $row["subscribe_url"];
			$this->unSubscribeURL = $row["unsubscribe_url"];
			$this->fName = $row["first_name"];
			$this->lName = $row["last_name"];

			$this->initSubscribeUnsubscribe();

			$this->sendCustomDefaultAutoresponderEmailer();


		}




	}

	// email default auto responder to registrant
	private function sendCustomDefaultAutoresponderEmailer(){
		// echo "<h2>sendDefaultAutoresponderEmailer()</h2>";

		if(isset($this->autoResponderTemplateLocation)){
			// echo "<h2>:::: send email</h2>";

			$emailbodyText = BASUtility::getContentsOfFile($this->autoResponderTemplateLocation);

			$original = array("{template_first_name}", "{template_last_name}", "{subscribe_url}", "{un_subscribe_url}", "{contact_email}", "{site_name}", "{site_url}", "{user_email}", "{project_dir}", "{url_templates}");
			$replace = array($this->fName, $this->lName, $this->subscribeURL ,  $this->unSubscribeURL, $this->projectEmailAddressReplyTo, $this->projectName, BASUtility::currentURL(), $this->email, SITE_PROJECT_DIRECTORY, URL_TO_TEMPLATES_FOLDER);

			$emailbodyText = str_replace($original,$replace, $emailbodyText);
			$email_subject = $this->autoResponderSubjectLine;

			$sendNotifications = new BASSendNotifications();

			$sendNotifications->sendEmailToUser($this->email, $this->fName . " " . $this->lName, $emailbodyText, "", $email_subject);

		}
	}

	// email default auto responder to registrant
	private function sendDefaultAutoresponderEmailer(){
		// echo "<h2>sendDefaultAutoresponderEmailer()</h2>";

		if(isset($this->autoResponderTemplateLocation)){
			// echo "<h2>:::: send email</h2>";

			$emailbodyText = BASUtility::getContentsOfFile(SUBSCRIPTION_FOLDER_ROOT . $this->autoResponderTemplateLocation);

			$original = array("{template_first_name}", "{template_last_name}", "{subscribe_url}", "{un_subscribe_url}", "{contact_email}", "{site_name}", "{site_url}", "{user_email}", "{project_dir}", "{url_templates}");
			$replace = array($this->fName, $this->lName, $this->subscribeURL ,  $this->unSubscribeURL, $this->projectEmailAddressReplyTo, $this->projectName, BASUtility::currentURL(), $this->email, SITE_PROJECT_DIRECTORY, URL_TO_TEMPLATES_FOLDER);

			$emailbodyText = str_replace($original,$replace, $emailbodyText);
			$email_subject = $this->autoResponderSubjectLine;

			$sendNotifications = new BASSendNotifications();

			$sendNotifications->sendEmailToUser($this->email, $this->fName . " " . $this->lName, $emailbodyText, "", $email_subject);

		}
	}


	// email default auto responder to registrant
	private function sendDefaultAutoresponderEmailerByBody($pass_body, $pass_subject){

		$emailbodyText = str_replace($original,$replace, $emailbodyText);
		$email_subject = $this->autoResponderSubjectLine;

		$sendNotifications = new BASSendNotifications();

		$sendNotifications->sendEmailToUser($this->email, $this->fName . " " . $this->lName, $pass_body, "", $pass_subject);

	}

	// email confirm subscripton emailer
	private function sendConfirmSubscriptionEmailer(){
		// echo "<h2>sendConfirmSubscriptionEmailer()</h2>";

		$emailbodyText = BASUtility::getContentsOfFile(CURRENT_WEBSITE_TEMPLATE_CONFIRM_SUBSCRIPTION_EMAIL);

		$original = array("{subscribe_url}", "{contact_email}", "{site_name}", "{site_url}", "{corporate_address}", "{project_dir}", "{url_templates}");
		$replace = array($this->subscribeURL ,  $this->projectEmailAddressReplyTo, $this->projectName, BASUtility::currentURL(), CURRENT_WEBSITE_PROJECT_CORPORATE_ADDRESS, SITE_PROJECT_DIRECTORY, URL_TO_TEMPLATES_FOLDER);

		$emailbodyText = str_replace($original,$replace, $emailbodyText);
		$email_subject = CURRENT_WEBSITE_TEMPLATE_CONFIRM_SUBSCRIPTION_EMAIL_SUBJECT;

		$sendNotifications = new BASSendNotifications();

		$sendNotifications->sendEmailToUser($this->email, $this->fName . " " . $this->lName, $emailbodyText, "", $email_subject);


	}

	// email notification to admin(s)
	private function sendAdminNotificationEmailer(){
		// echo "<h2>sendAdminNotificationEmailer()</h2>";

		// create body
		$message = $this->getAdminMessageBody();
		$email_subject = "E-Notification: New Registration at ". $this->projectName;

		$sendNotifications = new BASSendNotifications();

		$email_alt_body = "";

        //do not send notification
		if(count($this->adminEmailAddressesArray) > 0){
			$sendNotifications->sendEmailToUser($this->adminEmailAddressesArray, $this->adminNameArray, $message, $email_alt_body, $email_subject);
		}

	}

	private function getAdminMessageBody(){
		$allArray = $this->submittedFormDataArray;
		$max = count($allArray);

		$time = date("M d Y",time());

		$fd = fopen(CURRENT_WEBSITE_TEMPLATE_ADMIN_NOTIFICATION_EMAIL,"r");

		$MESSAGE = fread($fd, filesize(CURRENT_WEBSITE_TEMPLATE_ADMIN_NOTIFICATION_EMAIL));

		$fields = "<tr><td align='right' class='field'>Date Received:</td>\n";
		$fields .= "<td align='left' class='value'>" . $time . "</td></tr>\n";

		for($i = 0; $i < $max; $i++){
			$fields .= "<tr><td align='right' class='field'>" .$allArray[$i]["title"] . ": </td>\n";
			$fields .= "<td align='left' class='value'>" .$allArray[$i]["data"] . "</td></tr>\n";
		}

		$original = array("{site_name}", "{tbody}");
		$replace = array($site,  $fields);

		$MESSAGE =  str_replace($original,$replace, $MESSAGE);

		fclose($fd);
		return $MESSAGE;

	}

	//-------------------------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------------------------
	// SkitpOptin Autoresponders
	//-------------------------------------------------------------------------------------------------------------------------
	private function sendSkipOptInEmailer(){
		//echo "<h2>sendSkipOptInEmailer() " . SKIP_OPT_IN_TEMPLATE_LOCATION . "</h2>";

		$emailbodyText = BASUtility::getContentsOfFile(SKIP_OPT_IN_TEMPLATE_LOCATION);

		$original = array("{template_first_name}", "{template_last_name}", "{subscribe_url}", "{contact_email}", "{site_name}", "{site_url}", "{corporate_address}", "{project_dir}", "{url_templates}");
		$replace = array($this->fName, $this->lName, $this->subscribeURL ,  $this->projectEmailAddressReplyTo, $this->projectName, BASUtility::currentURL(), CURRENT_WEBSITE_PROJECT_CORPORATE_ADDRESS, SITE_PROJECT_DIRECTORY, URL_TO_TEMPLATES_FOLDER);

		$emailbodyText = str_replace($original,$replace, $emailbodyText);
		$email_subject = SKIP_OPT_IN_TEMPLATE_SUBJECT_LINE;

		$sendNotifications = new BASSendNotifications();

		$sendNotifications->sendEmailToUser($this->email, $this->fName . " " . $this->lName, $emailbodyText, "", $email_subject);

	}


}


?>
