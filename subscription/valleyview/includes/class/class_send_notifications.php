<?php


/*
This class send notifcations to registrants and admins

*/

class BASSendNotifications {


	/**
	
	$email_to pass as string or array. if only one email address then send as string. if mulitple recipient then send as array
	$full_name pass as string or array. if only one email address then send full name as string. if mulitple recipient then send full name as array
	
	*/
	public function sendEmailToUser($email_to, $full_name, $email_body, $email_alt_body, $email_subject){
	
		if(ini_get('safe_mode')){
			// safe mode on
		    $this->sendEmailToUserVIASimpleMail($email_to, $full_name, $email_body, $email_alt_body, $email_subject);
		}else{
			// safe mode off
		    $this->sendEmailToUserVIAPHPMailer($email_to, $full_name, $email_body, $email_alt_body, $email_subject);
		} // end of if(ini_get('safe_mode'))


	}
	
	
	public function sendEmailToUserVIAPHPMailer($email_to, $full_name, $email_body, $email_alt_body, $email_subject){

		
		// echo "**sendEmailToUser<br>";
		$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
			
		if(CURRENT_WEBSITE_MAIL_SMTP_AUTH){	
		 	$mail->IsSMTP();
		}
			
		try {
			
			// if SMTP AUTHORIZAITON required
			if(CURRENT_WEBSITE_MAIL_SMTP_AUTH){
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->Username   = CURRENT_WEBSITE_MAIL_SMTP_USERNAME;
				$mail->Password   = CURRENT_WEBSITE_MAIL_SMTP_PASSWORD;
				$mail->Port       = CURRENT_WEBSITE_MAIL_SMTP_PORT; 
				$mail->Host       = CURRENT_WEBSITE_MAIL_SMTP_HOST; // SMTP server  				
			}
			
			if(is_array($email_to)){
				$tatalAddresses = count($email_to);
				
				for($i = 0; $i < $tatalAddresses; $i++){
					$mail->AddAddress($email_to[$i], $full_name[$i]);
				}				
			}else{
				$mail->AddAddress($email_to, $full_name);
			}		
					
			$mail->SetFrom(CURRENT_WEBSITE_MAIL_FROM, CURRENT_WEBSITE_MAIL_ACCOUNT_NAME);
			$mail->AddReplyTo(CURRENT_WEBSITE_MAIL_REPLY_TO, CURRENT_WEBSITE_MAIL_REPLY_TO_ACCOUNT_NAME);
			
			$mail->Subject = $email_subject;
			
			$mail->MsgHTML($email_body);
			$mail->AltBody = $email_alt_body;
			$mail->Send();
		
		  //echo "Message Sent OK<p></p>\n";
		
		} catch (phpmailerException $e) {
		  echo "**<br>";
		  echo $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
		  echo "****<br>";
		  echo $e->getMessage(); //Boring error messages from anything else!
		}	
			
		
	}
	
	
	public function sendEmailToUserVIASimpleMail($email_to, $full_name, $email_body, $email_alt_body, $email_subject){
		
		$HEADER = 'MIME-Version: 1.0' . "\r\n";
		$HEADER .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$HEADER .= "From: " . CURRENT_WEBSITE_MAIL_FROM;		
		$emailAddress = "";
		
		if(is_array($email_to)){
			$tatalAddresses = count($email_to);
			
			$countArray = count($email_to);
			
			for($i = 0; $i < $tatalAddresses; $i++){
				
				$emailAddress = $emailAddress . $email_to[$i] . ", ";
			}		
			
			$emailAddress = substr($emailAddress, 0 , (strlen($emailAddress) - 2));
					
		}else{
			$emailAddress = $email_to;
		}	
		
		//echo "<h1>~~~~ " .  $emailAddress . "</h1>";

		if(!mail($emailAddress,$email_subject,$email_body,$HEADER)){
			echo "** email failed";
		}
		
	
		
	}
		
}



?>
