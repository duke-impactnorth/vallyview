<?php

/*
Add following fields to desired table

ALTER TABLE `table_name`  
	ADD `ip_address` VARCHAR(20) NOT NULL ,  
	ADD `confirm_subscription` TINYINT NOT NULL DEFAULT '0' ,  
	ADD `confirm_subscription_date` TIMESTAMP NOT NULL ,  
	ADD `unsubscribed` TINYINT NULL DEFAULT NULL ,  
	ADD `unsubscribe_url` VARCHAR(255) NOT NULL ,  
	ADD `subscribe_url` VARCHAR(255) NOT NULL ;

*/

class BASSubscribeUnsubscribe {

	private $connection;
	
	private $id;
	private $email;
	private $tableName;
	private $projectDirectory;

	private $bas_encrypt_decrypt;

	function __construct() {
		$this->bas_encrypt_decrypt = new BASEncryptDecrypt();
	}


	public function setId($pass_id){
		$this->id = $pass_id;
	}
	
	public function setEmail($pass_email){
		$this->email = $pass_email;
	}
	
	public function setTableName($pass_table_name){
		$this->tableName = $pass_table_name;
	}
	
	public function setProjectDirectory($pass_project_directory){
		$this->projectDirectory = $pass_project_directory;
	}	

	public function setDatabaseConnection($pass_connection){
		$this->connection = $pass_connection;
	}	
	

	public function checkIfTalbeExsists(){
		
		$currenQuery = 'SELECT 1 FROM ' . $this->tableName . ';';
				
		$val = mysql_query($currenQuery);
		
		if($val !== FALSE)
		{
		   return true;
		}
		
		return false;

	}

	
	public function checkIfRegistratnExsists(){

		$checkQuery = "SELECT id, email FROM `" . $this->tableName . "`
						WHERE id = " . $this->id . " 
						AND email = '" . $this->email . "' LIMIT 1;";	
		
		//echo $checkQuery;
		
		$checkQueryResult = mysql_query($checkQuery);
		
		if(!$checkQueryResult){
			return false;	
		}
		
		$num_rows = mysql_num_rows($checkQueryResult);
		
		if($num_rows == 1){
			return true;
		}
		
		return false;
						
	}
	
	public function checkIfRegistratnAlreadySubscribed(){
		$checkQuery = "SELECT id, email FROM `" . $this->tableName . "`
						WHERE id = " . $this->id . " 
						AND confirm_subscription = 1  
						AND unsubscribed = 0  
						AND email = '" . $this->email . "' LIMIT 1;";	
		
		//echo $checkQuery;
		
		$checkQueryResult = mysql_query($checkQuery);
		
		if(!$checkQueryResult){
			return false;	
		}
		
		$num_rows = mysql_num_rows($checkQueryResult);
		
		if($num_rows == 1){
			return true;
		}
		
		return false;
						
	}	
	
	// returns subscribe url
	// example: http://jasminecon:8888/subscription/subscribe.php?u=laOyladmlqXa1sjXmZfR0t8%3D&i=lamp&t=xuHmzuObzdns 
	public function getSubscribeURL(){
		
		//$subscribeURL = BASUtility::currentURL() .  SUBSCRIPTION_ENCLOSING_FOLDER . "/" . SUBSCRIPTION_FOLDER_NAME . "/" . $this->projectDirectory . "/subscribe.php?";
		$subscribeURL = URL_TO_SUBSCRIBE . "?";
		
		$encrypted_id = $this->bas_encrypt_decrypt->encryptForURL($this->id);
		$encrypted_email = $this->bas_encrypt_decrypt->encryptForURL($this->email);
		$encrypted_tablename = $this->bas_encrypt_decrypt->encryptForURL($this->tableName);
		
		$subscribeURL = $subscribeURL . "u=" . $encrypted_email . "&i=" . $encrypted_id . "&t=" . $encrypted_tablename;
		
		return $subscribeURL;
	}
	
	// returns unsubscribe URL
	// example: http://jasminecon:8888/subscription/unsubscribe.php?u=laOyladmlqXa1sjXmZfR0t8%3D&i=lamp&t=xuHmzuObzdns
	public function getUnSubscribeURL(){
		
		//$subscribeURL = BASUtility::currentURL() . SUBSCRIPTION_ENCLOSING_FOLDER . "/" . SUBSCRIPTION_FOLDER_NAME . "/" . $this->projectDirectory . "/unsubscribe.php?";
		$subscribeURL = URL_TO_UNSUBSCRIBE . "?";
		
		$encrypted_id = $this->bas_encrypt_decrypt->encryptForURL($this->id);
		$encrypted_email = $this->bas_encrypt_decrypt->encryptForURL($this->email);
		$encrypted_tablename = $this->bas_encrypt_decrypt->encryptForURL($this->tableName);
		
		$subscribeURL = $subscribeURL . "u=" . $encrypted_email . "&i=" . $encrypted_id . "&t=" . $encrypted_tablename;
		
		return $subscribeURL;
	}
		
	// update recored with unsubscribe and subscribe urls for this registrants
	public function updateDatabaseURLs(){
		if(!isset($this->tableName)){
			return false;	
		}
		// udpate table
		
		// add unsubscribe link to databse
		// field name unsubscribe_url 
	
		$updateQuery = "UPDATE " . $this->tableName . 
						" SET unsubscribe_url = '" . $this->getUnSubscribeURL() . "', " . 
						" subscribe_url = '" . $this->getSubscribeURL() . "' " . 
						" WHERE id = " . $this->id . " AND email = '" . $this->email . "'";
		//echo $updateQuery;
		
		if(mysql_query($updateQuery)){
			if(mysql_affected_rows() == 0){
				return false;
			}
		}else{
			return false;
		}
			
		return true;
	
	}
	
	// confirms registration
	// save registrant's ip address
	// save current time
	public function updateConfirmSubscription(){
		if(!isset($this->tableName)){
			false;	
		}
		// udpate table
		
		// add unsubscribe link to databse
		// field name unsubscribe_url 
		
		$updateQuery = "UPDATE " . $this->tableName . 
						" SET confirm_subscription = 1, " . 
						" unsubscribed = 0, " . 
						" confirm_subscription_date = '" . BASUtility::getCurrentTimeStamp() . "', " . 
						" ip_address = '" . BASUtility::getClientIPAddress() . "' " . 
						" WHERE id = " . $this->id . " AND email = '" . $this->email . "'";
		//echo $updateQuery;
		
		if(mysql_query($updateQuery)){
			if(mysql_affected_rows() == 0){
				return false;
			}
		}else{
			return false;
		}
			
		return true;
	
	}
		
	
	// confirms registration
	// save registrant's ip address
	// save current time
	public function unSubscribeMe(){
		if(!isset($this->tableName)){
			false;	
		}
		// udpate table
		
		// add unsubscribe link to databse
		// field name unsubscribe_url 
		
		// check if alerady unsubscribed
		
		if($this->checkIfRegistratnAlreadyUnsubscribed()){
			return true;
		}
		
		$updateQuery = "UPDATE " . $this->tableName . 
						" SET confirm_subscription = 0, " . 
						" unsubscribed = 1 " . 
						" WHERE id = " . $this->id . " AND email = '" . $this->email . "';";
		//echo $updateQuery;
		
		if(mysql_query($updateQuery)){
			if(mysql_affected_rows() == 0){
				return false;
			}
		}else{
			return false;
		}
			
		return true;
	
	}
		
		
	public function checkIfRegistratnAlreadyUnsubscribed(){

		$checkQuery = "SELECT id, email FROM `" . $this->tableName . "`
						WHERE id = " . $this->id . " 
						AND unsubscribed = 1  
						AND email = '" . $this->email . "' LIMIT 1;";	
		
		//echo $checkQuery;
		
		$checkQueryResult = mysql_query($checkQuery);
		
		if(!$checkQueryResult){
			return false;	
		}
		
		$num_rows = mysql_num_rows($checkQueryResult);
		
		if($num_rows == 1){
			return true;
		}
		
		return false;
						
	}
	
			
	
	// this will udpate all exsisting rows with unsubscribe and subscribe urls	
	// later we can get this urls to be used in mailchimp or any other mass email programe.
	public function updateAllRowsURLs(){
		// save current id and email
		$org_id = $this->id;
		$org_email = $this->email;
		
		$selectQuery = "SELECT id, email FROM `" . $this->tableName . "`";
				
		$result = mysql_query($selectQuery); 
		$num_rows = mysql_num_rows($result); 	
		
	   while($row=mysql_fetch_array($result)) {
		   
		   $this->id = $row["id"];
		   $this->email = $row["email"];
		   
		  // $this->getSubscribeURL();
		   //$this->getUnSubscribeURL();
		   
			$updateQuery = "UPDATE " . $this->tableName . 
							" SET unsubscribe_url = '" . $this->getUnSubscribeURL() . "', " . 
							" subscribe_url = '" . $this->getSubscribeURL() . "' " . 
							" WHERE id = " . $this->id . " AND email = '" . $this->email . "'";
			
			//echo $updateQuery;
			//echo "<hr>";
		   //echo "updating :: ". $row["id"] . " " . $row["email"].  "<br>";
			
			mysql_query($updateQuery);
		   
		   
	   }
   		
		echo "<br><strong>$num_rows rows updated</strong><br>";	
			
			
		// put back id and email
	   $this->id = $org_id;
	   $this->email = $org_email;
		   		
	}
	
	
	public function backupTable(){
		$tableToBeAltered = $this->tableName;
		$backupTableName = $tableToBeAltered . "_backup_" . mktime();
		
		$createTableQuery = "CREATE TABLE $backupTableName LIKE $tableToBeAltered ;" ;
		
		if(mysql_query($createTableQuery)){
			
			$copyDataQuery = "INSERT INTO $backupTableName SELECT * from $tableToBeAltered;";
			
			if(mysql_query($copyDataQuery)){
				return $backupTableName;
			}			
			
		}
					
		return false;	
	}
	
	// alter table 
	// with addtion of new fields
	// rmove any exsisting filed from the query, otherwise query will not run
	public function addNewFieldsToTableOld(){
		$tableToBeAltered = $this->tableName;
		
		$alterQuery = "ALTER TABLE `" . $tableToBeAltered . "`  
						ADD `ip_address` VARCHAR(20) NOT NULL ,  
						ADD `confirm_subscription` TINYINT NOT NULL DEFAULT '0' ,  
						ADD `confirm_subscription_date` TIMESTAMP NOT NULL ,  
						ADD `unsubscribed` TINYINT NULL DEFAULT NULL ,  
						ADD `unsubscribe_url` VARCHAR(255) NOT NULL ,  
						ADD `subscribe_url` VARCHAR(255) NOT NULL ;";
						
		if(mysql_query($alterQuery)){
			return true;
		}else{
			return false;
		}
	}



	// alter table 
	// with addtion of new fields
	// rmove any exsisting filed from the query, otherwise query will not run
	public function addNewFieldsToTable(){
		$tableToBeAltered = $this->tableName;
		
		$result = mysql_query("SHOW COLUMNS FROM $tableToBeAltered");
		if (!$result) {
			echo 'Could not run query: ' . mysql_error();
			return false;
		}
		
		if (mysql_num_rows($result) > 0) {
			
			$fileArray;
			
			while ($row = mysql_fetch_assoc($result)) {
				
				//echo $row["Field"] . " :: ";
				$fileArray[] = $row["Field"];
		
			}
		}
		
		$alterQuery = "";
		
		if (!in_array('ip_address', $fileArray))
		{
			echo "Adding <strong>ip_address</strong> field to $tableToBeAltered<br>";
			$alterQuery = $alterQuery . " ADD `ip_address` VARCHAR(20) NOT NULL ,";
			
		}
		
		if (!in_array('confirm_subscription', $fileArray)){
			echo "Adding <strong>confirm_subscription</strong> field to $tableToBeAltered<br>";
			$alterQuery = $alterQuery . " ADD `confirm_subscription` TINYINT NOT NULL DEFAULT '0' ,";
		}
		
		if (!in_array('confirm_subscription_date', $fileArray)){
			echo "Adding <strong>confirm_subscription_date</strong> field to $tableToBeAltered<br>";
			$alterQuery = $alterQuery . " ADD `confirm_subscription_date` TIMESTAMP NOT NULL ,";
		}
		
		if (!in_array('unsubscribed', $fileArray)){
			echo "Adding <strong>unsubscribed</strong> field to $tableToBeAltered<br>";
			$alterQuery = $alterQuery . " ADD `unsubscribed` TINYINT NULL DEFAULT '0' ,";
		}
		
		if (!in_array('unsubscribe_url', $fileArray)){
			echo "Adding <strong>unsubscribe_url</strong> field to $tableToBeAltered<br>";
			$alterQuery = $alterQuery . " ADD `unsubscribe_url` VARCHAR(255) NOT NULL ,";
		}
		
		if (!in_array('subscribe_url', $fileArray)){
			echo "Adding <strong>subscribe_url</strong> field to $tableToBeAltered<br>";
			$alterQuery = $alterQuery . " ADD `subscribe_url` VARCHAR(255) NOT NULL ,";
		}
		
		
		if($alterQuery != ""){
			$alterQuery = substr($alterQuery, 0, (strlen($alterQuery) - 1));
			
			$alterQuery = "ALTER TABLE `" . $tableToBeAltered . "` " . $alterQuery . ";";
			
			//echo "<br>" . $alterQuery . "<br>";
			
							
			if(mysql_query($alterQuery)){
				return true;
			}else{
				return false;
			}
			
		}else{
			echo "<br>All fields are already in the <strong>$tableToBeAltered</strong><br>";	
			return true;
		}

		
	}

		
	// deletes any duplicate entiers of email address	
	public function deleteEmailDuplicates(){
		$fromTable = $this->tableName;
		
		$deleteDuplicateQuery = "delete
			from $fromTable using $fromTable,
				$fromTable fromTable1
			where $fromTable.id > fromTable1.id
				and $fromTable.email = fromTable1.email";
				
		if(mysql_query($deleteDuplicateQuery)){
			echo "<br>" . mysql_affected_rows() . " duplicates removed<br>";
			
			return true;
		}else{
			return false;
		}				
	
		
	}

}

//$subscribeUnsubscribe = new BASSubscribeUnsubscribe();

/*
$subscribeUnsubscribe->setId();
$subscribeUnsubscribe->setEamil();
$subscribeUnsubscribe->setTableName();
*/

?>
