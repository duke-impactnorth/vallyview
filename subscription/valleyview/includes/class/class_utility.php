<?php


/*
This class send notifcations to registrants and admins

*/

class BASUtility {
	
	// returns current website url 
	// eg. http://www.jasminecondos.ca
	public static function currentURL() {
		
		if(defined('CURRENT_WEBSITE_PROJECT_URL')){
			return CURRENT_WEBSITE_PROJECT_URL;
		}
		
		$pageURL = 'http';
		
		if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		
		$pageURL .= "://";
		
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"];
		}
		
		return $pageURL;
	}

	// return ip address of registrant
	public static function getClientIPAddress() {
		$ipaddress = '';
		if ($_SERVER['HTTP_CLIENT_IP'])
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if($_SERVER['HTTP_X_FORWARDED_FOR'])
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if($_SERVER['HTTP_X_FORWARDED'])
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if($_SERVER['HTTP_FORWARDED_FOR'])
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if($_SERVER['HTTP_FORWARDED'])
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if($_SERVER['REMOTE_ADDR'])
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
	public static function getCurrentTimeStamp(){
		return $currentTime = date("Y-m-d H:i:s",time());	
	}
	
	public static function getContentsOfFile($passFileLocation){
		$fileData = fopen($passFileLocation,"r");
		$fileContents = fread($fileData, filesize($passFileLocation));
		fclose($fileData);
		
		return $fileContents;
				
	}
}


?>
