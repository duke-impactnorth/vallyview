<?php

$subscription_folder_name = 'subscription';

date_default_timezone_set('America/New_York');

defined('SUBSCRIPTION_FOLDER_NAME') ? null : define('SUBSCRIPTION_FOLDER_NAME', $subscription_folder_name);

// table name must be diffrent for each project
defined('CURRENT_WEBSITE_PROJECT_DATABASE_TABLE_NAME') ? null : define('CURRENT_WEBSITE_PROJECT_DATABASE_TABLE_NAME', $database_table_name);

// project URL http://www.example.com
defined('CURRENT_WEBSITE_PROJECT_URL') ? null : define('CURRENT_WEBSITE_PROJECT_URL', $website_url);

defined('CURRENT_WEBSITE_PROJECT_THANK_YOUPAGE_REDIRECT_URL') ? null : define('CURRENT_WEBSITE_PROJECT_THANK_YOUPAGE_REDIRECT_URL', $thank_you_page_redirect_url);

// project
defined('CURRENT_WEBSITE_PROJECT_NAME') ? null : define('CURRENT_WEBSITE_PROJECT_NAME', $name);

// corporte address
defined('CURRENT_WEBSITE_PROJECT_CORPORATE_ADDRESS') ? null : define('CURRENT_WEBSITE_PROJECT_CORPORATE_ADDRESS', $corporate_address);

// email accounts
defined('CURRENT_WEBSITE_MAIL_FROM') ? null : define('CURRENT_WEBSITE_MAIL_FROM', $email_from);
defined('CURRENT_WEBSITE_MAIL_ACCOUNT_NAME') ? null : define('CURRENT_WEBSITE_MAIL_ACCOUNT_NAME', $email_from_account_name);
defined('CURRENT_WEBSITE_MAIL_REPLY_TO') ? null : define('CURRENT_WEBSITE_MAIL_REPLY_TO', $email_reply_to);
defined('CURRENT_WEBSITE_MAIL_REPLY_TO_ACCOUNT_NAME') ? null : define('CURRENT_WEBSITE_MAIL_REPLY_TO_ACCOUNT_NAME', $email_reply_to_account_name);

// email SMTP authorization if required
defined('CURRENT_WEBSITE_MAIL_SMTP_AUTH') ? null : define('CURRENT_WEBSITE_MAIL_SMTP_AUTH', $flag_use_SMTP_authorization); // true or false

defined('CURRENT_WEBSITE_MAIL_SMTP_USERNAME') ? null : define('CURRENT_WEBSITE_MAIL_SMTP_USERNAME', $SMTP_username); //
defined('CURRENT_WEBSITE_MAIL_SMTP_PASSWORD') ? null : define('CURRENT_WEBSITE_MAIL_SMTP_PASSWORD', $SMTP_password); //
defined('CURRENT_WEBSITE_MAIL_SMTP_PORT') ? null : define('CURRENT_WEBSITE_MAIL_SMTP_PORT', $SMTP_port); //
defined('CURRENT_WEBSITE_MAIL_SMTP_HOST') ? null : define('CURRENT_WEBSITE_MAIL_SMTP_HOST', $SMTP_host); //


defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
defined('SITE_ROOT') ? null : define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT']);


$currentDir = dirname(__FILE__);

$findPre = DS . SUBSCRIPTION_FOLDER_NAME . DS;
$findPost = DS . "includes";

$getEnclosingFolderForSubscriptionFolder = get_string_between($currentDir, SITE_ROOT, DS . SUBSCRIPTION_FOLDER_NAME . DS);

defined('SUBSCRIPTION_FOLDER_ROOT') ? null : define('SUBSCRIPTION_FOLDER_ROOT', $_SERVER['DOCUMENT_ROOT'] . $getEnclosingFolderForSubscriptionFolder);
defined('SUBSCRIPTION_ENCLOSING_FOLDER') ? null : define('SUBSCRIPTION_ENCLOSING_FOLDER', substr($getEnclosingFolderForSubscriptionFolder, 1));


$projectFolderName = get_string_between($currentDir, $findPre, $findPost);
defined('SITE_PROJECT_DIRECTORY') ? null : define('SITE_PROJECT_DIRECTORY', $projectFolderName);


// template folder location
defined('FILE_TEMPLATE_FOLDER') ? null : define('FILE_TEMPLATE_FOLDER', SUBSCRIPTION_FOLDER_ROOT  .  DS . SUBSCRIPTION_FOLDER_NAME . DS . SITE_PROJECT_DIRECTORY . DS . 'templates' . DS);

// templates
defined('CURRENT_WEBSITE_TEMPLATE_CONFIRM_SUBSCRIPTION_EMAIL_SUBJECT') ? null : define('CURRENT_WEBSITE_TEMPLATE_CONFIRM_SUBSCRIPTION_EMAIL_SUBJECT', $template_confirm_subscription_email_subject);

defined('CURRENT_WEBSITE_TEMPLATE_CONFIRM_SUBSCRIPTION_EMAIL') ? null : define('CURRENT_WEBSITE_TEMPLATE_CONFIRM_SUBSCRIPTION_EMAIL', FILE_TEMPLATE_FOLDER . 'confirm_email.html');
defined('CURRENT_WEBSITE_TEMPLATE_CONFIRMED_SUBSCRIPTION_EMAIL') ? null : define('CURRENT_WEBSITE_TEMPLATE_CONFIRMED_SUBSCRIPTION_EMAIL', FILE_TEMPLATE_FOLDER . 'confirmed_email.html');
defined('CURRENT_WEBSITE_TEMPLATE_ADMIN_NOTIFICATION_EMAIL') ? null : define('CURRENT_WEBSITE_TEMPLATE_ADMIN_NOTIFICATION_EMAIL', FILE_TEMPLATE_FOLDER . 'notification.html');
defined('CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_EMAIL') ? null : define('CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_EMAIL', FILE_TEMPLATE_FOLDER . 'unsubscribe.html');
defined('CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_CONFIRM_EMAIL') ? null : define('CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_CONFIRM_EMAIL', FILE_TEMPLATE_FOLDER . 'unsubscribe_confirm.html');


$sub_enc_folder = "";

if(SUBSCRIPTION_ENCLOSING_FOLDER != ""){
	$sub_enc_folder = DS . SUBSCRIPTION_ENCLOSING_FOLDER;
}

defined('URL_TO_PROJECT') ? null : define('URL_TO_PROJECT', CURRENT_WEBSITE_PROJECT_URL . $sub_enc_folder . DS . SUBSCRIPTION_FOLDER_NAME . DS . SITE_PROJECT_DIRECTORY . DS);
defined('URL_TO_SUBSCRIBE') ? null : define('URL_TO_SUBSCRIBE', CURRENT_WEBSITE_PROJECT_URL . $sub_enc_folder . DS . SUBSCRIPTION_FOLDER_NAME . DS . SITE_PROJECT_DIRECTORY . DS . 'subscribe.php');
defined('URL_TO_UNSUBSCRIBE') ? null : define('URL_TO_UNSUBSCRIBE', CURRENT_WEBSITE_PROJECT_URL . $sub_enc_folder . DS . SUBSCRIPTION_FOLDER_NAME . DS . SITE_PROJECT_DIRECTORY . DS . 'unsubscribe.php');
defined('URL_TO_UNSUBSCRIBE_PROCESSOR') ? null : define('URL_TO_UNSUBSCRIBE_PROCESSOR', CURRENT_WEBSITE_PROJECT_URL . $sub_enc_folder . DS . SUBSCRIPTION_FOLDER_NAME . DS . SITE_PROJECT_DIRECTORY . DS . 'unsubscribe-processor.php');
defined('URL_TO_TEMPLATES_FOLDER') ? null : define('URL_TO_TEMPLATES_FOLDER', CURRENT_WEBSITE_PROJECT_URL . $sub_enc_folder . DS . SUBSCRIPTION_FOLDER_NAME . DS . SITE_PROJECT_DIRECTORY  . DS . 'templates' . DS);

// skip_opt_in

if(isset($skip_opt_in_template_location)){

	if(substr($skip_opt_in_template_location, 0, 1) == '\\'){
		$skip_opt_in_template_location = substr($skip_opt_in_template_location, 1);
	}

	defined('SKIP_OPT_IN_TEMPLATE_LOCATION') ? null : define('SKIP_OPT_IN_TEMPLATE_LOCATION', $_SERVER['DOCUMENT_ROOT'] . $skip_opt_in_template_location); //
	defined('SKIP_OPT_IN_TEMPLATE_SUBJECT_LINE') ? null : define('SKIP_OPT_IN_TEMPLATE_SUBJECT_LINE', $skip_opt_in_template_subject_line); //

}else{
	defined('SKIP_OPT_IN_TEMPLATE_LOCATION') ? null : define('SKIP_OPT_IN_TEMPLATE_LOCATION', ""); //
	defined('SKIP_OPT_IN_TEMPLATE_SUBJECT_LINE') ? null : define('SKIP_OPT_IN_TEMPLATE_SUBJECT_LINE', ""); //

}

if(isset($custom_autoresponder_template_location)){

	if(substr($custom_autoresponder_template_location, 0, 1) == '\\'){
		$custom_autoresponder_template_location = substr($custom_autoresponder_template_location, 1);
	}

	defined('CUSTOM_AUTORESPONDER_TEMPLATE_LOCATION') ? null : define('CUSTOM_AUTORESPONDER_TEMPLATE_LOCATION', $_SERVER['DOCUMENT_ROOT'] . $custom_autoresponder_template_location); //
	defined('CUSTOM_AUTORESPONDER_TEMPLATE_SUBJECT_LINE') ? null : define('CUSTOM_AUTORESPONDER_TEMPLATE_SUBJECT_LINE', $custom_autoresponder_template_subject_line); //

}else{
	defined('CUSTOM_AUTORESPONDER_TEMPLATE_LOCATION') ? null : define('CUSTOM_AUTORESPONDER_TEMPLATE_LOCATION', ""); //
	defined('CUSTOM_AUTORESPONDER_TEMPLATE_SUBJECT_LINE') ? null : define('CUSTOM_AUTORESPONDER_TEMPLATE_SUBJECT_LINE', ""); //

}


if(DEBUG_ON){

	echo '<p style="text-align:left;" >';
	echo "<strong>SUBSCRIPTION_FOLDER_NAME: " . SUBSCRIPTION_FOLDER_NAME . "</strong><br>";

	echo "<strong>CURRENT_WEBSITE_PROJECT_URL: " . CURRENT_WEBSITE_PROJECT_URL . "</strong><br>";
	echo "<strong>SITE_ROOT: " . SITE_ROOT . "</strong><br>";
	echo "<strong>currentDir: " . $currentDir . "</strong><br>";
	echo "<strong>SUBSCRIPTION_ENCLOSING_FOLDER: " . SUBSCRIPTION_ENCLOSING_FOLDER . "</strong><br>";
	echo "<strong>SUBSCRIPTION_FOLDER_ROOT: " . SUBSCRIPTION_FOLDER_ROOT . "</strong><br>";
	echo "<strong>SITE_PROJECT_DIRECTORY: " . SITE_PROJECT_DIRECTORY . "</strong><br>";

	echo '</p>';
	echo '<p style="text-align:left;" >';

	echo "<strong>FILE_TEMPLATE_FOLDER: " . FILE_TEMPLATE_FOLDER . "</strong><br>";

	echo "<strong>CURRENT_WEBSITE_TEMPLATE_CONFIRM_SUBSCRIPTION_EMAIL: " . CURRENT_WEBSITE_TEMPLATE_CONFIRM_SUBSCRIPTION_EMAIL . "</strong><br>";
	echo "<strong>CURRENT_WEBSITE_TEMPLATE_CONFIRMED_SUBSCRIPTION_EMAIL: " . CURRENT_WEBSITE_TEMPLATE_CONFIRMED_SUBSCRIPTION_EMAIL . "</strong><br>";
	echo "<strong>CURRENT_WEBSITE_TEMPLATE_ADMIN_NOTIFICATION_EMAIL: " . CURRENT_WEBSITE_TEMPLATE_ADMIN_NOTIFICATION_EMAIL . "</strong><br>";
	echo "<strong>CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_EMAIL: " . CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_EMAIL . "</strong><br>";
	echo "<strong>CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_CONFIRM_EMAIL: " . CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_CONFIRM_EMAIL . "</strong><br>";

	echo '</p>';
	echo '<p style="text-align:left;" >';

	echo "<strong>URL_TO_SUBSCRIBE: " . URL_TO_SUBSCRIBE . "</strong><br>";
	echo "<strong>URL_TO_UNSUBSCRIBE: " . URL_TO_UNSUBSCRIBE . "</strong><br>";

	echo '</p>';
	echo '<p style="text-align:left;" >';

	$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	echo "<strong>actual_link: " . $actual_link . "</strong><br>";
	echo "<strong>HTTP_HOST: " . $_SERVER[HTTP_HOST] . "</strong><br>";
	echo "<strong>PHP_SELF: " . $_SERVER[PHP_SELF] . "</strong><br>";

	echo '</p>';
	echo '<p style="text-align:left;" >';

	echo print_r($_SERVER);

	echo '</p>';
}

function get_string_between($string, $start, $end){
    $string = " ".$string;
    $ini = strpos($string,$start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string,$end,$ini) - $ini;
    return substr($string,$ini,$len);
}

require_once(SUBSCRIPTION_FOLDER_ROOT. DS . SUBSCRIPTION_FOLDER_NAME . DS . 'config.php');

require_once('class/class_main.php');
require_once('class/class_encrypt_decrypt.php');
require_once('class/class_subscribe_unsubscribe.php');
require_once('class/class_send_notifications.php');
require_once('class/class_utility.php');

require_once('class/class.phpmailer.php');
require_once('class/class.pop3.php');
require_once('class/class.smtp.php');


?>