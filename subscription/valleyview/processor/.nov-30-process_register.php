<?php

// validate form



if (!$_REQUEST["middle_name"] && $_REQUEST["fax_number"] == "416.661.7866") {
	//echo "here 1";

    if (!$_POST['email']) {
        echo "Required fields must be entered. Please go Back and fill the missing info";
    } else {
		//echo "here 2";

        // connect to database
    	require_once('../includes/initialize_subscription.php');
		//echo "here 3";

        //Get data into array


        $allArray = array(
            array(
                "data" => $_POST["first_name"],
                "field" => "first_name",
                "title" => "First Name"),
            array(
                "data" => $_POST["last_name"],
                "field" => "last_name",
                "title" => "Last Name"),
            array(
                "data" => $_POST["email"],
                "field" => "email",
                "title" => "Email Address"),            

            array(
                "data" => $_POST["postal_zip"],
                "field" => "postal_zip",
                "title" => "Postal Code"),
            array(
                "data" => $_POST["phone"],
                "field" => "phone",
                "title" => "Telephone"),
            array(
                "data" => $_POST["how_did_you_hear"],
                "field" => "how_did_you_hear",
                "title" => "How did you hear about us?"),

            array(
                "data" => $_POST["broker"],
                "field" => "broker",
                "title" => "Are you a broker?"),
			array(
                "data" => $_POST["brokerage"],
                "field" => "brokerage",
                "title" => "Brokerage"),
			
            array(
                "data" => $_POST["comments"],
                "field" => "comments",
                "title" => "comments")
        );

	//echo "here 4";
           

        $bas_main = new BASMain();
        $bas_main->setProjectName(CURRENT_WEBSITE_PROJECT_NAME);
        $bas_main->setProjectEmailAddressFrom(CURRENT_WEBSITE_MAIL_FROM);
        $bas_main->setProjectEmailAddressReplyTo(CURRENT_WEBSITE_MAIL_REPLY_TO);

        $bas_main->setDatabaseConnection($connection);
        $bas_main->setTableName(CURRENT_WEBSITE_PROJECT_DATABASE_TABLE_NAME);
        $bas_main->setSubmittedFormDataArray($allArray);

        $bas_main->setEmail(trim(strtolower($_POST["email"])));
        $bas_main->setFirstName(trim($_POST["first_name"]));
        $bas_main->setLastName(trim($_POST["last_name"]));

		foreach ($adminEmailArray as $adminEmail) {
			//echo "<h2>" . $adminEmail . "</h2>";
			$bas_main->setAdminEmailAddress($adminEmail, "Admin");
		}		

        // this auto responder will only be sent, if confirmed registrant is registring again.
        //$bas_main->setAutoResponderTemplateLocation("/emailers/autoresponder/autoresponder.html"); // comment this line if you do not want to send any default autoresponse
        //$bas_main->setAutoResponderSubjectLine("Thank you For Registering"); // comment this line if you do not want to send any default autoresponse

        $bas_main->executeRegistration();
		//echo "here 5";

        header("Location: " . CURRENT_WEBSITE_PROJECT_THANK_YOUPAGE_REDIRECT_URL);
    }
} else {
    header("Location: " . "/");
}

exit();
?>
