<?php



require_once('includes/initialize_subscription.php');



// if there is an autroresponse that need to be send after subscription confirmation, un comments the line below and pass propber location.

//$autoResponderTemplateLocation = "/emailers/autoresponder/autoresponder.html"; // comment this line if you do not want to send any autoresponse

//$autoResponderSubjectLine = "Thank You for registering"; // comment this line if you do not want to send any autoresponse

if(strlen(CUSTOM_AUTORESPONDER_TEMPLATE_LOCATION) > 5){
	$autoResponderTemplateLocation = CUSTOM_AUTORESPONDER_TEMPLATE_LOCATION;
	$autoResponderSubjectLine = CUSTOM_AUTORESPONDER_TEMPLATE_SUBJECT_LINE;
}




//echo "<h1>" . SITE_ROOT . "</h1>";



// i > id

// u > email

// t > table name



if(!isset($_GET["i"]) || !isset($_GET["u"])  || !isset($_GET["t"]) ){

	 header("Location: /");

}





$bas_encrypt_decrypt = new BASEncryptDecrypt();



$decrypted_id = $bas_encrypt_decrypt->decryptForURL($_GET["i"]); 

$decrypted_email = $bas_encrypt_decrypt->decryptForURL($_GET["u"]);

$decrypted_table_name = $bas_encrypt_decrypt->decryptForURL($_GET["t"]);



$subscribeUnsubscribe = new BASSubscribeUnsubscribe();



$subscribeUnsubscribe->setDatabaseConnection($connection);

$subscribeUnsubscribe->setId($decrypted_id);

$subscribeUnsubscribe->setEmail($decrypted_email);

$subscribeUnsubscribe->setTableName($decrypted_table_name);

$subscribeUnsubscribe->setProjectDirectory(SITE_PROJECT_DIRECTORY);





$registratnAlreadySubscribed = $subscribeUnsubscribe->checkIfRegistratnAlreadySubscribed();



//echo "<h1>" . $decrypted_table_name . "</h1>";



if($subscribeUnsubscribe->updateConfirmSubscription()){
	//echo "<h2>if true</h2>";

	

	$unSubscribeURL = $subscribeUnsubscribe->getUnSubscribeURL();

	

	

	//echo $decrypted_id . " : ";

	//echo $decrypted_email . " : ";

	//echo $decrypted_table_name;

	

	

	$getHTMLBody = BASUtility::getContentsOfFile(CURRENT_WEBSITE_TEMPLATE_CONFIRMED_SUBSCRIPTION_EMAIL);

	//echo '<h1 style="color:#ff">' . CURRENT_WEBSITE_TEMPLATE_CONFIRMED_SUBSCRIPTION_EMAIL . "</h1>";

	// Variables to change: {site_name}, {site_url}, {contact_email}, {user_email}, {un_subscribe_url}

	

	

			

	$original = array(	"{un_subscribe_url}", 
	
						"{url_templates}", 

						"{contact_email}", 

						"{site_name}", 

						"{site_url}", 

						"{user_email}",

						"{project_dir}",

						"{corporate_address}");

	$replace = array(	$unSubscribeURL ,  

						URL_TO_TEMPLATES_FOLDER,
						CURRENT_WEBSITE_MAIL_REPLY_TO, 

						CURRENT_WEBSITE_MAIL_ACCOUNT_NAME, 

						BASUtility::currentURL(),

						$decrypted_email,

						SUBSCRIPTION_ENCLOSING_FOLDER  .  '/' . SUBSCRIPTION_FOLDER_NAME . '/' . SITE_PROJECT_DIRECTORY,

						CURRENT_WEBSITE_PROJECT_CORPORATE_ADDRESS);

	

	

	$getHTMLBody = str_replace($original,$replace, $getHTMLBody);		

	

	echo $getHTMLBody;

 
	

	//if(!$registratnAlreadySubscribed){

		if($autoResponderTemplateLocation){

			// send custom auto responder

			sendCustomAutoResponseNewsletter();

		}else{

			// send default auto response

			sendDefaultAutoResponse($getHTMLBody);

		}

	//}

	

}else{

	echo "Some thing is not right, please check your link again.";

}





function sendDefaultAutoResponse($passHTMLBody){
	//echo "<h2>function sendDefaultAutoResponse()</h2>";

	global $autoResponderTemplateLocation, $autoResponderSubjectLine, $decrypted_id, $decrypted_email, $decrypted_table_name, $connection;

	$email_subject = "Thank You for registering with" . CURRENT_WEBSITE_PROJECT_NAME . " ";

	$bas_main = new BASMain(); 

	$bas_main->setProjectName(CURRENT_WEBSITE_PROJECT_NAME);
	$bas_main->setProjectEmailAddressFrom(CURRENT_WEBSITE_MAIL_FROM);
	$bas_main->setProjectEmailAddressReplyTo(CURRENT_WEBSITE_MAIL_REPLY_TO);

	$bas_main->setDatabaseConnection($connection);

	$bas_main->setTableName($decrypted_table_name);

			

	$bas_main->setAutoResponderTemplateLocation($autoResponderTemplateLocation); 

	$bas_main->setAutoResponderSubjectLine($autoResponderSubjectLine);

	

	$bas_main->sendAutoResponseAfterSubscribeConfirmed($decrypted_id, $decrypted_email, $passHTMLBody, $email_subject);	

}


  


function sendCustomAutoResponseNewsletter(){
	//echo "<h2>function sendCustomAutoResponseNewsletter()</h2>";

	global $autoResponderTemplateLocation, $autoResponderSubjectLine, $decrypted_id, $decrypted_email, $decrypted_table_name, $connection;

	
	//echo "<h2>" . $autoResponderTemplateLocation . "</h2>";

	$bas_main = new BASMain(); 

	$bas_main->setProjectName(CURRENT_WEBSITE_PROJECT_NAME);
	$bas_main->setProjectEmailAddressFrom(CURRENT_WEBSITE_MAIL_FROM);
	$bas_main->setProjectEmailAddressReplyTo(CURRENT_WEBSITE_MAIL_REPLY_TO);

	$bas_main->setDatabaseConnection($connection);
	$bas_main->setTableName($decrypted_table_name);

	$bas_main->setAutoResponderTemplateLocation($autoResponderTemplateLocation); 

	$bas_main->setAutoResponderSubjectLine($autoResponderSubjectLine);

	$bas_main->sendCustomAutoResponseNewsletterAfterSubscribeConfirmed($decrypted_id, $decrypted_email);

	

}







?>



