<?php

require_once('includes/initialize_subscription.php');

//echo "<h1>" . SITE_ROOT . "</h1>";

// i > id
// u > email
// t > table name

if(!isset($_REQUEST["i"]) || !isset($_REQUEST["u"])  || !isset($_REQUEST["t"]) ){
	
	 header("Location: /");
}



$bas_encrypt_decrypt = new BASEncryptDecrypt();

$decrypted_id = $bas_encrypt_decrypt->decryptForURL($_REQUEST["i"]);
$decrypted_email = $bas_encrypt_decrypt->decryptForURL($_REQUEST["u"]);
$decrypted_table_name = $bas_encrypt_decrypt->decryptForURL($_REQUEST["t"]);

$subscribeUnsubscribe = new BASSubscribeUnsubscribe();

$subscribeUnsubscribe->setDatabaseConnection($connection);
$subscribeUnsubscribe->setId($decrypted_id);
$subscribeUnsubscribe->setEmail($decrypted_email);
$subscribeUnsubscribe->setTableName($decrypted_table_name);
$subscribeUnsubscribe->setProjectDirectory(SITE_PROJECT_DIRECTORY);

//echo "<h1>" . $_REQUEST["email-address"] . "</h1>";
//echo "<h1>" . $decrypted_email . "</h1>";


if((strtolower($_REQUEST["email-address"]) == strtolower($decrypted_email) ) && $subscribeUnsubscribe->unSubscribeMe()){
	

	$getHTMLBody = BASUtility::getContentsOfFile(CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_CONFIRM_EMAIL);
			
	// Variables to change: {site_name}, {site_url}, {contact_email}, {user_email}, {un_subscribe_url}
	
	
			
	$original = array(	"{site_name}",  
						"{url_templates}",
						"{project_dir}",
						"{site_url}");
	$replace = array(	CURRENT_WEBSITE_MAIL_ACCOUNT_NAME ,  
						URL_TO_TEMPLATES_FOLDER,
						SUBSCRIPTION_FOLDER_ROOT  .  '/' . SUBSCRIPTION_FOLDER_NAME . '/' . SITE_PROJECT_DIRECTORY,
						BASUtility::currentURL());
	
	
	$getHTMLBody = str_replace($original,$replace, $getHTMLBody);		
	
	echo $getHTMLBody;
}else{
	echo "Some thing is not right, please check your link again.";
}


?>

