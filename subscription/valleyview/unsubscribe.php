<?php

require_once('includes/initialize_subscription.php');

//echo "<h1>" . SITE_ROOT . "</h1>";

// i > id
// u > email
// t > table name

if(!isset($_GET["i"]) || !isset($_GET["u"])  || !isset($_GET["t"]) ){
	 header("Location: /");
}



$bas_encrypt_decrypt = new BASEncryptDecrypt();

$decrypted_id = $bas_encrypt_decrypt->decryptForURL($_GET["i"]);
$decrypted_email = $bas_encrypt_decrypt->decryptForURL($_GET["u"]);
$decrypted_table_name = $bas_encrypt_decrypt->decryptForURL($_GET["t"]);

$subscribeUnsubscribe = new BASSubscribeUnsubscribe();

$subscribeUnsubscribe->setDatabaseConnection($connection);
$subscribeUnsubscribe->setId($decrypted_id);
$subscribeUnsubscribe->setEmail($decrypted_email);
$subscribeUnsubscribe->setTableName($decrypted_table_name);
$subscribeUnsubscribe->setProjectDirectory(SITE_PROJECT_DIRECTORY);

//echo "<h1>" . $decrypted_table_name . "</h1>";

if($subscribeUnsubscribe->checkIfRegistratnExsists()){
	
	$unSubscribeURL = $subscribeUnsubscribe->getUnSubscribeURL();
	
	/*
	echo $decrypted_id;
	echo $decrypted_email;
	echo $decrypted_table_name;
	*/
	
	$getHTMLBody = BASUtility::getContentsOfFile(CURRENT_WEBSITE_TEMPLATE_UNSUBSCRIBE_EMAIL);
			
	// Variables to change: {site_name}, {site_url}, {contact_email}, {user_email}, {un_subscribe_url}
	
//{site_name}, {site_url}, {user_email}, {unsubscribe_processor_parameters}	
			
	$unsubscribe_processor_parameter_u = $_GET["u"];	
	$unsubscribe_processor_parameter_i = $_GET["i"];	
	$unsubscribe_processor_parameter_t = $_GET["t"];	

	$original = array(	"{unsubscribe_processor_parameter_u}", 
						"{unsubscribe_processor_parameter_i}", 
						"{unsubscribe_processor_parameter_t}", 
						"{url_templates}", 
						"{unsubscribe_processor_url}", 
						"{site_name}", 
						"{site_url}", 
						"{project_dir}",
						"{user_email}");
	$replace = array(	$unsubscribe_processor_parameter_u,  
						$unsubscribe_processor_parameter_i,
						$unsubscribe_processor_parameter_t,
						URL_TO_TEMPLATES_FOLDER,
						URL_TO_UNSUBSCRIBE_PROCESSOR,
						CURRENT_WEBSITE_MAIL_ACCOUNT_NAME, 
						BASUtility::currentURL(),
						SUBSCRIPTION_ENCLOSING_FOLDER  .  '/' . SUBSCRIPTION_FOLDER_NAME . '/' . SITE_PROJECT_DIRECTORY,
						$decrypted_email);
	
	
	$getHTMLBody = str_replace($original,$replace, $getHTMLBody);		
	
	echo $getHTMLBody;
}else{
	echo "Some thing is not right, please check your link again.";
}


?>

